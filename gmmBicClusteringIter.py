#!/usr/bin/env python

#####################################################################################################################################################################
#REQUIRES: scikit-learn/scipy
#sys.argv[1] = Sorted file with TR events 
#sys.argv[2] = output directory location 
#python gmmBicClusteringIter.py <INPUT_FILE> <OUTPUT_DIR> 
#####################################################################################################################################################################
import sys
import math
import os
from itertools import *
from sklearn import mixture as M
from scipy.stats import anderson as ad
from scipy.stats import binom as bN
import numpy as np
#import cProfile

def getCseparationGauss(g_stats,c):
    lowerBound = []
    st1 = g_stats[0]
    st2 = g_stats[1]
    sTd12 = max(math.sqrt(g_stats[0][1]),math.sqrt(g_stats[1][1]))
    diffMeans = abs(g_stats[0][0]-g_stats[1][0])
    lowerBound.append(diffMeans)
    for I in c:
        lowerBound.append(I*sTd12)
    return lowerBound

def getProbFromBinom(numCl):
    minWt = int(min(numCl))
    numData = int(numCl[0] + numCl[1])
    probMinWt = bN.cdf(minWt,numData,0.5)
    return probMinWt

class qbin:
    def __init__(self):
        self.Query_id, self.targetStart, self.targetEnd = None,0,0
        self.ReftRStart, self.ReftREnd, self.RefPeriod, self.RefNum, self.tRSeq, self.anchorPre, self.anchorSuf, self.naiveTR = 0,0,0,0.0,None,0,0,0.0

def parseQbin(file):
    for line in open(file):
        values = line.rstrip("\n").split(" ")
        hit = qbin()
        hit.Query_id, hit.targetStart, hit.targetEnd = values[0], int(values[1]),int(values[2])
        hit.ReftRStart, hit.ReftREnd, hit.RefPeriod, hit.RefNum, hit.tRSeq, hit.anchorPre, hit.anchorSuf, hit.naiveTR = int(values[3]), int(values[4]), int(values[5]), float(values[6]), values[7], int(values[8]),int(values[9]),float(values[10])
        yield hit

def removeLabel(TR_list,label,cl,L):
    TR_listR = []
    TR_listLabel = -10
    for i in range(0,len(TR_list)):
        if label[i] != cl:
           TR_listR.append(TR_list[i])
        else:
           TR_listLabel = (i)
    return TR_listR,TR_listLabel

def decouple(TR_listNA,label):
    temp = list(label)
    TR_listNA.reverse()
    for l in TR_listNA:
        temp.insert(l,-1)
    return temp

def andersonDarling(X):
    A2stats = ad(X) #anderson-darling EDF test
    numTR = np.size(X)
    #modified value of A2stats
    modA2 = (1.0 + 0.75/numTR + 2.25/(numTR*numTR))
    A2statsMod = A2stats[0]*modA2
    return A2statsMod

def cluster_TRs(TR_list):
    epsilon = 0.01
    flag = True
    TR_listNA = []
    bic = np.zeros((2),dtype=np.float_)
    while (flag):
        #print "TR_list:\n",TR_list
        numTR = len(TR_list)
        TR_Data = np.zeros((numTR,1),dtype=np.float_)
        TR_Data[:,0] = np.array(TR_list)
        if numTR == 2:
           g1_stats = (np.mean(TR_Data),np.var(TR_Data))
           flag = False
           label = np.array([0,0])
           bic[1], bic[0] = 0, 0
           break
        else:
            #Calling GMM routines here...
            g1 = M.GMM(n_components=1)
            g1.fit(TR_Data)
            bic[0] = g1.bic(TR_Data)
            g1_stats = (g1.means_[0][0],g1.covars_[0][0])
            g2 = M.GMM(n_components=2)
            g2.fit(TR_Data)
            bic[1] = g2.bic(TR_Data)
            label = g2.predict(TR_Data)
            weight = [g2.weights_[0],g2.weights_[1]]
            g2_stats = ((g2.means_[0][0],g2.covars_[0][0]),(g2.means_[1][0],g2.covars_[1][0]))
            #Check if num of reads in any label is = 1
            #print bic[0], bic[1]
            if bic[1] < bic[0]:
               if weight[0]*numTR > 1.0+epsilon and weight[1]*numTR > 1.0+epsilon:
                  flag = False
                  break
               else:
                  if weight[0]*numTR <= 1.0+epsilon:
                     TR_list,TR_listLabel = removeLabel(TR_list,label,0,len(TR_listNA)) #remove label 0 and return the TR list
                  elif weight[1]*numTR <= 1.0+epsilon:
                     TR_list,TR_listLabel = removeLabel(TR_list,label,1,len(TR_listNA)) #remove label 1 and return the TR list
                  TR_listNA.append(TR_listLabel)
                  #print "TR_listNA:\n",TR_listNA
            else:
               break
    #print "bic[0], bic[1]: ", bic[0], bic[1]
    if bic[1] < bic[0]:
       oneAllele = False
    else: 
       oneAllele = True
    #print "TR_list:\n", TR_list
    labelD = decouple(TR_listNA,label)
    #print "Labels mapped to original TR List: ", labelD
    A2stats = andersonDarling(TR_Data[:,0]) 
    #print "oneAllele:\n",oneAllele
    return oneAllele,bic,g1_stats,g2_stats,label,weight,labelD,len(TR_listNA),A2stats

def writeReadsToFile(hits,outfile,labelD,bic,numTRs,wt,stats12,numAllele,numTRna,A2stats,prob,str_lb):
    index = 0
    for hit in hits:
        cl = labelD[index]
        if cl != -1:
           m = stats12[cl][0]
           stD = math.sqrt(stats12[cl][1])
           siZe = wt[cl]
           outfile.write("%s %d %d %d %d %d %.2f %s %.2f %.2f %d %d %.2f %.2f %.2f %d %d %.2f %.2f %d %d %.2f %s\n" %(hit.Query_id,hit.targetStart,hit.targetEnd,hit.ReftRStart,hit.ReftREnd,hit.RefPeriod,hit.RefNum,hit.tRSeq,hit.naiveTR,hit.naiveTR/hit.RefNum,numAllele,cl,siZe,m,stD,numTRs,numTRna,bic[cl],A2stats,hit.anchorPre,hit.anchorSuf,prob,str_lb))
        else:
           noCluster = 'na'
           outfile.write("%s %d %d %d %d %d %.2f %s %.2f %.2f %s %d %d\n" %(hit.Query_id,hit.targetStart,hit.targetEnd,hit.ReftRStart,hit.ReftREnd,hit.RefPeriod,hit.RefNum,hit.tRSeq,hit.naiveTR,hit.naiveTR/hit.RefNum,noCluster,hit.anchorPre,hit.anchorSuf))
        index += 1
    outfile.flush()

def binQtrs():
    inm5fn = sys.argv[1]
    tempm5 = inm5fn.split("/")[-1].split(".")[0]
    outBinfn = sys.argv[2]
    hgT_outFn = outBinfn + "/" + tempm5 + "_cluster.txt"
    outfile = open(hgT_outFn, 'w')
    outfile.write("QueryId targetStart targetEnd refTrStart rfTrEnd rfTrPeriod rfTrNum rfTrSeq QNum TRratio numAllele cluster Weight Mean Std TotalReads Rejects BIC Ad2Stat anchorPre anchorSuf probBN DiffMean(cluster=2) c=1.0 c=1.5 c=2.0\n")
    outfile.write("#                                                                                         qTr/rTr   1,2,-1  0,1                                    1.159                                                                 #\n")
    #housekeeping for outfile

    acceptData = False
    oneAllele = False
    hits = parseQbin(inm5fn)
    for k,g in groupby(hits, lambda x: x.ReftRStart):
        hits = list(g)
        naiveTRs = []
        for hit in hits:
            naiveTRs.append((hit.naiveTR)/(hit.RefNum))
        numTRs = len(naiveTRs)
        #call the clustering module#
        if len(naiveTRs) > 2:
           oneAllele,bic,g1_stats,g2_stats,label,weight,labelD,numTRna,A2stats = cluster_TRs(naiveTRs)
           if oneAllele == True:
              numAllele = 1
              m2, s2 = 0, 0
              wt = [1.0, 0.0]
              g1_stats = (g1_stats,(0.0,0.0))
              prob = 1.0
              str_lb = 'na na na na'
              labelD = [0 if x != -1 else x for x in labelD]
              #print "labelD in oneAllele==True:\n",labelD 
              writeReadsToFile(hits,outfile,labelD,bic,numTRs,wt,g1_stats,numAllele,numTRna,A2stats,prob,str_lb)
           else:
              numAllele = 2
              netNum = numTRs - numTRna
              numCL = [weight[0]*netNum,weight[1]*netNum]
              prob = getProbFromBinom(numCL)
              lb = getCseparationGauss(g2_stats,[1.0,1.5,2.0])
              flb = ["%.2f" % member for member in lb]
              str_lb = ' '.join(map(str,flb))
              #print "Str_lb: ",str_lb
              writeReadsToFile(hits,outfile,labelD,bic,numTRs,weight,g2_stats,numAllele,numTRna,A2stats,prob,str_lb)
        else:
            numAllele = -1
            g1_stats = (np.mean(np.array(naiveTRs)), np.std(np.array(naiveTRs)))
            g1_stats = (g1_stats,(0.0,0.0))
            bic = [0,0]
            labelD = [0]*numTRs
            wt = [1.0, 0.0]
            rejectReads = 0
            numTRna = 0
            prob = 0.0
            str_lb = 'na na na na'
            A2stats = 0
            writeReadsToFile(hits,outfile,labelD,bic,numTRs,wt,g1_stats,numAllele,numTRna,A2stats,prob,str_lb)
        outfile.flush() 
    outfile.close()

#cProfile.run('binQtrs()')
binQtrs()
