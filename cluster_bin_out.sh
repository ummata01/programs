#!/bin/bash

#export PYTHONPATH="/hpc/users/ummata01/gitrepos/hupac/scripts:$PATH"
#export PATH="/packages/numpy/1.6.1-python2.7.2/packages/python/2.7.2/bin:$PATH"

path1="/hpc/users/ummata01/targets_overhang100"
path=`pwd`
tag="chr"
numProc="1"
numHours="4"
ver=".txt"

i="1"
while [ $i -le 22 ]
 do
   varr=`qstat -u ummata01 | grep Q | wc -l`
   if [ $varr -le 200 ]
   then
     m5=$tag$i
     for line in $(cat $path/all_m5/$m5/fofn_m5)
      do
       #submitjob $numHours -c $numProc -m 12 -q small -A acc_25 python $path1/querydict_latest3.py $line $path/trfMaskChrom/$m5$ver $path/all_m5/bin_out4/$m5 2\>\&1
       submitjob $numHours -c $numProc -m 12 -q small_24hr python $path1/querydict_latest3.py $line $path/trfMaskChrom/$m5$ver $path/all_m5/bin_out4/$m5 2\>\&1
      done 
     echo "done submitting $m5"
     i=$(( i + 1 ))
     echo $i
   else
     echo "Sleeping..."
     sleep 2h 
   fi
 done
