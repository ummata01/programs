#!/usr/bin/env python
import os
import sys
sys.path.insert(0, '/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/pHmm')
sys.path.insert(0, '/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/psCount')
import numpy as np
from io_hupac.ReadMatcherIO import parseRm5
from model.AlignmentHit import AlignmentHit
from io_hupac.binFileIO import parsebinf
from model.binLine import binLine
from model.Sequence import revcomp as rc
import time
import multiprocessing as mp
#import model.dpFuncs_sw as dpF
import dpFuncs_sw2 as dpF
from mleHmm import countsOnList as cL
from mleHmm import getGlobalProb as load
from pHmmFwdLog2_cdfLB import pairHmmRun 

#GLOBAL VARIABLE
#fastaDict = {}
#binData = {}

def dictforbindata(binData,binfile):
    for line in parsebinf(binfile):
        binData.setdefault(line.query_id,[]).append(line)
    print "Length of binData: ", len(binData)

def dictforqueries(queryAligns,m5file):
    for hit in parseRm5(m5file):
        queryAligns[hit.query_id] = hit

def tb_prefix(query,prefix,P,P_p,iPre):
    pre = False
    j = np.size(prefix)
    i = iPre
    qSeq = map(chr,query)
    preSeq = map(chr,prefix)
    qList = []
    preList= []
    jLessThanZero = False
    while(j > 0):
      if j > 0: 
         if (P[i][j] > 0):
            if (P_p[i][j] == 0):
               qList.append(qSeq[i-1])
               preList.append(preSeq[j-1])
               i += -1
               j += -1
            if (P_p[i][j] == 1):
               qList.append(qSeq[i-1])
               preList.append("-")
               i += -1
            if (P_p[i][j] == 2):
               qList.append("-")
               preList.append(preSeq[j-1])
               j += -1
         else:
            jLessThanZero = True
            break
      else:
         jLessThanZero = True
         break
    if jLessThanZero == False:
       preList.reverse()
       qList.reverse()
       q_pre_Aligned = []
       q_pre_Aligned.append(''.join(preList))
       q_pre_Aligned.append(''.join(qList))
    else:
       q_pre_Aligned = ['','']
    return q_pre_Aligned

def tb_suffix(query,suffix,S,S_p):
    temp = 0
    i = 0
    j = np.size(suffix)
    nq = np.size(query)
    qSeq = map(chr,query)
    sufSeq = map(chr,suffix)
    qList = []
    sufList= []
    for k in range(nq+1): # Finds the maximal value for the suffix to begin tb
        if temp < S[k][j]:
           temp = S[k][j]
           i = k
    bestscoresuffix = temp
    pre = False
    index_i_tR = 0
    jLessThanZero = False
    while (not(pre)):
      if j > 0:
         if (S[i][j] > 0):
            if (S_p[i][j] == 1):
               qList.append(qSeq[i-1])
               sufList.append("-")
               i += -1
            elif (S_p[i][j] == 2):
               qList.append("-")
               sufList.append(sufSeq[j-1])
               j += -1
            elif (S_p[i][j] == 0):
               qList.append(qSeq[i-1])
               sufList.append(sufSeq[j-1])
               i += -1
               j += -1
            elif (S_p[i][j] == 3):
               qList.append(qSeq[i-1])
               sufList.append(sufSeq[j-1])
               j += -1
               i += -1
               pre = True
            index_i_tR = i
         else :
            index_i_tR = 0
            jLessThanZero = True
            break
      else:
         index_i_tR = 0
         jLessThanZero = True
         break
    if jLessThanZero == False:
       sufList.reverse()
       qList.reverse()
       q_suf_Aligned = []
       q_suf_Aligned.append(''.join(sufList))
       q_suf_Aligned.append(''.join(qList))
       #print "q_suf_Aligned:\n",q_suf_Aligned[0],"\n",q_suf_Aligned[1] 
    else:
       q_suf_Aligned = ['','']
    return index_i_tR, bestscoresuffix,q_suf_Aligned

def finalTrace(qArray,tR,T,T_p,tR_rowMax,index_i_tR):
    i = index_i_tR
    #print "i index in TR: ", i
    if (i > 0):
      j = tR_rowMax[i][1]
      scoretRentry = T[i][j]
      t = len(tR)
      pre = False
      trScoreNeg = False
      query = map(chr,qArray)
      qList = []
      tList = []
      while (not (pre)):
        if j > 0:
          if (T[i][j] > 0):
            if (T_p[i][j] == 0):
               qList.append(query[i-1])
               tList.append("-")
               i += - 1
            elif (T_p[i][j] == 1):
               qList.append("-")
               tList.append(tR[j-1])
               j += - 1
            elif (T_p[i][j] == 2):
               qList.append(query[i-1])
               tList.append(tR[j-1])
               i += - 1
               j += - 1
            elif (T_p[i][j] == 3):
               qList.append(query[i-1])
               tList.append(tR[j-1])
               for k in range(0,j-1): #open up gaps on query
                   qList.append("-")
                   tList.append(tR[k])
               i += - 1
               j = t
            elif (T_p[i][j] == 4):
               qList.append(query[i-1])
               tList.append(tR[j-1])
               i += - 1
               pre = True
               trSinQ = i
               scoretRexit = T[i+1][j]
          else:
              trScoreNeg = True
              print "Tandem repeat score <= 0"
              break
        else:
            trScoreNeg = True
            break
      #print "trScoreNeg: ",trScoreNeg
      if trScoreNeg == False:
         #print "tandem repeat score > 0"
         tList.reverse()
         qList.reverse()
         repeatAligned = []
         repeatAligned.append(''.join(tList))
         repeatAligned.append(''.join(qList))

         tempTList = []
         for i in range(0, len(tList)):
            if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
               tempTList.append(tList[i])

         tRLength = len(tempTList)
         str_tR = "".join(tR)
         str_tempTList = "".join(tempTList)
         if (t < tRLength):
              if t >= 1:
                 number_tR = (tRLength)/float(t)
                 naiveTR = index_i_tR - trSinQ
              else:
                 number_tR = -30000.0
                 naiveTR = -30000.0
                 repeatAligned = ['','']
                 scoretRentry = 0
                 scoretRexit = 0
         else:
           number_tR = -20000.0
           naiveTR = -20000.0
           repeatAligned = ['','']
           scoretRentry = 0
           scoretRexit = 0
      else:
          number_tR = -40000.0
          naiveTR = -40000.0
          repeatAligned = ['','']
          scoretRentry = 0
          scoretRexit = 0
    else:
      number_tR = -10000.0
      naiveTR = -10000.0
      repeatAligned = ['','']
      scoretRentry = 0
      scoretRexit = 0
    return number_tR, repeatAligned, scoretRentry, scoretRexit, naiveTR
 
def alignRegions(query,pre_suf_tR):
    '''INPUT: 
    query - query sequence
    pre_suf_tR = [<prefix_seq>, <suf_seq>, <tr_seq>]
    RETURNS:  
    number_tR = number of tandem repeats in query
    repeatAligned = [<TR align sequence, ref>, <TR align sequence, query>]
    Prefix_BestScore = Smith-waterman score of Prefix Score matrix
    Suffix_BestScore = SW score in suffix matrix (overall: Prefix+TR+suffix)
    tR_maxScore = SW score in tR matrix (Prefix + TR)
    tR_exitScore = SW score in tR matrix where alignment jumps to prefix matrix
    '''
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
    pArray = np.arange(len_prefix,dtype=np.int_)
    tempP = map(ord,prefix)
    tempP2 = np.asarray(tempP)
    pArray = tempP2
#    print "pArray: ", pArray

    suffix = []
    suffix = list(pre_suf_tR[1])
    len_suffix = len(suffix)
    sArray = np.arange(len_suffix,dtype=np.int_)
    tempS = map(ord,suffix)
    tempS2 = np.asarray(tempS)
    sArray = tempS2
#    print "sArray: ", sArray

    tR = []
    tR = list(pre_suf_tR[2])
    len_tR = len(tR)
    tRArray = np.arange(len_tR,dtype=np.int_)
    temptR = map(ord,tR)
    temptR2 = np.asarray(temptR)
    tRArray = temptR2
#    print "tRArray: ", tRArray

    preScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    # Added in initialization for prefix
    preMaxScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    preMaxScore.fill(-1)
    for j in xrange (1, len_prefix+1):
        preScore[0][j] = -5*j

    Prefix_BestScore = 0
    Prefix_BestScoreIndex = 0
    #Prefix_BestScore = dpF.sw_prefix(query,pArray,preScore,preMaxScore)
    Prefix_BestScore, Prefix_BestScoreIndex = dpF.sw_prefix(query,pArray,preScore,preMaxScore)
    #print "Prefix_BestScoreIndex: ",Prefix_BestScoreIndex
    # Added in initialization for tR
    tRScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)
    for j in xrange(1,len_tR+1):
        tRScore[0][j] = -10000000

    tRMaxScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)-1
    tR_rowMax = np.zeros((len(query)+1,2),dtype=np.int_)
    dpF.sw_tR_simple(query,tRArray,tRScore,tRMaxScore,len_prefix,preScore,tR_rowMax)

    index_i_tR = 0
    Suffix_BestScore = 0
    # Added in initialization for suffix
    sufScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)
    for j in xrange(1,len_suffix+1):
        sufScore[0][j] = -10000000

    sufMaxScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)-1
    dpF.sw_suffix(query,sArray,sufScore,sufMaxScore,tR_rowMax)
    index_i_tR,Suffix_BestScore,alignSufxQ = tb_suffix(query,sArray,sufScore,sufMaxScore)

    number_tR = 0.0
    naiveTR = 0.0
    tR_maxScore = 0
    repeatAligned = []
    #print "Entering finalTrace for query: "
    number_tR, repeatAligned, tR_maxScore, tR_exitScore, naiveTR = finalTrace(query,tR,tRScore,tRMaxScore,tR_rowMax,index_i_tR)
    #print "Done final Trace"
    #Here goes the prefix_tracebacks
    alignPrfxQ = tb_prefix(query,pArray,preScore,preMaxScore,Prefix_BestScoreIndex)
    #here query is in int, but tR is in string representation
    print "exit finalTrace..."
    return number_tR, repeatAligned, Prefix_BestScore, Suffix_BestScore, tR_maxScore, tR_exitScore, alignSufxQ, alignPrfxQ, naiveTR

#####

def calculateRegions_tR(flag,target, rev_target, trf_start, trf_end, target_start, target_end,refTrf_seq, flankLen):
    listPreSuf_tR = []
    tempTarget = []
    prefix_tR = ""
    suffix_tR = ""
    #print "trf_start", trf_start
    #print "target_start", target_start
    #print "trf_end", trf_end
    #print "target_end", target_end

    lengthOfPreX = trf_start - target_start
    lengthOfSufX = target_end - target_start
    startSufX = trf_end - target_start
    len_target = len(target)
    len_revtarget = len(rev_target)

    #print "startSufX ", startSufX
    #print "lengthofSufX ",lengthOfSufX
    #print "lengthofPreX ",lengthOfPreX
    if (len_target < lengthOfSufX):
       listPreSuf_tR.append('E')
    else:
       if flag == True:
          #print "I am in flag = TRUE"
          tempRevTarget = rev_target
          rev_prefix_tR = []
          for j in xrange(0,lengthOfPreX):
              rev_prefix_tR.append(tempRevTarget[j])
          suffix_tR = rc(''.join(rev_prefix_tR))
          #print "suffix_tR: ",suffix_tR
          rev_suffix_tR = []
          for k in xrange(startSufX, lengthOfSufX):
              rev_suffix_tR.append(tempRevTarget[k])
          prefix_tR = rc(''.join(rev_suffix_tR))
          #print "prefix_tR: ",prefix_tR

          refTrf_seq = rc(refTrf_seq)
          #print "refTrf_seq: ",refTrf_seq
       else:
          #print "I am in flag = false"
          tempTarget = target
          #print " len of tempTarget ", len(tempTarget) 
          prefix_tR = []
          for j in xrange(0,lengthOfPreX):
              prefix_tR.append(tempTarget[j])

          suffix_tR = []
          for k in xrange(startSufX, lengthOfSufX):
              suffix_tR.append(tempTarget[k])
          prefix_tR = ''.join(prefix_tR)
          suffix_tR = ''.join(suffix_tR)
       #print "prefix_tR:\n",prefix_tR
       #print "prefix_tR[-flankLen:]:\n",prefix_tR[-flankLen:]
       #print "suffix_tR:\n",suffix_tR
       #print "suffix_tR[0:flankLen]:\n",suffix_tR[0:flankLen]
       #print "refTrf_seq:\n",refTrf_seq
       listPreSuf_tR.append(prefix_tR[-flankLen:])
       #listPreSuf_tR.append(prefix_tR)
       listPreSuf_tR.append(suffix_tR[0:flankLen])
       #listPreSuf_tR.append(suffix_tR)
       listPreSuf_tR.append(refTrf_seq)
    #print listPreSuf_tR
    return listPreSuf_tR


def align_worker(ListOfQs,m5Dict,binData,fn):
    #print "worker starts for List of query: "
    start1 = time.time()
    FinalList = []
    #print ListOfQs
    for query in ListOfQs:
        hit = m5Dict[query]
        if hit.query_id in binData:
           tempLine = binLine() #HAVE TO CHECK IF THE ALIGNED REGION IN THE QUERY IS AROUND THE TR EVENT. This is because if m5 were not generate with best n 1, one can have alternative alignments as well...
           loq = len(hit.QuerySeq)
           qSeq = ''.join(hit.QuerySeq)
           qArray = np.arange(loq,dtype=np.int_)
           tempQ = map(ord,hit.QuerySeq)
           tempQ2 = np.asarray(tempQ)
           qArray = tempQ2
           pre_suf_tR = []
           flag = False # assume its in positive strand
           if hit.target_strand == 0:
              flag = True #1 means its the negative strand
           numInList = len(binData[hit.query_id])
           #print "number of entries in list: ",numInList
           for k in range(0,numInList):
               tempLine = binData[hit.query_id][k]
               if tempLine.prefixFlank >= 100 and tempLine.suffixFlank >= 100:
                   pre_suf_tR = calculateRegions_tR(flag,hit.TargetSeq,hit.RevTargetSeq,tempLine.refTrf_st,tempLine.refTrf_en,hit.target_start,hit.target_end,tempLine.refTrf_seq,flankLen)
                   #print pre_suf_tR, len(pre_suf_tR)
                   if (len(pre_suf_tR)) == 1:
                      tempStr = "query has weird m5 alignment (check m5 file alignment)\n"
                      outList = [hit.query_id, tempStr]
                      #print "outList:\n",outList
                   else:
                      number_tR, repeatAligned, PreS, SufS, tR_S, tR_Ex, qSufAligned, qPreAligned, naiveTR = alignRegions(qArray, pre_suf_tR)
                      print "Done alignRegions"
                      #print "len of qSufAligned ",len(qSufAligned[1]), " len of qPreAligned ", len(qPreAligned[1])
                      if repeatAligned[1] != '': #Check if TR in q exists 
                         tList = repeatAligned[1]
                         tempTList = []
                         for i in range(0, len(tList)):
                             if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
                                tempTList.append(tList[i])
                         trInQseq = ''.join(tempTList)
                         #print "trInQseq: ",trInQseq
                         preSeqInQ = ''.join([x if x != '-' else '' for x in qPreAligned[1]])
                         sufSeqInQ = ''.join([x if x != '-' else '' for x in qSufAligned[1]])
                         #print " len of preSeqInQ ", len(preSeqInQ), "\nlen of sufSeqInQ ", len(sufSeqInQ)
                         if len(preSeqInQ) >= 100 and len(sufSeqInQ) >= 100: #flanking prefix and suffix length are more than 100. Perform both Local and Global pHmm calculations
                            preFlank = preSeqInQ[-95:]
                            sufFlank = sufSeqInQ[0:95]
                            listOfAligned = [qSufAligned, qPreAligned]
                            tProb, eProb_M, eProb_X = cL(listOfAligned) #Local Parameters for pHmm
                            tProb1, eProb_M1, eProb_X1 = load(hit.target_id) #Global Parameters for pHmm
                            print "GLocal pHmm start..."
                            numTRL, llrL, ProbMaxL, SamplesL, lowerBoundL, listOfDevL = pairHmmRun(trInQseq,pre_suf_tR[2],tProb,eProb_M,eProb_X)
                            #print "Local psCount pHmm End"
                            #print "Global psCount pHmm start..."
                            numTRG, llrG, ProbMaxG, SamplesG, lowerBoundG, listOfDevG = pairHmmRun(trInQseq,pre_suf_tR[2],tProb1,eProb_M1,eProb_X1)
                            #print "Global psCount pHmm End"
                            print "number_tR: ",number_tR," numTRL: ",numTRL," numTRG: ",numTRG
                            if ProbMaxL > ProbMaxG: #Check which run of pHmm gives higher probability of P(O|lambda). Select those parameters respectively 
                               numTR,llr,ProbMax,Samples,lowerBound,listOfDev = numTRL, llrL, ProbMaxL, SamplesL, lowerBoundL, listOfDevL
                            else:
                               numTR,llr,ProbMax,Samples,lowerBound,listOfDev = numTRG, llrG, ProbMaxG, SamplesG, lowerBoundG, listOfDevG
                         else: #Don't want q which don't span the TR event greater than 100 base pairs
                            preFlank, sufFlank = '', ''
                            numTR, llr, ProbMax, Samples, lowerBound, listOfDev = -1,0,-1.0*float('inf'),[],0,[]
                      else: #TR is q doesn't exists...pass default values
                         numTR, llr, ProbMax, Samples, lowerBound, listOfDev = -1,0,-1.0*float('inf'),[],0,[]
                         repeatAligned = ["Error","Error"]
                         preFlank, sufFlank = '', '' #We don't want pre and suf in q sequences if there is no TR in q detected
                      outList = [hit.query_id,hit.target_id,hit.target_start,hit.target_end,pre_suf_tR[2],number_tR,tempLine.refTrf_copyNum,PreS,SufS,tR_S,tR_Ex,repeatAligned[0],repeatAligned[1],tempLine.refTrf_st,tempLine.refTrf_en,tempLine.refTrf_repeat,naiveTR/float(tempLine.refTrf_repeat),numTR,llr,ProbMax,Samples,lowerBound,listOfDev,preFlank,sufFlank]
                   FinalList.append(outList)
               else:
                   tempStr = "query flanks around TR event less than 100 bp\n"
                   outList = [hit.query_id, tempStr]
                   FinalList.append(outList)
        else:
           tempStr = "query does not exist in binned file\n"
           outList = [hit.query_id, tempStr]
           FinalList.append(outList)
    end1 = time.time()
    print "Duration within worker: ",end1-start1
    #print "len(FinalList): ",(FinalList)
    writeFile(FinalList,fn)
    #print "worker ends for query" 
    #return FinalList

def writeFile(x,fn):
    outfile = open(fn,'w') 
    for result in x:
        strResult = ' '.join(map(str,result))
        outfile.write("%s\n"%(strResult))
        outfile.flush()
    outfile.close()


if __name__ == '__main__': 
    #'''USAGE: python pacMonStr_V0.py <>.m5 <>.binned 1000 <numCores> <outputDirectory>'''
    #input 1: RESULT.binned file
    start = time.time()
    inm5fn = sys.argv[1]
    tempstr = inm5fn.rstrip("\n").split("/")
    #outStr = "/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/3dp/" + tempstr[-1] 
 
    ver = '.PacV0'
    outdir = sys.argv[5]
    outfn = tempstr[-1].split(".")[0] + ver
    outStr = outdir + "/" + outfn + ".out"
    #FOR debugging change here:
    #outStr = "/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/3dp/test3.out"

    inbinfn = sys.argv[2]

    #input 3: length of region flanking TR to align to 
    flankLen = int(sys.argv[3])
    #input 4:  number of cpus to use
    ncores = int(sys.argv[4])
    binData = {}
    dictforbindata(binData,inbinfn)
    #binData is a global dictionary
    print "No of entries in binData dict: ",len(binData)

    #Create a dictionary with <query_id:Sequence> entries
    #this should be used if the user supplies the queries in fasta and another file with prefix suffix data for a specific query
    #create_fastaDict(fastaDict,inFafn)

    #Create a dictionary with <query_id:filtered queries for a TR event with its relevant information> entries
    ListOfQueryAligns = []
    queryAligns = {}
    dictforqueries(queryAligns,inm5fn)
    NumOfQ = len(queryAligns)
    print "No of entries in queryAligns dict: ", NumOfQ
    num = int(NumOfQ/(NumOfQ*0.05)) # 5% of number of queries, so around 100
    if NumOfQ > num:
       chunkDict = int(NumOfQ/num)
    else:
       chunkDict = 1
    ListOfQueries = queryAligns.keys()
    chunkedListQueries = [ListOfQueries[i::chunkDict] for i in range(chunkDict)]  
    #print "Length of chunkedListQueries: ", (chunkedListQueries)
    #form a dictionary with the *.txt file. This dictionary could be formed everytime when a new query is started.
    pool = mp.Pool(ncores)
    for i in range(0,len(chunkedListQueries)):
         fn = outStr +"_" +str(i+1)
         pool.apply_async(align_worker,(chunkedListQueries[i],queryAligns,binData,fn))
    pool.close()
    pool.join()
    print "Done...exiting 3dpHmm"
