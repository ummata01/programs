#!/usr/bin/env python
import os
import sys
import numpy as np
from io_hupac.ReadMatcherIO import parseRm5
from model.AlignmentHit import AlignmentHit
import countSeq2 as cS
import time

def countsOnM5(file):
    tempstr = file.rstrip("\n").split("/")
    print tempstr
    outfn = tempstr[-1]
    outdir = tempstr[-2]
    dirSave = sys.argv[2].split()[0]

    fnRoot = dirSave + "/" + outfn
    start = time.time()
    countsTotal = np.zeros((5,5))
    countsTotalEmm = np.zeros((4,4))
    transTotal = np.zeros((3,3))
    gammaTotal = np.zeros((3))
    for hit in parseRm5(file):
        alnQ,alnT = hit.getAligned()
        #print "alnT:\n",alnT
        #print "alnQ:\n",alnQ
        nq = len(alnQ)
  	q = np.arange(nq,dtype=np.int_)
    	tempQ = map(ord,alnQ)
    	q = np.asarray(tempQ)
        nt = len(alnT)
        t = np.arange(nt,dtype=np.int_)
        tempT = map(ord,alnT)
        t = np.asarray(tempT)
        countsLocal,transLocal,countsLocalEmm,gammaLocal = cS.getCountsFromAln(q,nq,t,nt)
	countsTotal = np.add(countsLocal,countsTotal)
	countsTotalEmm = np.add(countsLocalEmm,countsTotalEmm)
        transTotal = np.add(transLocal,transTotal)
        gammaTotal = np.add(gammaLocal,gammaTotal)
    #print "countsTotal:\n",countsTotal
    #print "countsTotalEmm:\n",countsTotalEmm

    tProb = cS.getTransProb(transTotal)
    #print "tProb:\n",tProb
    fn = fnRoot + '.tProb.txt'
    np.savetxt(fn,tProb)    

    ProbInit = cS.getInitProb(gammaTotal)
    #print "InitProb:\n", ProbInit
    fn = fnRoot + '.InitProb.txt'
    np.savetxt(fn,ProbInit)    

    eProbM = cS.new_eProb(countsTotal)
    #print "eProb in match state:\n",eProbM
    fn = fnRoot + '.eProbM.txt'
    np.savetxt(fn,eProbM)    

    eProbX = cS.new_eProb(countsTotalEmm)
    #print "eProb in ins state:\n",eProbX
    fn = fnRoot + '.eProbX.txt'
    np.savetxt(fn,eProbX)    
    
    end = time.time()
    print "duration in code: ",end-start

def getInitProb(I):
    ip = np.zeros((3))
    sum_k = I[0]+I[1]+I[2]
    for i in range(0,3):
        if sum_k != 0.0:
           ip[i] = I[i]/sum_k
    return ip

def new_eProb(emm):
    eProb_bw = np.zeros((4,4))
    sum_k = np.zeros((4))
    for i in range(0,4):
        for j in range(0,4):
            sum_k[i] += emm[i,j]
    for i in range(0,4):
        for j in range(0,4):
            if sum_k[i] != 0.0:
               eProb_bw[i,j] = emm[i,j]/sum_k[i]
    return eProb_bw

def getCountsFromAln(q,nq,t,nt):
    #0=m,1=x,2=y (on both axis)
    trans = np.zeros((3,3))
    #0=A,1=C,2=T,3=G,4='-' (on both axis)
    countsLocal = np.zeros((5,5))
    #0=A,1=C,2=T,3=G (on both axis)
    countsLocalEmm = np.zeros((4,4))
    gamma = np.zeros((3))
    if nt == nq:
       for i in range(0,nq-1):
           q_i = getindicies(q[i])
           t_i = getindicies(t[i])
           q_i1 = getindicies(q[i+1])
           t_i1 = getindicies(t[i+1])
           countsLocal[q_i,t_i] += 1
           if i == 0:
              if  (q_i != 4) and (t_i != 4):
                  gamma[0] += 1
              if  (q_i == 4) and (t_i != 4):
                  gamma[2] += 1
              if  (q_i != 4) and (t_i == 4):
                  gamma[1] += 1
           if  (q_i != 4) and (t_i != 4):# i,j was match state
               if  (q_i1 != 4) and (t_i1 != 4): #i+1,j+1 is a match
    		   trans[0,0] += 1
               if  (q_i1 != 4) and (t_i1 == 4): #i+1 is an insertion
    		   trans[0,1] += 1
                   countsLocalEmm[q_i,q_i1] += 1
               if  (q_i1 == 4) and (t_i1 != 4): #i+1 is a deletion
    		   trans[0,2] += 1
           if  (q_i == 4) and (t_i != 4):# i,j was deletion state
               if  (q_i1 != 4) and (t_i1 != 4):
    		   trans[2,0] += 1
               if  (q_i1 != 4) and (t_i1 == 4):
    		   trans[2,1] += 1
                   countsLocalEmm[q_i,q_i1] += 1
               if  (q_i1 == 4) and (t_i1 != 4):
    		   trans[2,2] += 1
           if  (q_i != 4) and (t_i == 4):# i,j was insertion state
               if  (q_i1 != 4) and (t_i1 != 4):
    		   trans[1,0] += 1
               if  (q_i1 != 4) and (t_i1 == 4):
    		   trans[1,1] += 1
                   countsLocalEmm[q_i,q_i1] += 1
               if  (q_i1 == 4) and (t_i1 != 4):
    		   trans[1,2] += 1
    return countsLocal, trans, countsLocalEmm, gamma

def getTransProb(T):
    tProb = np.zeros((3,3))
    sumT = T.sum(axis=1)
    tProb[0,0] = T[0,0]/sumT[0]
    tProb[0,1] = T[0,1]/sumT[0]
    tProb[0,2] = T[0,2]/sumT[0]
    tProb[1,0] = T[1,0]/sumT[1]
    tProb[1,1] = T[1,1]/sumT[1]
    tProb[1,2] = T[1,2]/sumT[1]
    tProb[2,0] = T[2,0]/sumT[2]
    tProb[2,1] = T[2,1]/sumT[2]
    tProb[2,2] = T[2,2]/sumT[2]
    return tProb
 
def getindicies(q):
    if q == 65: #its an A
       qi = 0
    if q == 67: #its a C
       qi = 1
    if q == 84: #its a T
       qi = 2
    if q == 71: #its a G
       qi = 3
    if q == 45: #its a '-'
       qi = 4
    return qi	


countsOnM5(sys.argv[1])

