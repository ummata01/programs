#!/usr/bin/env python
import numpy as np
import math
cimport numpy as np
cimport cython

np.import_array()

cdef extern from "math.h" nogil:
     long double exp(long double)
     long double log(long double) 

def forwardProbLog(np.ndarray[np.int_t,ndim=1] q, int nq, np.ndarray[np.int_t,ndim=1] r, int nr, np.ndarray[np.float_t,ndim=2] Lf_M, np.ndarray[np.float_t,ndim=2] Lf_X, np.ndarray[np.float_t,ndim=2] Lf_Y, np.ndarray[np.float_t,ndim=2] eProb_M, np.ndarray[np.float_t,ndim=2] eProb_X, np.ndarray[np.float_t,ndim=1] eProb_Y, np.ndarray[np.float_t,ndim=2] tProb):

    #Variable Declarations
    cdef int i
    cdef int j
    cdef int qi
    cdef int rj
    cdef double emm

    #Initialization
    Lf_M[0,0], Lf_X[0,0], Lf_Y[0,0] = np.log(tProb[3,0]), np.log(tProb[3,1]), np.log(tProb[3,2])

    #Recurrsion
    for i in range(1,nq+1):
        qi = getindicies(q[i-1]) #seq in q at i position concantenated with a gap
        if i == 1:
           emm = 0.25
           Lf_X[i,0] = log(emm) + Lf_X[i-1,0]
        else:
           qi_1 = getindicies(q[i-2])
           emm = eProb_X[qi,qi_1]
           Lf_X[i,0] = log(emm) + log(tProb[0,1]*exp(Lf_M[i-1,0]) + tProb[1,1]*exp(Lf_X[i-1,0]) + tProb[2,1]*exp(Lf_Y[i-1,0]))

    for j in range(1,nr+1):
        rj = getindicies(r[j-1]) #seq in r at j position concantenated with a gap
        if j == 1:
           Lf_Y[0,j] = log(eProb_Y[rj]) + Lf_Y[0,j-1]
        else:
           Lf_Y[0,j] = log(eProb_Y[rj]) + log(tProb[0,2]*exp(Lf_M[0,j-1]) + tProb[2,2]*exp(Lf_Y[0,j-1]) + tProb[1,2]*exp(Lf_X[0,j-1]))

    for j in range(1,nr+1):
        for i in range(1,nq+1):
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            if i == 1:
               emm = 0.25
            else:
               qi_1 = getindicies(q[i-2])
               emm = eProb_X[qi,qi_1]
            if i == 1 and j == 1:
               Lf_M[i,j] = log(eProb_M[qi,rj]) + Lf_M[i-1,j-1]
            else:
               Lf_M[i,j] = log(eProb_M[qi,rj]) + log(tProb[0,0]*exp(Lf_M[i-1,j-1]) + tProb[1,0]*exp(Lf_X[i-1,j-1]) + tProb[2,0]*exp(Lf_Y[i-1,j-1]))
            Lf_X[i,j] = log(emm) + log(tProb[0,1]*exp(Lf_M[i-1,j]) + tProb[1,1]*exp(Lf_X[i-1,j]) + tProb[2,1]*exp(Lf_Y[i-1,j]))
            Lf_Y[i,j] = log(eProb_Y[rj]) + log(tProb[0,2]*exp(Lf_M[i,j-1]) + tProb[1,2]*exp(Lf_X[i,j-1]) + tProb[2,2]*exp(Lf_Y[i,j-1]))

    return Lf_M,Lf_X,Lf_Y

cdef inline int getindicies(int q):
    cdef int qi
    if q == 65: #its an A
       qi = 0
    if q == 67: #its a C
       qi = 1
    if q == 84: #its a T
       qi = 2
    if q == 71: #its a G
       qi = 3
    return qi

def numTRlog(np.ndarray[np.float_t,ndim=2] Lf_M, np.ndarray[np.float_t,ndim=2] Lf_X, np.ndarray[np.float_t,ndim=2] Lf_Y, int ntr, int nr, int nq):
    cdef np.ndarray[np.float_t,ndim=1] LendAt = np.zeros((nr+1),dtype=np.double)
    cdef int j
    cdef long double LallW = 0.0
    cdef long double num = 0.0
    cdef long double temp = -9999.0
    cdef long double MaxfwdProb = 0.0
    cdef long double ProbRandom = 1.0
    cdef int index = 0
    cdef long double llr = 1.0
    cdef long double sumL = 0.0
    for j in range(1,nr+1):
        if j%ntr == 0:
           LendAt[j] = log(exp(Lf_M[nq,j])+exp(Lf_X[nq,j])+exp(Lf_Y[nq,j]))
           #LendAt[j] = Lf_M[nq,j]+log(1.0+exp(Lf_X[nq,j] - Lf_M[nq,j])+exp(Lf_Y[nq,j] - Lf_M[nq,j])) #This is wrong...
           if LendAt[j] >= temp:
              temp = LendAt[j]
              index = j
    LMaxfwdProb = temp
    LProbRandom = randomModel(nq,index)
    print "Max forward probability in Log: ",LMaxfwdProb
    print "Occurs at index: ",index
    print "LProbRandom: ", LProbRandom

    llr = LMaxfwdProb - LProbRandom
    print "Log-Odds ratio Score: ", llr

    if (llr > 0.0):
       for j in range(1,nr+1):
           if j%ntr == 0:
              sumL += exp(LendAt[j])
       LallW = log(sumL)
       print "All W sum in Log: ",(LallW), "sumL: ",sumL
       for j in range(1,nr+1):
           if j%ntr == 0:
              num = num + (exp(LendAt[j] - LallW))*(j/ntr)
    else:
       num = 0.0
       print "Log-Odds ratio Score < 0.0"
    return num, llr, index  
 
cdef inline long double randomModel(int nq,int nr):
    cdef long double eta = 0.05
    cdef long double g = 0.25 #prob of emitting a base = 0.25 for each base in query sequence.
    cdef long double pqr = 1.0
    cdef long double LprobRand = 1.0
    LprobRand = 2.0*log(eta) + (nq+nr+1)*log(1.0-eta) + (nq+nr)*log(g)
    #print "LprobRand: ",LprobRand
    return LprobRand
