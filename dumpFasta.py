#!/usr/bin/env python

import sys
import os


def create_fastaDict(fastaDict,file):
    fastaIn = open(file,'r')
    fastaLine = fastaIn.readline()
    while (fastaLine):
          qidFin = fastaLine
          qidF = qidFin.strip().split(">")[1]
          seq = fastaIn.readline().strip()
          fastaDict.setdefault(qidF,[]).append(seq)
          fastaLine = fastaIn.readline()
    print "FastaDictionary formed, with number of queries: ",len(fastaDict)

def run():
    infafn = sys.argv[1] #input fasta file
    fastaDict = {}
    create_fastaDict(fastaDict,infafn)

    inqid = sys.argv[2] #input query_id file

    outfn = sys.argv[3] #output file name
    outfile = open(outfn,'w')
    for line in open(inqid,'r'):
        q = line.rstrip("\n") 
        #print "q: ", q         
        if q in fastaDict:
           seq_q = fastaDict[q][0]
           outfile.write(">%s\n"%(q))
           outfile.write("%s\n" %(seq_q))
           outfile.flush()
    outfile.close()


run()

