#!/usr/bin/env python

#Given a fasta file and number of sequences to output per split, this program splits the original Fasta file with 300 sequences each
#USAGE: python splitFaToFa.py <source_FastaFile> <number of sequence needed per file> <destination folder> 
#OUTPUT: Fasta files with 300 sequences per file

import sys
import os

def splitFastaToManyFasta(fastaFile):
    split = int(sys.argv[2]) #number of split Fasta files required
    where = sys.argv[3] #directory to write split fasta file
    try:
       fastaIn = open(fastaFile,'r')
    except:
       print fastaFile, " not Found!"
    if split > 0:
       fastaLine = fastaIn.readline()
       fs = 0
       while (fastaLine):
                num = 1
                seqline = fastaLine
                data = []
                while (num <=split and seqline):
                    if (seqline.find(">") == 0):
                       data.append(seqline)
                       num += 1
                       seqline = fastaIn.readline()
                    while (seqline and seqline.find(">") == -1):
                          data.append(seqline)
                          seqline = fastaIn.readline()
                fs += 1
                fn = where +"/" +str(fs) + "ctg.fasta"
                out = open(fn,'w')
                out.write("%s"%(''.join(data)))
                fastaLine = seqline
    else:
       print "Split > 0 please! Split = 0, implies original file!"
    fastaIn.close()

splitFastaToManyFasta(sys.argv[1]) #input fasta file
