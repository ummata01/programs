#!/usr/bin/env python
import sys
import os
import numpy as np
#import phmmfwd as pf

def forwardProb(q,nq,r,nr,f_M,f_X,f_Y,eProb,tProb):
    #Computational complexity of O(T*N**2), where T = len of observation seq and N = num of hidden states
    #Initialization of the probability matrices
    f_M[0,0], f_X[0,0], f_Y[0,0] = tProb[3,0], tProb[3,1], tProb[3,2] #assuming we start in the Match state, which is not generalized situation.
   
    #first j = 0 case (all the j-1 terms are 0.0 except for in X state):
    for i in range(1,nq+1):
 	qi = getindicies(q[i-1]) #seq in q at i position concantenated with a gap
        f_X[i,0] = eProb[qi,4]*(tProb[0,1]*f_M[i-1,0] + tProb[1,1]*f_X[i-1,0])
        f_M[i,0] = 0.0
        f_Y[i,0] = 0.0
    #then i = 0 case:
    for j in range(1,nr+1):
 	rj = getindicies(r[j-1]) #seq in r at j position concantenated with a gap
        f_Y[0,j] = eProb[4,rj]*(tProb[0,2]*f_M[0,j-1] + tProb[2,2]*f_Y[0,j-1])
        f_M[0,j] = 0.0 
        f_X[0,j] = 0.0 
    #Recursion for i = 1,...,nq and j = 1,...,nr:
    for j in range(1,nr+1):
        for i in range(1,nq+1):
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            f_M_i_j = eProb[qi,rj]*(tProb[0,0]*f_M[i-1,j-1] + tProb[1,0]*f_X[i-1,j-1] + tProb[2,0]*f_Y[i-1,j-1])
   	    f_X_i_j = eProb[qi,4]*(tProb[0,1]*f_M[i-1,j] + tProb[1,1]*f_X[i-1,j]) 
   	    f_Y_i_j = eProb[4,rj]*(tProb[0,2]*f_M[i,j-1] + tProb[2,2]*f_Y[i,j-1])
            sum_all_state_i_j = 1 
            f_M[i,j] = f_M_i_j/sum_all_state_i_j
            f_X[i,j] = f_X_i_j/sum_all_state_i_j
            f_Y[i,j] = f_Y_i_j/sum_all_state_i_j
    return f_M,f_X,f_Y

def getindicies(q):
    if q == 65: #its an A
       qi = 0
    if q == 67: #its a C
       qi = 1
    if q == 84: #its a T
       qi = 2
    if q == 71: #its a G
       qi = 3
    return qi

def numTR(f_M,f_X,f_Y,ntr,nr,nq):
    endAt = np.zeros((nr+1),dtype=np.float)
    RandProb = np.zeros((nr+1),dtype=np.float)
    temp = 0.0
    index = 0
    for j in range(1,nr+1):
        if j%ntr == 0:
           m = f_M[nq,j]
           x = f_X[nq,j]
           y = f_Y[nq,j]
           endAt[j] = m+x+y
           if endAt[j] >= temp:
              temp = endAt[j]
              index = j
    MaxfwdProb = temp 
    ProbRandom = randomModel(nq,index)
    print "Max forward probability: ",MaxfwdProb
    llr = np.log(MaxfwdProb/ProbRandom)
    #computing log-odds ratio from max. fwd prob. of hmm w.r.t the random model
    print "Log-Odds ratio Score: ", llr
    #This is the measure of the likelihood that two sequences, occuring as aligned pairs, are related by any alignment, as opposed to being unrelated!
    if (llr > 0.0):
       allW = np.sum(endAt)
       num = 0.0
       for j in range(1,nr+1):
         if j%ntr == 0:
            num = num + (j/ntr)*endAt[j]/allW
    else:
       num = 0.0
       print "Log-Odds ratio Score < 0.0"       
    return num

def randomModel(nq,nr):
    eta = 0.05
    g = 0.25
    pqr = 1.0
    for i in range(1,nq+nr+1):
        pqr = pqr*g
    t = (1.0-eta)**(nq+nr+1)*eta*eta
    probRand = t*pqr 
    lprobRand = -1.0*np.log(probRand)
#    print "Random model probability: ", probRand
    return probRand

if  __name__== '__main__':
    QuerySeq = sys.argv[1]
    trSeq = sys.argv[2] 
    Qnum = int(sys.argv[3])
    nq = len(QuerySeq)
    ntr = len(trSeq)
    q = np.arange(nq,dtype=np.int_)
    tempQ = map(ord,QuerySeq)
    q = np.asarray(tempQ)
    print "q: ",QuerySeq

    repeatSeq = trSeq*(Qnum)
    nr = len(repeatSeq)
    r = np.arange(nr,dtype=np.int_)
    tempR = map(ord,repeatSeq)
    r = np.asarray(tempR)
#    print "r: ",repeatSeq

    f_M = np.zeros((nq+1,nr+1))
    f_X= np.zeros((nq+1,nr+1))
    f_Y = np.zeros((nq+1,nr+1))
    #eProb=emission probability dictionary (<base_pair>,<prob value>) 
    #tProb=transition probability matrix (5 states: M, X, Y, Begin, End) 
    m = 0.856
    um = 0.048
    #um1 = 0.015 #differential emission rates for N|A, where N = C,G,T (similar for other four nucleotides)
    pen = 0.25 #num_TR is very sensitive on this value 
    #indices on both axis: 0=A,1=C,2=T,3=G,4=gap
    eProb = np.array([[ m,  um,  um,  um,  pen],\
              [ um,  m,  um,  um,  pen],\
              [ um,  um,  m,  um,  pen],\
              [ um,  um,  um,  m,  pen],\
              [ pen,  pen,  pen,  pen,  0.0]])
    #state transition probability matrix
    ex,ey = 0.075,0.025 #ex - X->X prob, ey - Y->Y prob (this says staying in INS state is more probable than DEL state)
    dx,dy = 0.11,0.02 #dx - M->X prob, dy - M->Y prob (this says going to INS state is more probable than DEL state from M)
    tau = 0.02 # A {M,X,Y} -> End state prob
    #indices on both axis: 0=M,1=X,2=Y,3=Begin,4=End 
    tProb = np.array([[ 1.0-dx-dy-tau,  dx,  dy,  0.,  tau],\
              [ 1.0-ex-tau,  ex,  0.,  0.,  tau],\
              [ 1.0-ey-tau,  0.,  ey,  0.,  tau],\
              [ 1.0-dx-dy-tau,  dx,  dy,  0.,  tau],\
              [ 0.,  0.,  0.,  0.,  0.]])
    f_M,f_X,f_Y= forwardProb(q,nq,r,nr,f_M,f_X,f_Y,eProb,tProb)
    
    num_TR = numTR(f_M,f_X,f_Y,ntr,nr,nq)
    #this method should include a comparison with the random model rather than normalizing the values
    print "number of TR in query: ", num_TR
