#!/usr/bin/env python

import sys
import os

###################################################################################################
#sys.argv[1] = input query tr count file for queries                                     
#sys.argv[2] = input bin_out file for each chr (.txt/.binned)             
#sys.argv[3] = output directory location (probably per chr)
#python binByTrfLoc.py xaa chr1.binned /hpc/users/ummata01/targets_overhang100/bin_out 
###################################################################################################

class Qtrs:
    def __init__(self):
        self.query_id, self.TrInQuery = None, 0
#query_id,0 targetStart,1 targetEnd,2 RefTandemRepeat TrInQuery,4 RefcopyNum PreScore SufScore tR_Score TrAligned QueryAligned

def parseQtrs(file):
    for line in open(file):
        templist = line.rstrip("\n").split(" ")
        hit = Qtrs()
        hit.query_id,hit.TrInQuery = templist[0],int(templist[4])
        yield hit

def dictforQtrs(QtrsDb,inm5fn):
    for hit in parseQtrs(inm5fn):
        QtrsDb[hit.query_id] = hit.TrInQuery
        print hit.TrInQuery
    print "Db for queries with tr counts formed, with number of items: ", len(QtrsDb)
        
def binQtrs():
    inm5fn = sys.argv[1]

    QtrsDb = {}
    dictforQtrs(QtrsDb,inm5fn)
    
    tempm5 = inm5fn.split("/")[-1].split("_")[1]
    outBinfn = sys.argv[3]
    hgT_outFn = outBinfn + "/" + tempm5 + ".qbinratio"
    outfile = open(hgT_outFn, 'w')
    outfile.write("Query_id target targetStart targetEnd ReftRStart ReftREnd RefPeriod RefNum QNum Ratio tRSeq\n")
    #housekeeping for outfile

    inBinfn = sys.argv[2]
    for hline in open(inBinfn, 'r'):
        hvalues = hline.rstrip("\n").split()
        hgT_query_id = hvalues[0]
        hgT_start = int(hvalues[1])
        hgT_end = int(hvalues[2])
        hgT_trfS = int(hvalues[3])
        hgT_trfE = int(hvalues[4])
        hgT_period = int(hvalues[5])
        hgT_RefcopyNum = float(hvalues[6])
        hgT_trfSeq = hvalues[7]
        for query,tr in QtrsDb.items():
            if hgT_query_id == query:
               Ratio = 0.0
               Ratio = float(tr)/hgT_RefcopyNum
               outfile.write("%s %s %i %i %i %i %d %d %i %.2f %s\n" %(hgT_query_id, tempm5, hgT_start, hgT_end, hgT_trfS, hgT_trfE, hgT_period, hgT_RefcopyNum, tr, Ratio, hgT_trfSeq))          
               outfile.flush()
    outfile.close()

binQtrs()
