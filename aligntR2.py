#!/usr/bin/env python

import os
import sys
from io.ReadMatcherIO import parseRm5
from model.AlignmentHit import AlignmentHit
from model.Sequence import revcomp as rc
import numpy as np

#read in two sequences. target. query. and align them 
#
#target sequence could be decomposed into three parts:
#a) prefix_tR: region upstream of tR region (calculated by say trf)
#b) tR: region of the tandem repeat
#c) suffix_tR: region downstream of the tR region
#
#A general function to perform prefix_tR/suffix_tR alignment with query
#Another function to perform tR alignment with query



def dictforqueries(queryAligns,m5file):
    for hit in parseRm5(m5file):
        queryAligns[hit.query_id] = hit

def sw_prefix(query,prefix,preScore,preMaxScore):
    print len(query), len(prefix)
    #Recursion for prefix_vs_query: local alignment
    for i in range(1,len(query)+1):
        for j in range(1,len(prefix)+1):
            tempRec = []
            tempRec.append(0)
            if query[i-1] == prefix[j-1]:
               tempRec.append(preScore[i-1][j-1] + 1)
            else:
               tempRec.append(preScore[i-1][j-1] - 1)
            tempRec.append(preScore[i-1][j] - 1)
            tempRec.append(preScore[i][j-1] - 1)
            tempScore = max(tempRec)
            tempmaxScore = 0
            for k in range(0,len(tempRec)):
                if tempScore == tempRec[k]:
                   tempmaxScore = k
                   #k=0 no step; k=1 step to i-1,j-1; k=2 step to i-1,j; k=3 step to i,j-1
            preScore[i][j] = tempScore
            preMaxScore[i][j] = tempmaxScore

    bestscore = 0
    best_i = 1
    best_j = 1
    for i in range(1,len(query)+1):
        j = len(prefix)
        if bestscore < preScore[i][j]:
           bestscore = preScore[i][j]
           best_i = i
           best_j = j
    print bestscore, best_i,best_j
    #bestscores and indices for the scoring matrix, preScore

    #trace back here ->
    align_pre = []
    align_query = []
    align_state = []

    tempScore = bestscore
    temp_i = best_i
    temp_j = best_j
    while (temp_i > 0 and temp_j > 0):
          if preMaxScore[temp_i][temp_j] == 1:
             align_pre.append(prefix[temp_j-1])
             align_query.append(query[temp_i-1])
             align_state.append("M")
             temp_i = temp_i - 1
             temp_j = temp_j - 1
             tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 2:
               align_pre.append("-")
               align_query.append(query[temp_i-1])
               align_state.append("D")
               temp_i = temp_i - 1
               temp_j = temp_j
               tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 3:
               align_pre.append(prefix[temp_j-1])
               align_query.append("-")
               align_state.append("I")
               temp_i = temp_i
               temp_j = temp_j - 1
               tempScore = preScore[temp_i][temp_j]

    print 'This is prefix'
    print ''.join(prefix)
    align_pre.reverse()
    print 'This is query'
    print ''.join(query)
    align_query.reverse()
    print 'This is aligned:'
    print ''.join(align_pre)
    print ''.join(align_query)
    align_state.reverse()
    print ''.join(align_state)


def sw_tR(query,tR,tRScore,tRMaxScore,len_prefix):
    print tR
    #Recursion for tR_vs_query: local alignment
    for i in range(1,len(query)+1):
        tRScore[i][0] = tRScore[i-1][len(tR)]
        tRMaxScore[i][0] = 9
        for j in range(1,len(tR)+1):
            tempRec = []
            tempRec.append(0)
            if j == 1:
               if query[i-1] == tR[j-1]:
                  tempRec.append(tRScore[i][0] + 1)
               else:
                  tempRec.append(tRScore[i][0] - 1)
               tempRec.append(0)
               tempRec.append(tRScore[i-1][j] - 1)
               tempRec.append(0)
            else:
               if query[i-1] == tR[j-1]:
                  tempRec.append(tRScore[i][0] + 1)
                  tempRec.append(tRScore[i-1][j-1] + 1)
               else:
                  tempRec.append(tRScore[i][0] - 1)
                  tempRec.append(tRScore[i-1][j-1] - 1)
               tempRec.append(tRScore[i-1][j] - 1)
               tempRec.append(tRScore[i][j-1] - 1)
            tempScore = max(tempRec)
            tempmaxScore = 0
            for k in range(0,len(tempRec)):
                if tempScore == tempRec[k]:
                   tempmaxScore = k
                   #k=0 no step; k=1 step to i,0; k=2 step to i-1,j-1; k=3 step to i-1,j; k=4 step to i,j-1; k=5 step to i-1,len(tR)
            tRScore[i][j] = tempScore
            tRMaxScore[i][j] = tempmaxScore
    print "k=0 no step; k=1 step to i,0; k=2 step to i-1,j-1; k=3 step to i-1,j; k=4 step to i,j-1"
    bestscore = 0
    best_i = 1
    best_j = 1
    for i in range(1,len(query)+1):
        for j in range(1,len(tR)+1):
            if bestscore <= tRScore[i][j]:
               bestscore = tRScore[i][j]
               best_i = i
               best_j = j
    #bestscores and indices for the scoring matrix, preScore
    print bestscore, best_i, best_j
    #trace back here ->
    align_tR = []
    align_query = []
    align_state = []

    tempScore = bestscore
    temp_i = best_i
    temp_j = best_j
    while (tempScore > 0):
          if tRMaxScore[temp_i][temp_j] == 9:
               temp_i = temp_i - 1
               temp_j = len(tR)
               tempScore = tRScore[temp_i][temp_j]
          elif tRMaxScore[temp_i][temp_j] == 2:
               align_tR.append(tR[temp_j-1])
               align_query.append(query[temp_i-1])
               align_state.append("M")
               temp_i = temp_i - 1
               temp_j = temp_j - 1
               tempScore = tRScore[temp_i][temp_j]
          elif tRMaxScore[temp_i][temp_j] == 3:
               align_tR.append("-")
               align_query.append(query[temp_i-1])
               align_state.append("D")
               temp_i = temp_i - 1
               temp_j = temp_j
               tempScore = tRScore[temp_i][temp_j]
          elif tRMaxScore[temp_i][temp_j] == 4:
               align_tR.append(tR[temp_j-1])
               align_query.append("-")
               align_state.append("I")
               temp_i = temp_i
               temp_j = temp_j - 1
               tempScore = tRScore[temp_i][temp_j]
               if temp_j == 0:
                  align_query.append(query[temp_i-1])
                  align_tR.append("-")
                  align_state.append("d")
          elif tRMaxScore[temp_i][temp_j] == 1:
               align_tR.append(tR[temp_j-1])
               align_query.append(query[temp_i-1])
               align_state.append("M")
               temp_i = temp_i
               temp_j = 0
               tempScore = tRScore[temp_i][temp_j]

    print 'This is repeat'
    print ''.join(tR)
    align_tR.reverse()
    print 'This is repeat aligned'
    print ''.join(align_tR)
    align_query.reverse()
    print 'This is query aligned'
    print ''.join(align_query)
    align_state.reverse()
    print 'This is aligned state'
    print ''.join(align_state)


def alignRegions(query,pre_suf_tR,trf_seq):
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
#    suffix = []
#    suffix = pre_suf_tR[1]

    tR = list(trf_seq)
    tR_n = len(tR)
    
    preScore = np.zeros((len(query)+1,len(prefix)+1),dtype=int)
    preMaxScore = np.zeros((len(query)+1,len(prefix)+1),dtype=int)
    sw_prefix(query,prefix,preScore,preMaxScore)

    tRScore = np.zeros((len(query)+1,len(tR)+1),dtype=int)
    tRMaxScore = np.zeros((len(query)+1,len(tR)+1),dtype=int)
    sw_tR(query,tR,tRScore,tRMaxScore,len_prefix)

   
 
#    tRScore = np.zeros((n+1,tR_n+1),dtype=int)
#    sufScore = np.zeros((n+1,suf_n+1),dtype=int)           

def calculateRegions_tR(target, rev_target, trf_start, trf_end, target_start, target_end):
    listPreSuf_tR = []
    tempTarget = []
    prefix_tR = ""
    suffix_tR = ""

    tempTarget = target
    tempRevTarget = rev_target

    lengthOfPreX = trf_start - target_start
    rev_prefix_tR = []
    for j in range(0,lengthOfPreX):
        rev_prefix_tR.append(tempRevTarget[j])
    suffix_tR = rc(''.join(rev_prefix_tR))
    
    lengthOfSufX = target_end - target_start
    startSufX = trf_end - target_start
    rev_suffix_tR = []
    for k in range(startSufX, lengthOfSufX):
        rev_suffix_tR.append(tempRevTarget[k])
    prefix_tR = rc(''.join(rev_suffix_tR))

    listPreSuf_tR.append(prefix_tR)
    listPreSuf_tR.append(suffix_tR)

    return listPreSuf_tR
 
def align_main():
    inm5fn = sys.argv[1]
    queryAligns = {}
    dictforqueries(queryAligns,inm5fn)
    print "Db for queries formed, with number of items"
    print len(queryAligns)
    for query,hit in queryAligns.items():
          pre_suf_tR = []
          pre_suf_tR = calculateRegions_tR(hit.TargetSeq,hit.RevTargetSeq,8258085,8258356,hit.target_start,hit.target_end)
          alignRegions(hit.QuerySeq, pre_suf_tR,"GAAA")


   
align_main() 
