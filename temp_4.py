import sys
import os
import numpy as np

def sw_prefix(query,prefix,preScore,preMaxScore):
    print len(query), len(prefix)
    #Recursion for prefix_vs_query: local alignment
    for i in range(1,len(query)+1):
        for j in range(1,len(prefix)+1):
            tempRec = []
            tempRec.append(0)
            if query[i-1] == prefix[j-1]:
               tempRec.append(preScore[i-1][j-1] + 1)
            else:
               tempRec.append(preScore[i-1][j-1] - 1)
            tempRec.append(preScore[i-1][j] - 1)
            tempRec.append(preScore[i][j-1] - 1)
            tempScore = max(tempRec)
            tempmaxScore = 0
            for k in range(0,len(tempRec)):
                if tempScore == tempRec[k]:
                   tempmaxScore = k
                   #k=0 no step; k=1 step to i-1,j-1; k=2 step to i-1,j; k=3 step to i,j-1
            preScore[i][j] = tempScore
            preMaxScore[i][j] = tempmaxScore

    bestscore = 0
    best_i = 1
    best_j = 1
    for i in range(len(query)+1):
        j = len(prefix)
        if bestscore < preScore[i][j]:
           bestscore = preScore[i][j]
           best_i = i
           best_j = j
    #bestscores and indices for the scoring matrix, preScore

    #trace back here ->
    align_pre = []
    align_query = []
    align_state = []

    tempScore = bestscore
    temp_i = best_i
    temp_j = best_j
    while (tempScore > 0):
          if preMaxScore[temp_i][temp_j] == 1:
             align_pre.append(prefix[temp_j-1])
             align_query.append(query[temp_i-1])
             align_state.append("M")
             temp_i = temp_i - 1
             temp_j = temp_j - 1
             tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 2:
               align_pre.append("-")
               align_query.append(query[temp_i-1])
               align_state.append("D")
               temp_i = temp_i - 1
               temp_j = temp_j
               tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 3:
               align_pre.append(prefix[temp_j-1])
               align_query.append("-")
               align_state.append("I")
               temp_i = temp_i
               temp_j = temp_j - 1
               tempScore = preScore[temp_i][temp_j]

    print 'This is prefix'
    print ''.join(prefix)
    align_pre.reverse()
    print 'This is query'
    print ''.join(query)
    align_query.reverse()
    print 'This is aligned:'
    print ''.join(align_pre)
    print ''.join(align_query)
    align_state.reverse()
    print ''.join(align_state)


def sw_tR_simple(query,tR, T, T_p,len_prefix, P,  pen=1):
    p = len_prefix
    t = len(tR)
    for i in range(1,len(query)+1):
        m = -1
        if query[i-1] == tR[0]:
            m = 1        
        vals = [P[i-1][p] + m,
                T[i-1][t] + m]
                
        T[i][1] = max(vals)
        T_p[i][1] = 3 + np.argmax(vals)

        for j in range(2,len(tR)+1):
            m = -1
            if query[i-1] == tR[j-1]:
                m = 1
            vals = [T[i-1][j] - pen,
                    T[i][j-1] - pen,
                    T[i-1][j-1] + m,
                    P[i-1][p] + m]
            T[i][j] = max(vals)
            T_p[i][j] = np.argmax(vals)
    print T
    print T_p
    sys.stdout.flush()
    return T, T_p

def tb_tR_simple (query, tR, T, T_p):
    t = len(tR)
    temp = 0
    maxTup = (0,0)
    for i in range(len(query)+1):
        for j in range(len(tR)+1):
            if temp < T[i][j]:
               temp = T[i][j]
               maxTup = (i,j)
    pre = False
    i,j = maxTup
    qList = []
    tList = []
    print "i,j:", i,j
    while (not (pre)):
        if (T_p[i][j] == 0):
            qList.append(query[i-1])
            tList.append("-")
            i += - 1
        elif (T_p[i][j] == 1):
            qList.append("-")
            tList.append(tR[j-1])
            j += - 1
        elif (T_p[i][j] == 2):
            qList.append(query[i-1])
            tList.append(tR[j-1])            
            i += - 1
            j += - 1
        elif (T_p[i][j] == 3):
            qList.append(query[i-1])
            tList.append(tR[j-1])            
            i += - 1
            pre = True
        else:
            qList.append(query[i-1])
            tList.append(tR[j-1])            
            i += -1
            j = t
    print "".join(qList)[::-1]
    print "".join(tList)[::-1]
    return 0

def sw_suffix(query,suffix,sufScore,sufMaxScore,len_tR,tRScore, pen=1):
    m = 1 
    for i in range(1, len(query)+1):
        for j in range(1, len(suffix)+1):
            tempRec = []
            if query[i-1] == suffix[j-1]:
               tempRec.append(sufScore[i-1][j-1] + m)
               tempRec.append(tRScore[i-1][len_tR] + m)
            else:
               tempRec.append(sufScore[i-1][j-1] - m)
               tempRec.append(tRScore[i-1][len_tR] - m)
            tempRec.append(sufScore[i-1][j] - pen)
            tempRec.append(sufScore[i][j-1] - pen)
            sufScore[i][j] = max(tempRec)
            sufMaxScore[i][j] = np.argmax(tempRec)
    print sufScore
    print sufMaxScore   

def tb_suffix(query,suffix,S,S_p):
    temp = 0
    i = 0
    j = 0
    for k in range(len(query)+1):
        s = len(suffix)
        if temp < S[k][s]:
           temp = S[k][s]
           i = k
           j = s

    pre = False
    qList = []
    sList = []
    print "best_i, best_j:", i, s, " score: ", temp

    while (not (pre)):
        if (S_p[i][j] == 2):
            qList.append(query[i-1])
            sList.append("-")
            i += - 1
        elif (S_p[i][j] == 3):
            qList.append("-")
            sList.append(suffix[j-1])
            j += - 1
        elif (S_p[i][j] == 0):
            qList.append(query[i-1])
            sList.append(suffix[j-1])
            i += - 1
            j += - 1
        elif (S_p[i][j] == 1):
            qList.append(query[i-1])
            sList.append(suffix[j-1])
            i += - 1
            pre = True
    print "".join(qList)[::-1]
    print "".join(sList)[::-1]
    return 0    


def alignRegions(query,pre_suf_tR,trf_seq):
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
    suffix = []
    suffix = list(pre_suf_tR[1])
    len_suffix = len(suffix)

    tR = list(trf_seq)
    len_tR = len(tR)

    preScore = np.zeros((len(query)+1,len(prefix)+1),dtype=int)
    preMaxScore = np.zeros((len(query)+1,len(prefix)+1),dtype=int)
    sw_prefix(query,prefix,preScore,preMaxScore)

    tRScore = np.zeros((len(query)+1,len(tR)+1),dtype=int)
    tRMaxScore = np.zeros((len(query)+1,len(tR)+1),dtype=int)
    sw_tR_simple(query,tR,tRScore,tRMaxScore,len_prefix,preScore)
    tb_tR_simple(query,tR,tRScore,tRMaxScore)


    sufScore = np.zeros((len(query)+1,len(suffix)+1),dtype=int)
    sufMaxScore = np.zeros((len(query)+1,len(suffix)+1),dtype=int)
    sw_suffix(query,suffix,sufScore,sufMaxScore,len_tR,tRScore)
    tb_suffix(query,suffix,sufScore,sufMaxScore)


q1 = 'TCTCGAAGAATTAT'
q = list(q1)
tList = ['TC','TTAT']
str = 'GAA'
alignRegions(q,tList,str)

