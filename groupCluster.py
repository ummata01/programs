#!/usr/bin/env python

#####################################################################################################################################################################
#sys.argv[1] = input xxx.qbinrS file which is sorted (sort -k5m ... > xxx.qbinrS):This is a sort on ReftRStart position                               
#sys.argv[2] = output directory location 
#python filterQbykmean.py ~/targets_overhang100/qbin/kmeans/unique/unq_*.txt ~/targets_overhang100/qbin/chr1.qbin ~/targets_overhang100/qbin/kmeans/unique/qbykmeans
#####################################################################################################################################################################
import sys
import os
from itertools import *
from sklearn.cluster import KMeans as kM
from scipy.stats import anderson as ad
import utilsKmeansCluster as uKc 
import numpy as np

class qbin:
    def __init__(self):
        self.Query_id, self.targetStart, self.targetEnd = None,0,0
        self.ReftRStart, self.ReftREnd, self.RefPeriod, self.RefNum, self.tRSeq, self.anchorPre, self.anchorSuf, self.naiveTR = 0,0,0,0.0,None,0,0,0.0

def parseQbin(file):
    for line in open(file):
        values = line.rstrip("\n").split(" ")
        hit = qbin()
        hit.Query_id, hit.targetStart, hit.targetEnd = values[0], int(values[1]),int(values[2])
        hit.ReftRStart, hit.ReftREnd, hit.RefPeriod, hit.RefNum, hit.tRSeq, hit.anchorPre, hit.anchorSuf, hit.naiveTR = int(values[3]), int(values[4]), int(values[5]), float(values[6]), values[7], int(values[8]),int(values[9]),float(values[10])
        yield hit

def andersonDarling(X):
    A2stats = ad(X) #anderson-darling EDF test
    numTR = np.size(X)
    #modified value of A2stats
    #modA2 = (1.0 + 4.0/numTR - 25.0/(numTR*numTR))
    modA2 = (1.0 + 0.75/numTR + 2.25/(numTR*numTR))
    A2statsMod = A2stats[0]*modA2
    acceptHypothesis0 = False
    print acceptHypothesis0, A2statsMod
    #if A2statsMod < 1.869: #significance level alpha = 0.0001, Ref: G-means
    if A2statsMod < 1.159: #significance level alpha = 0.005, Ref: D'Agostino (1986) in Table 4.7, p. 123
       acceptHypothesis0 = True
       print "accept H0"
    else:
       acceptHypothesis0 = False
       print "reject H0"
    return A2statsMod, acceptHypothesis0

def setAlleleFlags(acceptHypothesis0, bic):
    oneAllele = False
    acceptData = False
    if bic[0] > bic[1]:
       if acceptHypothesis0 == True:
          oneAllele = True
          acceptData = True
       else:
          acceptData = False
    else:
       if acceptHypothesis0 == False:
          oneAllele = False
          acceptData = True
       else:
          acceptData = False
    return oneAllele, acceptData

def cluster_TRs(TR_list):
    numTR = len(TR_list)
#    TR_list.sort()
    TR_Data = np.zeros((numTR,1),dtype=np.float_)
    for i in range(0,numTR):
        TR_Data[i,0] = TR_list[i]
    bic = np.zeros((2),dtype=np.float_)
    centre, label, statsD, numReads = [], [], [], []
    for numCluster in [1,2]:
        #K-means calculations
        kmeans = kM(numCluster)
        kmeans.fit(TR_Data)
        centroids = kmeans.cluster_centers_
        centre.append(centroids)
        if numCluster == 1:
           TR_CL_1 = [TR_Data[:,0]]
           bic[numCluster-1] = uKc.BIC_Gauss(TR_CL_1,numTR,centroids,numCluster)
           stats_nCL_1 = (np.mean(TR_Data[:,0]), np.std(TR_Data[:,0]))
           statsD.append(stats_nCL_1)
        if numCluster == 2:
           label = kmeans.labels_
           tempCL1 = []
           tempCL2 = []
           for j in range(0,len(TR_Data)):
               if label[j] == 0:
                  tempCL1.append(TR_Data[j])
               else:
                  tempCL2.append(TR_Data[j])
           TR_CL1 = np.asarray(tempCL1)
           TR_CL2 = np.asarray(tempCL2)
           numReads = [np.size(TR_CL1),np.size(TR_CL2)]
           TR_CL_1_2 = [TR_CL1,TR_CL2]
           bic[numCluster-1] = uKc.BIC_Gauss(TR_CL_1_2,numTR,centroids,numCluster)
           stats_nCL_2_1 = (np.mean(TR_CL1), np.std(TR_CL1)) 
           stats_nCL_2_2 = (np.mean(TR_CL2), np.std(TR_CL2)) 
           statsD.append(stats_nCL_2_1)
           statsD.append(stats_nCL_2_2)

    A2statsMod, acceptHypothesis0 = andersonDarling(TR_Data[:,0])
    print acceptHypothesis0
    oneAllele, acceptData = setAlleleFlags(acceptHypothesis0, bic)
    print acceptData,oneAllele,A2statsMod,centre,statsD,bic,label,numReads
    return acceptData,oneAllele,A2statsMod,centre,statsD,bic,label,numReads

def writeReadsToFile(hits,outfile,label,flag):
    if flag == True:
       cluster = 0
       for hit in hits:
           outfile.write("%s %d %d %d %d %d %.2f %s %.2f %.2f %d %d %d\n" %(hit.Query_id,hit.targetStart,hit.targetEnd,hit.ReftRStart,hit.ReftREnd,hit.RefPeriod,hit.RefNum,hit.tRSeq,hit.naiveTR,hit.naiveTR/hit.RefNum,cluster,hit.anchorPre,hit.anchorSuf))
    else:
        k = 0
        for hit in hits:
            outfile.write("%s %d %d %d %d %d %.2f %s %.2f %.2f %d %d %d\n" %(hit.Query_id,hit.targetStart,hit.targetEnd,hit.ReftRStart,hit.ReftREnd,hit.RefPeriod,hit.RefNum,hit.tRSeq,hit.naiveTR,hit.naiveTR/hit.RefNum,label[k],hit.anchorPre,hit.anchorSuf))
            k += 1
    outfile.flush()

def binQtrs():
    inm5fn = sys.argv[1]
    tempm5 = inm5fn.split("/")[-1].split(".")[0]
    outBinfn = sys.argv[2]
    hgT_outFn = outBinfn + "/" + tempm5 + "_cluster.txt"
    outfile = open(hgT_outFn, 'w')
    outfile.write("Num_Alleles ADstats BICratio Size1 Mean1 Stdev1 Size2 Mean2 Stdev2\n")
    outfile.write("#-1=NoCall  1.8692  CL1/CL2   CL1   CL1   CL1    CL2   CL2   CL2 #\n")
    outfile.write("QueryId targetStart targetEnd refTrStart refTrEnd refTrPeriod refTrNum refTrSeq readTrNum TR_ratio cluster anchorPre anchorSuf\n")
    outfile.write("#                                                                                         qTr/rTr  0 or 1                    #\n")
    #housekeeping for outfile

    acceptData = False
    oneAllele = False
    centre = []
    statsD = []
    bic = []
    hits = parseQbin(inm5fn)
    for k,g in groupby(hits, lambda x: x.ReftRStart):
        hits = list(g)
        naiveTRs = []
        for hit in hits:
            naiveTRs.append((hit.naiveTR)/(hit.RefNum))
        #call the clustering module#
        if len(naiveTRs) >= 2:
           acceptData,oneAllele,A2stats,centre,statsD,bic,labelD,numReads = cluster_TRs(naiveTRs)
           print "bic: ", bic
           if acceptData == True:
              if oneAllele == True:
                 numAllele = 1
                 m2, s2 = 0, 0
                 sizeCL1, sizeCL2 = len(naiveTRs), 0
                 outfile.write("%d %.2f %.2f %d %.2f %.2f %d %d %d\n" %(numAllele,A2stats,bic[0]/bic[1],sizeCL1,statsD[0][0],statsD[0][1],sizeCL2,m2,s2))
                 outfile.flush()
                 writeReadsToFile(hits,outfile,labelD,oneAllele)
              else:
                 numAllele = 2
                 outfile.write("%d %.2f %.2f %d %.2f %.2f %d %.2f %.2f\n" %(numAllele,A2stats,bic[0]/bic[1],numReads[0],statsD[1][0],statsD[1][1],numReads[1],statsD[2][0],statsD[2][1]))
                 outfile.flush()
                 writeReadsToFile(hits,outfile,labelD,False)
           else:
              numAllele = -1
              m2, s2 = 0, 0 
              sizeCL1, sizeCL2 = len(naiveTRs), 0
              outfile.write("%d %.2f %.2f %d %.2f %.2f %d %.2f %.2f\n" %(numAllele,A2stats,bic[0]/bic[1],sizeCL1,statsD[0][0],statsD[0][1],sizeCL2,m2,s2))
              outfile.flush()
              writeReadsToFile(hits,outfile,labelD,True)
        else:
            numAllele = -1
            m1, s1 = naiveTRs[0], 0
            m2, s2 = 0, 0
            A2stats, bic01 = 0, 0
            labelD = [0]
            sizeCL1, sizeCL2 = len(naiveTRs), 0
            outfile.write("%d %.2f %.2f %d %.2f %.2f %d %.2f %.2f\n" %(numAllele,A2stats,bic01,sizeCL1,m1,s1,sizeCL2,m2,s2))
            outfile.flush()
            writeReadsToFile(hits,outfile,labelD,True)
    outfile.close()

binQtrs()
