#!/usr/bin/env python

import sys
import os

###################################################################################################
#sys.argv[1] = input m5 file (.m5)                                     
#sys.argv[2] = input hgTablewithrepeats information (.txt)             
#sys.argv[3] = the directory path where the out file needs to be stored (format .binned)
#python querydict2.py chr10_ovrhg1k.m5 hgchr10.txt /hpc/users/ummata01/targets_overhang100/bin_out 
###################################################################################################
class binLine:
    def __init__( self ):
        self.query_id, self.target_start, self.target_end, \
            self.refTrf_st, self.refTrf_en = None,0,0,0,0
        self.refTrf_repeat, self.refTrf_copyNum, self.refTrf_seq = 0,0,None
        self.anchorPre, self.anchorSuf = 0,0
#query, obj.target_start, obj.target_end, hgT_start, hgT_end, hgT_period, hgT_copyNum, hgT_trfSeq,(hgT_start - obj.target_start),(obj.target_end - hgT_end)

def parsebinf(file):
    for line in open(file):
        values = line.rstrip("\n").split(" ")
        hit = binLine()
        hit.query_id, hit.target_start, hit.target_end, hit.refTrf_st, hit.refTrf_en = values[0], int(values[1]), int(values[2]), int(values[3]),int(values[4])
        hit.refTrf_repeat, hit.refTrf_copyNum, hit.refTrf_seq = float(values[5]), float(values[6]), values[7]
        hit.anchorPre, hit.anchorSuf = int(values[8]), int(values[9])
        yield hit

class AlignmentHit:
    def __init__( self ):
        self.query_length, self.query_id, self.query_start, \
            self.query_end, self.query_strand = 0, None, 0, 0, None
        self.target_length, self.target_id, self.target_start, \
            self.target_end,self.target_strand = 0, None, 0, 0, None
        self.full = None
        self.QueryStrOrg = ""
        self.TargetStrOrg = ""

def parseRm5(file):
    """Parses readmatcher -printFormat 5 output into AlignmentHit objects"""
    for line in open(file):
        #print line
        
        values = line.rstrip("\n").split(" ")
        hit = AlignmentHit()
        hit.full = line
        hit.query_id, hit.query_length, hit.query_start, hit.query_end, hit.query_strand = values[0], int(values[1]), int(values[2]), int(values[3]), values[4]

        hit.target_id, hit.target_length, hit.target_start, hit.target_end, hit.target_strand = values[6], int(values[7]), int(values[8]), int(values[9]), values[10]
        hit.score = -1*int(values[11])
        #target_id = values[6] because there is white space in the m5 file before target_id

        hit.QueryStrOrg = values[17] 
        hit.TargetStrOrg = values[19] 

        hit.query_id = "/".join(hit.query_id.split("/")[0:3])
        yield hit

def dictforqueries(fastqReads,m5file):
    for hit in parseRm5(m5file): 
  	fastqReads[hit.query_id] = hit

def getTRestimateFromRef(RefStr,QueryStr,aPre,aSuf,TRinRef,period):
    RefIndex = 0

    for i in range(0,len(RefStr)):
        refTrS = i
        if RefIndex == aPre:
           break
        else:
           if RefStr[i] != '-':   
              RefIndex += 1

    numBasesInQueryPre = 0

    for j in range(0,refTrS):
        if QueryStr[j] != '-':  
           numBasesInQueryPre += 1
    RefIndexB = 0

    for i in range(len(RefStr)-1,-1,-1):
        refTrE = i
        if RefIndexB == aSuf:
           break
        else:
           if RefStr[i] != '-': 
              RefIndexB += 1

    numBasesInQuerySuf = 0

    for j in range(refTrE,len(QueryStr)):
        if QueryStr[j] != '-': 
           numBasesInQuerySuf += 1

    trInQuery = QueryStr[refTrS:refTrE+1] #extracting relevant TR region from Query #have to add one to get the element at index
    lenQinTR = 0
    for i in range(0,len(trInQuery)):
        if trInQuery[i] != '-':
           lenQinTR += 1
    corrRatio = ((aPre+aSuf)/float(numBasesInQueryPre+numBasesInQuerySuf))
    numTRcorrected = corrRatio*lenQinTR/period
    return numTRcorrected

def binm5file():
    inm5fn = sys.argv[1] #input .m5 file
    fastqReads = {}
    dictforqueries(fastqReads,inm5fn)

    tempm5 = inm5fn.split("/")[-1].split(".")[0]    
    outBinfn = sys.argv[3] #directory to store the output 
    hgT_outFn = outBinfn + "/" + tempm5 + "_estTR.txt"
    outfile = open(hgT_outFn, 'w')

    inhgTfn = sys.argv[2] #input .binned_anchor file 
    for hline in open(inhgTfn, 'r'):
        values = hline.rstrip("\n").split()
        query_id, target_start, target_end, refTrf_st, refTrf_en = values[0], int(values[1]), int(values[2]), int(values[3]),int(values[4])
        refTrf_repeat, refTrf_copyNum, refTrf_seq = float(values[5]), float(values[6]), values[7]
        anchorPre, anchorSuf = int(values[8]), int(values[9])
        if query_id in fastqReads:
           hit = AlignmentHit()
           hit = fastqReads[query_id]
           if anchorPre > 50 or anchorSuf > 50:
              numTR = getTRestimateFromRef(hit.TargetStrOrg,hit.QueryStrOrg,anchorPre,anchorSuf,refTrf_copyNum,refTrf_repeat)
              outfile.write("%s %.2f\n" %(hline.rstrip("\n"), numTR))
              outfile.flush()
    outfile.close()    

binm5file()
