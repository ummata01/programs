#!/usr/bin/env python

#####################################################################################################################################################################
#sys.argv[1] = input xxx.qbinrS file which is sorted (sort -k5m ... > xxx.qbinrS):This is a sort on ReftRStart position                               
#sys.argv[2] = output directory location 
#python filterQbykmean.py ~/targets_overhang100/qbin/kmeans/unique/unq_*.txt ~/targets_overhang100/qbin/chr1.qbin ~/targets_overhang100/qbin/kmeans/unique/qbykmeans
#####################################################################################################################################################################
import sys
import os
from itertools import *
from sklearn import mixture as M
from scipy.stats import anderson as ad
import numpy as np
#import cProfile

class qbin:
    def __init__(self):
        self.Query_id, self.targetStart, self.targetEnd = None,0,0
        self.ReftRStart, self.ReftREnd, self.RefPeriod, self.RefNum, self.tRSeq, self.anchorPre, self.anchorSuf, self.naiveTR = 0,0,0,0.0,None,0,0,0.0

def parseQbin(file):
    for line in open(file):
        values = line.rstrip("\n").split(" ")
        hit = qbin()
        hit.Query_id, hit.targetStart, hit.targetEnd = values[0], int(values[1]),int(values[2])
        hit.ReftRStart, hit.ReftREnd, hit.RefPeriod, hit.RefNum, hit.tRSeq, hit.anchorPre, hit.anchorSuf, hit.naiveTR = int(values[3]), int(values[4]), int(values[5]), float(values[6]), values[7], int(values[8]),int(values[9]),float(values[10])
        yield hit

def andersonDarling(X):
    A2stats = ad(X) #anderson-darling EDF test
    numTR = np.size(X)
    #modified value of A2stats
    #modA2 = (1.0 + 4.0/numTR - 25.0/(numTR*numTR))
    modA2 = (1.0 + 0.75/numTR + 2.25/(numTR*numTR))
    A2statsMod = A2stats[0]*modA2
    acceptHypothesis0 = False
    #if A2statsMod < 1.869: #significance level alpha = 0.0001, Ref: G-means
    if A2statsMod < 1.159: #significance level alpha = 0.005, Ref: D'Agostino (1986) in Table 4.7, p. 123
       acceptHypothesis0 = True
    else:
       acceptHypothesis0 = False
    return A2statsMod, acceptHypothesis0

def setAlleleFlags(acceptHypothesis0, bic):
    oneAllele = False
    acceptData = False
    if bic[0] < bic[1]:
       if acceptHypothesis0 == True:
          oneAllele = True
          acceptData = True
       else:
          acceptData = False
    else:
       if acceptHypothesis0 == False:
          oneAllele = False
          acceptData = True
       else:
          acceptData = False
    return oneAllele, acceptData

def cluster_TRs(TR_list):
    numTR = len(TR_list)
    TR_Data = np.zeros((numTR,1),dtype=np.float_)
    for i in range(0,numTR):
        TR_Data[i,0] = TR_list[i]
    bic = np.zeros((2),dtype=np.float_)

    #Calling GMM routines here...
    g1 = M.GMM(n_components=1)
    g1.fit(TR_Data)
    bic[0] = g1.bic(TR_Data)
    g1_stats = (g1.means_[0][0],g1.covars_[0][0])
    
    g2 = M.GMM(n_components=2)
    g2.fit(TR_Data)
    bic[1] = g2.bic(TR_Data)
    label = g2.predict(TR_Data)
    g2_stats = ((g2.means_[0][0],g2.covars_[0][0]),(g2.means_[1][0],g2.covars_[1][0]))
    weight = [g2.weights_[0],g2.weights_[1]]
    #Get Anderson-Darling statistics for the entire Data 
    A2statsMod, acceptHypothesis0 = andersonDarling(TR_Data[:,0])
    #Based on the AD stats and bic set the Allele flags
    oneAllele, acceptData = setAlleleFlags(acceptHypothesis0, bic)

    return acceptData,oneAllele,A2statsMod,bic,g1_stats,g2_stats,label,weight

def writeReadsToFile(hits,outfile,label,flag):
    if flag == True:
       cluster = 0
       for hit in hits:
           outfile.write("%s %d %d %d %d %d %.2f %s %.2f %.2f %d %d %d\n" %(hit.Query_id,hit.targetStart,hit.targetEnd,hit.ReftRStart,hit.ReftREnd,hit.RefPeriod,hit.RefNum,hit.tRSeq,hit.naiveTR,hit.naiveTR/hit.RefNum,cluster,hit.anchorPre,hit.anchorSuf))
    else:
        k = 0
        for hit in hits:
            outfile.write("%s %d %d %d %d %d %.2f %s %.2f %.2f %d %d %d\n" %(hit.Query_id,hit.targetStart,hit.targetEnd,hit.ReftRStart,hit.ReftREnd,hit.RefPeriod,hit.RefNum,hit.tRSeq,hit.naiveTR,hit.naiveTR/hit.RefNum,label[k],hit.anchorPre,hit.anchorSuf))
            k += 1

def binQtrs():
    inm5fn = sys.argv[1]
    tempm5 = inm5fn.split("/")[-1].split(".")[0]
    outBinfn = sys.argv[2]
    hgT_outFn = outBinfn + "/" + tempm5 + "_cluster.txt"
    outfile = open(hgT_outFn, 'w')
    outfile.write("Num_Alleles ADstats BIC TotalReads Weight1 Mean1 Var1 Weight2 Mean2 Var2\n")
    outfile.write("#-1=NoCall   1.159  low             CL1     CL1  CL1   CL2     CL2  CL2 #\n")
    outfile.write("QueryId targetStart targetEnd refTrStart refTrEnd refTrPeriod refTrNum refTrSeq readTrNum TR_ratio cluster anchorPre anchorSuf\n")
    outfile.write("#                                                                                         qTr/rTr  0 or 1                    #\n")
    #housekeeping for outfile

    acceptData = False
    oneAllele = False
    hits = parseQbin(inm5fn)
    for k,g in groupby(hits, lambda x: x.ReftRStart):
        hits = list(g)
        naiveTRs = []
        for hit in hits:
            naiveTRs.append((hit.naiveTR)/(hit.RefNum))
        numTRs = len(naiveTRs)
        #call the clustering module#
        if len(naiveTRs) >= 2:
           acceptData,oneAllele,A2stats,bic,g1_stats,g2_stats,labelD,weight = cluster_TRs(naiveTRs)
           if acceptData == True:
              if oneAllele == True:
                 numAllele = 1
                 m2, s2 = 0, 0
                 sizeCL1, sizeCL2 = 1.0, 0.0
                 outfile.write("%d %.2f %.2f %d %.2f %.2f %.3f %.2f %.2f %.2f\n" %(numAllele,A2stats,bic[0],numTRs,sizeCL1,g1_stats[0],g1_stats[1],sizeCL2,m2,s2))
                 writeReadsToFile(hits,outfile,labelD,oneAllele)
              else:
                 numAllele = 2
                 outfile.write("%d %.2f %.2f %d %.2f %.2f %.3f %.2f %.2f %.3f\n" %(numAllele,A2stats,bic[1],numTRs,weight[0],g2_stats[0][0],g2_stats[0][1],weight[1],g2_stats[1][0],g2_stats[1][1]))
                 writeReadsToFile(hits,outfile,labelD,oneAllele)
           else:
              numAllele = -1
              m2, s2 = 0, 0 
              sizeCL1, sizeCL2 = 1.0, 0.0
              bic_ = bic[0] if bic[0] < bic[1] else bic[1]
              outfile.write("%d %.2f %.2f %d %.2f %.2f %.2f %.2f %.2f %.2f\n" %(numAllele,A2stats,bic_,numTRs,sizeCL1,g1_stats[0],g1_stats[1],sizeCL2,m2,s2))
              writeReadsToFile(hits,outfile,labelD,True)
        else:
            numAllele = -1
            m1, s1 = naiveTRs[0], 0
            m2, s2 = 0, 0
            A2stats, bic01 = 'na','na'
            labelD = [0]
            sizeCL1, sizeCL2 = 1.0, 0.0
            outfile.write("%d %s %s %d %.2f %.2f %.2f %.2f %.2f %.2f\n" %(numAllele,A2stats,bic01,numTRs,sizeCL1,m1,s1,sizeCL2,m2,s2))
            writeReadsToFile(hits,outfile,labelD,True)
        outfile.flush() 
    outfile.close()

#cProfile.run('binQtrs()')
binQtrs()
