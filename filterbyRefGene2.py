#!/usr/bin/env python

import sys
import os
from itertools import *
#####################################################################################################################################################################
#sys.argv[1] = modRefGenehg19.txt (the table containing definition of regions) 
#sys.argv[2] = file containing the location of events in the format (chr1 XXXX XXXXX), where XXXX and XXXXX are locations on chromosomes (int)    
#sys.argv[3] = output file location and name (program takes the name and appends either "_cd" or "_exon" to this name and creates them accordingly

#python filterbyRefGene2.py modRefGenehg19.txt event.txt out.txt 
#####################################################################################################################################################################

class qbin:
    def __init__(self):
        self.target, self.tsStart, self.tsEnd, self.cdStart, self.cdEnd = None,0,0,0,0
        self.exS, self.exE, self.name = [],[],None

def parseQbin(file):
    for line in open(file):
        values = line.rstrip("\n").split()
        hit = qbin()
        hit.target, hit.tsStart, hit.tsEnd, hit.cdStart,hit.cdEnd = values[0], int(values[1]), int(values[2]), int(values[3]),int(values[4])
        hit.name = values[7]
        texS, texE = values[5],values[6]
        hit.target=hit.target.split("_")[0]
        tempS=texS.split(",")
        for i in range(0,len(tempS)-1):
          hit.exS.append(tempS[i])

        tempE=texE.split(",")
        for i in range(0,len(tempE)-1):
          hit.exE.append(tempE[i])
        
        yield hit

class tr:
    def __init__(self):
        self.target, self.trStart, self.trEnd = None,0,0

def parsetr(file):
    for line in open(file):
        values = line.rstrip("\n").split()
        hit = tr()
        hit.target, hit.trStart, hit.trEnd = values[0], int(values[1]), int(values[2]) 
        yield hit

def binQtrs():
    inBinfn = sys.argv[1]
    intrf = sys.argv[2]
    outf = sys.argv[3]
    outfn = outf + "_cd"
    outfile = open(outfn,'w')
    outfn2 = outf + "_exon"
    outfile2 = open(outfn2,'w')
    for hits in parseQbin(inBinfn):
        for trfs in parsetr(intrf):
            if hits.target == trfs.target:
                if ((trfs.trStart >= hits.tsStart and trfs.trStart <= hits.tsEnd)): 
                    if ((trfs.trStart >= hits.cdStart and trfs.trStart <= hits.cdEnd)):
                        ann = "CD"
                        outfile.write("%s %i %i %s %i %i %i %i %s\n"%(trfs.target, trfs.trStart, trfs.trEnd, ann, hits.tsStart, hits.tsEnd, hits.cdStart, hits.cdEnd, hits.name))
                        outfile.flush()
                    ex_len = len(hits.exS)
                    for k in range(0,ex_len):
                        exSk = int(hits.exS[k])
                        exEk = int(hits.exE[k])
                        if((trfs.trStart >= exSk and trfs.trStart <= exEk)):
                                ann = "Exon"
                                outfile2.write("%s %i %i %s %i %i %s\n"%(trfs.target, trfs.trStart, trfs.trEnd, ann, exSk, exEk,hits.name))
                                outfile2.flush()
    outfile.close()
    outfile2.close()


binQtrs()
