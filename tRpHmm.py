#!/usr/bin/env python
"""Calculating expected value of number of Tandem repeats in a given DNA sequence """
import os
import sys
import numpy as np
import pairHmmFWD2 as pHf2
import math


def getNumTRfromPairHmm(QuerySeq,trSeq,tProb,eProb_M,eProb_X):
    """
       The objective of this function is to calculate expected value of number of tandem repeats in QuerySeq via a pair Hidden Markov Model
       
       Parameters
       ---------
       This functions takes in five inputs:
       QuerySeq = string 
                  A sequence with Tandem repeats (ACACACAC....AC). This string is converted to numpy array of integers via mapping and stored as 'q'

       trSeq = string
               A tandem repeat element (AC). This is used as a seed to construct the second sequence, repeatSeq, for pairHMM. repeatSeq -> converted to numpy array of integers and stored as 'r'

       tProb = numpy array (5 by 5). 0-Match M, 1-Insertion X, 2-Deletion Y, 3-Begin
               transition probability for the pairHmm (Three states: Match, Insertion, Deletion)
 
       eProb_M = numpy array (4 by 4). 0-A, 1-C, 2-T, 3-G for both axis
                 Emission probability when in Match state (M). 
                 For pairHmm eProb_M[i,j] = prob of observing 'i' in QuerySeq and 'j' in trSeq when in Match state

       eProb_X = numpy array (4 by 4). 0-A, 1-C, 2-T, 3-G for both axis 
                 Emission probability when in insertion state (X) - (prob of observing a base at t) conditioned on (prob of observing a base at t-1)
                 For pairHmm eProb_X[i,j] = prob of observing 'i' in QuerySeq (at t) and '-' in trSeq when in insertion state, given that we observed 'j' in QuerySeq at t-1.

       Returns
       ---------
       num_TR = float
                Number of Tandem Repeat elements of trSeq, in a DNA sequence, QuerySeq

       llr = float
             Log-likelihood ratio (Higher the better, as it represents higher signal from a random sequence)

       ProbMax = float 
                 max probability (in Log.) in forward prob matrix of a pairHMM (less negative implies higher probability) 

       Notes
       -----
       1) The function sets up the problem and calls pairHmmFWD2 module which implements pairHmm where one sequence is QuerySeq and other trSeq*factor
          Here, 
              Factor = mltFactor * estTRinQ
          where, 
              mltFactor = factor to ensure the upper bounds in number of Tandem repeats in QuerySeq are detected
              estTRinQ = coarse estimation of number of Tandem repeats in QuerySeq

       2) The transition (tProb, which includes probabilites of being in initial states) and emission probabilities (M, X) are estimated from data and are required input for the calculation of the number of tandem repeats

       3) The emission probability in the delete state (Y) is by definition 1.0, as one steps forward in the repeatSeq (or r) space, but only gets '-' from the QuerySeq space in pairHMM 

       4) Run-time is O(N^2*|QuerySeq|*|repeatSeq|), where N = number of states in the Hidden Markov Model. In case here, N = 3.
    """
    #print "QuerySeq ", QuerySeq
    #print "tProb:\n", tProb
    pen = 1.0
    eProb_Y = np.array([pen, pen, pen, pen])

    nq = len(QuerySeq) #QuerySeq represents the putative TR region in the Query from 3DP step
    ntr = len(trSeq) #trSeq is the repeat Element

    #coarse estimation of TRs in QuerySeq
    estTRinQ = int(nq/ntr)
    #A factor reflecting unrestricted TR estimation
    mltFactor = 1.5

    #Converting QuerySeq to integers
    q = np.arange(nq,dtype=np.int)
    tempQ = map(ord,QuerySeq)
    q = np.asarray(tempQ)

    #Defining the repeatSeq for pairHmm
    repeatSeq = trSeq*int(mltFactor*estTRinQ)
    #Constructing the repeat array and converting to integers
    nr = len(repeatSeq)
    r = np.arange(nr,dtype=np.int)
    tempR = map(ord,repeatSeq)
    r = np.asarray(tempR)

    print "nr: ",nr," nq: ",nq

    #Defining forward Prob for three states: M, X, Y
    Lf_M = np.zeros((nq+1,nr+1))-1.0*float("inf")
    Lf_X= np.zeros((nq+1,nr+1))-1.0*float("inf")
    Lf_Y = np.zeros((nq+1,nr+1))-1.0*float("inf")
    #Calculate forward probability in the three states
    Lf_M,Lf_X,Lf_Y = pHf2.forwardProbLog(q,nq,r,nr,Lf_M,Lf_X,Lf_Y,eProb_M,eProb_X,eProb_Y,tProb)

    #Calculating expected value of number of TRs in QuerySeq
    num_TR,llr,index,ProbMax = pHf2.numTRlog(Lf_M,Lf_X,Lf_Y,ntr,nr,nq)

    #print "num_TR: ",num_TR,"llr: ",llr,"index: ",index," having Max. Prob (log) of: ",ProbMax
    return num_TR, llr, ProbMax
