#!/usr/bin/env python

import os
import sys
from io.ReadMatcherIO import parseRm5
from model.AlignmentHit import AlignmentHit
from io.binFileIO import parsebinf
from model.binLine import binLine
from model.Sequence import revcomp as rc
import numpy as np
#import cProfile
#read in two sequences. target. query. and align them 
#
#target sequence could be decomposed into three parts:
#a) prefix_tR: region upstream of tR region (calculated by say trf)
#b) tR: region of the tandem repeat
#c) suffix_tR: region downstream of the tR region
#
#A general function to perform prefix_tR/suffix_tR alignment with query
#Another function to perform tR alignment with query


def dictforbindata(binData,binfile):
    for line in parsebinf(binfile):
        binData[line.query_id] = line

def dictforqueries(queryAligns,m5file):
    for hit in parseRm5(m5file):
        queryAligns[hit.query_id] = hit

#####

def sw_prefix(query,prefix,preScore,preMaxScore,pen = -2):
#    print len(query), len(prefix)
    #Recursion for prefix_vs_query: local alignment
    for i in range(1,len(query)+1):
        for j in range(1,len(prefix)+1):
            m = -1
            if query[i-1] == prefix[j-1]:
               m = 1 
            vals = [preScore[i-1][j-1] + m, preScore[i-1][j] + pen, preScore[i][j-1] + pen]
            tempScore = max(vals)
            tempmaxScore = 0
            for k in range(0,3):
                if tempScore == vals[k]:
                   tempmaxScore = k
            preScore[i][j] = tempScore
            preMaxScore[i][j] = tempmaxScore

    bestscore = 0
    best_i = 1
    best_j = 1
    for i in range(len(query)+1):
        j = len(prefix)
        if bestscore < preScore[i][j]:
           bestscore = preScore[i][j]
           best_i = i
           best_j = j
    #bestscores and indices for the scoring matrix, preScore

    #trace back here ->
    align_pre = []
    align_query = []
#    align_state = []

    tempScore = bestscore
    temp_i = best_i
    temp_j = best_j
    while (temp_j > 0):
          if preMaxScore[temp_i][temp_j] == 0:
             align_pre.append(prefix[temp_j-1])
             align_query.append(query[temp_i-1])
#             align_state.append("M")
             temp_i = temp_i - 1
             temp_j = temp_j - 1
             tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 1:
               align_pre.append("-")
               align_query.append(query[temp_i-1])
#               align_state.append("D")
               temp_i = temp_i - 1
               temp_j = temp_j
               tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 2:
               align_pre.append(prefix[temp_j-1])
               align_query.append("-")
#               align_state.append("I")
               temp_i = temp_i
               temp_j = temp_j - 1
               tempScore = preScore[temp_i][temp_j]
    return bestscore


def sw_tR_simple(query,tR, T, T_p,len_prefix, P,T_rowMax,pen=-2):
    p = len_prefix
    t = len(tR)
    for i in range(1,len(query)+1):
        m = -1
        if query[i-1] == tR[0]:
            m = 1        
        vals = [P[i-1][p] + m,
                T[i-1][t] + m]
                
        T[i][1] = max(vals)
        T_p[i][1] = 3 + np.argmax(vals)

        for j in range(2,len(tR)+1):
            m = -1
            if query[i-1] == tR[j-1]:
                m = 1
            vals = [T[i-1][j] + pen,
                    T[i][j-1] + pen,
                    T[i-1][j-1] + m,
                    P[i-1][p] + m]
            T[i][j] = max(vals)
            T_p[i][j] = np.argmax(vals)
   # print T
   # print T_p
    for k in range(len(query)+1):
        temp = T[k][0]
        temp_tR = 0
        for l in range(1,len(tR)+1):
            if temp < T[k][l]:
               temp = T[k][l]
               temp_tR = l
        T_rowMax[k][0] = temp
        T_rowMax[k][1] = temp_tR
    

def tb_tR_simple (query, tR, T, T_p):
    t = len(tR)
    temp = 0
    maxTup = (0,0)
    for i in range(len(query)+1):
        for j in range(len(tR)+1):
            if temp < T[i][j]:
               temp = T[i][j]
               maxTup = (i,j)
    pre = False
    i,j = maxTup
    qList = []
    tList = []
  #  print "i,j:", i,j
    while (not (pre)):
        if (T_p[i][j] == 0):
            qList.append(query[i-1])
            tList.append("-")
            i += - 1
        elif (T_p[i][j] == 1):
            qList.append("-")
            tList.append(tR[j-1])
            j += - 1
        elif (T_p[i][j] == 2):
            qList.append(query[i-1])
            tList.append(tR[j-1])            
            i += - 1
            j += - 1
        elif (T_p[i][j] == 3):
            qList.append(query[i-1])
            tList.append(tR[j-1])            
            i += - 1
            pre = True
        else:
            qList.append(query[i-1])
            tList.append(tR[j-1])            
            i += -1
            j = t
#    print "In tr traceback"
#    print "queryAligned in tr", "".join(qList)[::-1]
#    print "trAligned in query", "".join(tList)[::-1]
    return 0

def sw_suffix(query,suffix,sufScore,sufMaxScore,len_tR,tRScore,tR_rowMax,pen=-2):
#    print "suffix"
#    print "".join(suffix)
#    print "Length of suffix:", len(suffix)
    
    for i in range(1, len(query)+1):
        m = -1
        if query[i-1] == suffix[0]:
           m = 1
        vals = tR_rowMax[i-1][0]+m
        sufScore[i-1][1] = vals
        sufMaxScore[i-1][1] = 3
        for j in range(2, len(suffix)+1):
            m = -1
            if query[i-1] == suffix[j-1]:
               m = 1
            vals = [sufScore[i-1][j-1] + m,
                    sufScore[i-1][j] + pen,
                    sufScore[i][j-1] + pen]
            tempScore = max(vals)
            tempmaxScore = 0
            for k in range(0,3):
                if tempScore == vals[k]:
                   tempmaxScore = k
            sufScore[i][j] = tempScore
            sufMaxScore[i][j] = tempmaxScore
 
def tb_suffix(query,suffix,S,S_p):
    temp = 0
    i = 0
    j = len(suffix)
    # Finds the maximal value for the suffix to begin tb
    for k in range(len(query)+1):
        if temp < S[k][j]:
           temp = S[k][j]
           i = k
    bestscoresuffix = temp

    qList = []
    sList = []
#    print "best_i, best_j:", i, j, " score: ", temp
    pre = False
    index_i_tR = 0
    while (not (pre)):
        if (S_p[i][j] == 1):
            qList.append(query[i-1])
            sList.append("-")
            i += - 1
        elif (S_p[i][j] == 2):
            qList.append("-")
            sList.append(suffix[j-1])
            j += - 1
        elif (S_p[i][j] == 0):
            qList.append(query[i-1])
            sList.append(suffix[j-1])
            i += - 1
            j += - 1
        elif (S_p[i][j] == 3):
            qList.append(query[i-1])
            sList.append(suffix[j-1])
            j += - 1
            i += -1
            pre = True
            scoreAtExit = S[i+1][j+1]
    index_i_tR = i
    print "Best score from sw_suffix"
    print bestscoresuffix
#    print "index i in tR where suffix tb exits:"
#    print index_i_tR
#    print "query aligned to suffix"
#    print "".join(qList)[::-1]
#    print "query suffix aligned to query"
#    print "".join(sList)[::-1]
 
    return index_i_tR, bestscoresuffix

def finalTrace(query,tR,P,P_p,T,T_p,tR_rowMax,index_i_tR):
    i = index_i_tR
    j = tR_rowMax[i][1]
    scoretRentry = T[i][j]
    t = len(tR)
    pre = False
    qList = []
    tList = []
    while (not (pre)):
        if (T_p[i][j] == 0):
            qList.append(query[i-1])
            tList.append("-")
            i += - 1
        elif (T_p[i][j] == 1):
            qList.append("-")
            tList.append(tR[j-1])
            j += - 1
        elif (T_p[i][j] == 2):
            qList.append(query[i-1])
            tList.append(tR[j-1])
            i += - 1
            j += - 1
        elif (T_p[i][j] == 3):
            qList.append(query[i-1])
            tList.append(tR[j-1])
            i += - 1
            pre = True
            scoretRexit = T[i+1][j]
        else:
            qList.append(query[i-1])
            tList.append(tR[j-1])
            i += -1
            j = t
    tList.reverse()
    qList.reverse()
    repeatAligned = []
    repeatAligned.append(''.join(tList))
    repeatAligned.append(''.join(qList))
#    print "In final Trace" 
#    print "".join(qList)
    print "Bestscore in tR:"
    print scoretRentry
#    print "score at prefix entry:"
#    print P[i][len(prefix)]
    print "In final Trace"
    print repeatAligned
    
    tempTList = []
    for i in range(0, len(tList)):
        if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
           tempTList.append(tList[i])

    tRLength = len(tempTList)

    str_tR = "".join(tR)
    flag = False
    count_st = 0
    while (not (flag)):
        temp = []
        for k in range(len(tR)):
            temp.append(tempTList[k+count_st])
        temp_str = "".join(temp)
        if str_tR == temp_str:
           flag = True
        else:
           count_st += 1
#    print "Extra bases at start", count_st
    
    flag = False 
    count_end = tRLength
    while (not (flag)):
        temp = []
        for k in range(len(tR)):
            temp.append(tempTList[count_end-1-k])
#        print "last four bases in tR repeat", ''.join(temp)
        temp.reverse()
        temp_str = "".join(temp)
#        print "after reverse", temp_str
        if str_tR == temp_str:
           flag = True
        else:
           count_end += -1    
    extraBaseEnd = tRLength - count_end
#    print "Extra bases at end", extraBaseEnd

    number_tR = (tRLength - count_st - extraBaseEnd)/len(tR)
#    print "Number of tandem repeats = ", number_tR
    return number_tR, repeatAligned, scoretRentry
    

def alignRegions(query,pre_suf_tR):
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
    suffix = []
    suffix = list(pre_suf_tR[1])
    len_suffix = len(suffix)
    tR = []
    tR = list(pre_suf_tR[2])
    len_tR = len(tR)

    preScore = np.zeros((len(query)+1,len(prefix)+1),dtype=int)
    # Added in initialization for prefix
    preMaxScore = np.zeros((len(query)+1,len(prefix)+1),dtype=int)-1
    for j in range (1, len(prefix)+1):
        preScore[0][j] = preScore[0][j-1] -1 
    Prefix_BestScore = 0 
    Prefix_BestScore = sw_prefix(query,prefix,preScore,preMaxScore)

    tRScore = np.zeros((len(query)+1,len(tR)+1),dtype=int)-10000000
    tRMaxScore = np.zeros((len(query)+1,len(tR)+1),dtype=int)-1
    tR_rowMax = np.zeros((len(query)+1,2),dtype=int)
    sw_tR_simple(query,tR,tRScore,tRMaxScore,len_prefix,preScore,tR_rowMax)
    #tb_tR_simple(query,tR,tRScore,tRMaxScore)

    index_i_tR = 0
    Suffix_BestScore = 0
    sufScore = np.zeros((len(query)+1,len(suffix)+1),dtype=int)-10000000
    sufMaxScore = np.zeros((len(query)+1,len(suffix)+1),dtype=int)-1
    sw_suffix(query,suffix,sufScore,sufMaxScore,len_tR,tRScore,tR_rowMax)
    index_i_tR,Suffix_BestScore = tb_suffix(query,suffix,sufScore,sufMaxScore)
   
    number_tR = 0.0
    tR_maxScore = 0
    repeatAligned = [] 
    number_tR, repeatAligned, tR_maxScore = finalTrace(query,tR,preScore,preMaxScore,tRScore,tRMaxScore,tR_rowMax,index_i_tR)    
    return number_tR, repeatAligned, Prefix_BestScore, Suffix_BestScore, tR_maxScore 
#####
   

def calculateRegions_tR(flag,target, rev_target, trf_start, trf_end, target_start, target_end,refTrf_seq, flankLen):
    listPreSuf_tR = []
    tempTarget = []
    prefix_tR = ""
    suffix_tR = ""
    lengthOfPreX = trf_start - target_start
    lengthOfSufX = target_end - target_start
    startSufX = trf_end - target_start
    
    if flag == True:
       tempRevTarget = rev_target
       rev_prefix_tR = []
       for j in range(0,lengthOfPreX):
           rev_prefix_tR.append(tempRevTarget[j])
       suffix_tR = rc(''.join(rev_prefix_tR))
    
       rev_suffix_tR = []
       for k in range(startSufX, lengthOfSufX):
           rev_suffix_tR.append(tempRevTarget[k])
       prefix_tR = rc(''.join(rev_suffix_tR))

       refTrf_seq = rc(refTrf_seq)
    else:
#       print "I am in flag = false"
       tempTarget = target 
       prefix_tR = []
       for j in range(0,lengthOfPreX):
           prefix_tR.append(tempTarget[j])

       suffix_tR = []
       for k in range(startSufX, lengthOfSufX):
           suffix_tR.append(tempTarget[k])
       prefix_tR = ''.join(prefix_tR)
       suffix_tR = ''.join(suffix_tR)

    listPreSuf_tR.append(prefix_tR[-flankLen:])
    listPreSuf_tR.append(suffix_tR[0:flankLen])
    listPreSuf_tR.append(refTrf_seq)       
    
#    print listPreSuf_tR
    return listPreSuf_tR
 
def align_main():
    #input 1: m5 file
    inm5fn = sys.argv[1]
    tempstr = inm5fn.rstrip("\n").split("/")
   
    outdir = tempstr[-2]
    outfn = tempstr[-1]
    outStr = "/hpc/users/ummata01/gitrepos/workIn/repeatAnalysis/" + outdir + "/" + outdir + "_" + outfn
    
#    outStr = "test.txt"
    outfile = open(outStr,'w')
    outfile.write("query_id targetStart targetEnd RefTandemRepeat TrInQuery RefcopyNum PreScore SufScore tR_Score TrAligned QueryAligned\n")
    #input 2: txt file with binned queries
    inbinfn = sys.argv[2]

    #input 3: length of region flanking TR to align to 
    flankLen = int(sys.argv[3])

    binData = {}
    dictforbindata(binData,inbinfn)
    print "Db for bin file formed, with number of lines"
    print len(binData)
    queryAligns = {}
    dictforqueries(queryAligns,inm5fn)
#form a dictionary with the *.txt file. This dictionary could be formed everytime when a new query is started.
    print "Db for queries formed, with number of items"
    print len(queryAligns)
    for query,hit in queryAligns.items():
          if hit.query_id in binData:
             pre_suf_tR = []
#             print "Target",hit.TargetStr
             flag = False # assume its in positive strand
#             print "query",hit.QueryStr
             if hit.target_strand == 0:
                flag = True #1 means its the negative strand
#             print flag
             tempLine = binLine()
             tempLine = binData[hit.query_id] 
#            print tempLine.refTrf_st, tempLine.refTrf_en, "refTrf:",tempLine.refTrf_seq
             pre_suf_tR = calculateRegions_tR(flag,hit.TargetSeq,hit.RevTargetSeq,tempLine.refTrf_st,tempLine.refTrf_en,hit.target_start,hit.target_end,tempLine.refTrf_seq, flankLen)
             number_tR, repeatAligned, PreS, SufS, tR_S = alignRegions(hit.QuerySeq, pre_suf_tR)
             outfile.write("%s %d %d %s %d %d %d %d %d %s %s\n" %(hit.query_id,hit.target_start,hit.target_end,pre_suf_tR[2],number_tR,tempLine.refTrf_copyNum,PreS,SufS,tR_S,repeatAligned[0],repeatAligned[1]))
             outfile.flush()
          else: 
             outfile.write("query %s does not exist in binned file\n" %(hit.query_id))
             continue
   
align_main() 
