#!/usr/bin/env python
import sys
import os
import numpy as np

def zetaHmm(q,nq,r,nr,f_M,f_X,f_Y,b_M,b_X,b_Y,eProb_M,eProb_X,eProb_Y,tProb):
    z = np.zeros((4,4))#0=M,1=X,2=Y for both the axis

    zetaMM = np.zeros((nq,nr))
    zetaMX = np.zeros((nq,nr))
    zetaMY = np.zeros((nq,nr))
    zetaXM = np.zeros((nq,nr))
    zetaXX = np.zeros((nq,nr))
    zetaYM = np.zeros((nq,nr))
    zetaYY = np.zeros((nq,nr))
    for i in range(1,nq):#{1,nq-1}...are the bounds
        for j in range(1,nr):#{1,nr-1}
            qi_1 = getindicies(q[i-1])#{q[0],q[nq-2]}
            qi = getindicies(q[i])#{q[1],q[nq-1]}
            rj = getindicies(r[j])#{r[1],r[nr-1]}
            emmX = eProb_X[qi,qi_1]#{(q[1],q[0]),(q[nq-1],q[nq-2])}

            zetaMM[i,j] = f_M[i,j]*tProb[0,0]*eProb_M[qi,rj]*b_M[i,j] #M->M transition => advance both in i,j
            z[0,0] += f_M[i,j]*tProb[0,0]*eProb_M[qi,rj]*b_M[i,j]

            zetaMX[i,j] = f_M[i,j]*tProb[0,1]*emmX*b_X[i,j-1] #M->X transition => advance in i but not j in b_X
            z[0,1] += f_M[i,j]*tProb[0,1]*emmX*b_X[i,j-1]

            zetaMY[i,j] = f_M[i,j]*tProb[0,2]*eProb_Y[rj]*b_Y[i-1,j] #M->Y transition => advance in j but not i in b_Y
            z[0,2] += f_M[i,j]*tProb[0,2]*eProb_Y[rj]*b_Y[i-1,j]

            zetaXM[i,j] = f_X[i,j]*tProb[1,0]*eProb_M[qi,rj]*b_M[i,j] #X->M transition => advance both in i,j
            z[1,0] += f_X[i,j]*tProb[1,0]*eProb_M[qi,rj]*b_M[i,j] 

            zetaXX[i,j] = f_X[i,j]*tProb[1,1]*emmX*b_X[i,j-1] #X->X transition => advance in i but not j in b_X
            z[1,1] += f_X[i,j]*tProb[1,1]*emmX*b_X[i,j-1] 

            zetaYM[i,j] = f_Y[i,j]*tProb[2,0]*eProb_M[qi,rj]*b_M[i,j] #Y->M transition => advance both i,j
            z[2,0] += f_Y[i,j]*tProb[2,0]*eProb_M[qi,rj]*b_M[i,j] 

	    zetaYY[i,j] = f_Y[i,j]*tProb[2,2]*eProb_Y[rj]*b_Y[i-1,j] #Y->Y transition => advance in j but not i in b_Y
	    z[2,2] += f_Y[i,j]*tProb[2,2]*eProb_Y[rj]*b_Y[i-1,j] 

    z[3,0] = f_M[1,1]*b_M[0,0]
    z[3,1] = f_X[1,0]*b_X[0,0]
    z[3,2] = f_Y[0,1]*b_Y[0,0]

    tProb_bw = new_tProb(z,tProb) #get the new transition matrix

    print "zetaMM:\n ",zetaMM
    print "zetaMX:\n ",zetaMX
    print "zetaMY:\n ",zetaMY
    print "zetaXM:\n ",zetaXM
    print "zetaXX:\n ",zetaXX
    print "zetaYM:\n ",zetaYM
    print "zetaYY:\n ",zetaYY
    print "z: ",z
    print "tProb_bw:\n",tProb_bw
    eProb_M_bw,eProb_X_bw = gammaHmm(q,nq,r,nr,f_M,f_X,f_Y,b_M,b_X,b_Y,eProb_M,eProb_X,eProb_Y,tProb)
    print "eProb_M_bw:\n",eProb_M_bw
    print "eProb_X_bw:\n",eProb_X_bw
    return tProb_bw,eProb_M_bw,eProb_X_bw

def new_tProb(z,tProb):
    tProb_bw = np.zeros((5,5))
    gamma_M = z[0,0]+z[0,1]+z[0,2] #probability of transitioning from Match to any state
    if gamma_M != 0.0:
       tProb_bw[0,0] = z[0,0]/gamma_M #prob of transition M->M
       tProb_bw[0,1] = z[0,1]/gamma_M #prob of transition M->X
       tProb_bw[0,2] = z[0,2]/gamma_M #prob of transition M->Y
       tProb_bw[0,4] = tProb[0,4]
    gamma_X = z[1,0]+z[1,1] #prob of transition from X to any state
    if gamma_X != 0.0:
       tProb_bw[1,0] = z[1,0]/gamma_X #prob of transition X->M
       tProb_bw[1,1] = z[1,1]/gamma_X #prob on transition Y->M
       tProb_bw[1,4] = tProb[1,4]
    gamma_Y = z[2,0]+z[2,2] #prob of transition from Y to any state
    if gamma_Y != 0.0:
       tProb_bw[2,0] = z[2,0]/gamma_Y #prob of transition Y->M
       tProb_bw[2,2] = z[2,2]/gamma_Y #prob of transition Y->Y
       tProb_bw[2,4] = tProb[2,4]
    #other entries, with Begin/End state  
    gamma_1 = z[3,0]+z[3,1]+z[3,2] #prob of being in any state at first observation     
    for j in range(0,3):
        if gamma_1 != 0.0:
           tProb_bw[3,j] = z[3,j]/gamma_1
    return tProb_bw

def gammaHmm(q,nq,r,nr,f_M,f_X,f_Y,b_M,b_X,b_Y,eProb_M,eProb_X,eProb_Y,tProb):
    emm_M = np.zeros((4,4)) #emission expectations when in state M
    for i in range(1,nq+1):
        for j in range(1,nr+1): #(i,j in f_M matches i-1,j-1 in b_M matrix
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            emm_M[qi,rj] += f_M[i,j]*b_M[i-1,j-1]
    eProb_bw_M = new_eProb(emm_M)
    #eProb_X = [ aa,  ca,  ta,  ga]
    #          [ ac,  cc,  tc,  gc]
    #          [ at,  ct,  tt,  gt]
    #          [ ag,  cg,  tg,  gg] #first order dependence introduced here
    emm_X = np.zeros((4,4)) #emission expectations when in state X
    for i in range(1,nq-1):
        for j in range(1,nr-1):
            qi_1 = getindicies(q[i-1])
            qi = getindicies(q[i])
            emm_X[qi,qi_1] += f_X[i,j]*b_X[i-1,j-1]
    eProb_bw_X = new_eProb(emm_X)

    return eProb_bw_M,eProb_bw_X

def new_eProb(emm):
    eProb_bw = np.zeros((4,4))
    sum_k = np.zeros((4))
    for i in range(0,4):
        for j in range(0,4):
            sum_k[i] += emm[i,j]
    for i in range(0,4):
        for j in range(0,4):
            if sum_k[i] != 0.0:
               eProb_bw[i,j] = emm[i,j]/sum_k[i]
    return eProb_bw

def forwardProb(q,nq,r,nr,f_M,f_X,f_Y,eProb_M,eProb_X,eProb_Y,tProb):
    #Initialization of the probability matrices
    f_M[0,0], f_X[0,0], f_Y[0,0] = tProb[3,0], tProb[3,1], tProb[3,2]
    for i in range(1,nq+1):
        qi = getindicies(q[i-1]) #seq in q at i position concantenated with a gap
        if i == 1:
           emm = 1.0 #a special case as emmisions in X state are conditional : RETHINK about this!!!
        else:
           qi_1 = getindicies(q[i-2])
           emm = eProb_X[qi,qi_1]
        f_X[i,0] = emm*(tProb[0,1]*f_M[i-1,0] + tProb[1,1]*f_X[i-1,0])
        f_M[i,0] = 0.0
        f_Y[i,0] = 0.0
    for j in range(1,nr+1):
        rj = getindicies(r[j-1]) #seq in r at j position concantenated with a gap
        f_Y[0,j] = eProb_Y[rj]*(tProb[0,2]*f_M[0,j-1] + tProb[2,2]*f_Y[0,j-1])
        f_M[0,j] = 0.0
        f_X[0,j] = 0.0
    for j in range(1,nr+1):
        for i in range(1,nq+1):
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            if i == 1:
               emm = 1.0
            else:
               qi_1 = getindicies(q[i-2])
               emm = eProb_X[qi,qi_1]
            f_M_i_j = eProb_M[qi,rj]*(tProb[0,0]*f_M[i-1,j-1] + tProb[1,0]*f_X[i-1,j-1] + tProb[2,0]*f_Y[i-1,j-1])
            f_X_i_j = emm*(tProb[0,1]*f_M[i-1,j] + tProb[1,1]*f_X[i-1,j])
            f_Y_i_j = eProb_Y[rj]*(tProb[0,2]*f_M[i,j-1] + tProb[2,2]*f_Y[i,j-1])
            sum_all_state_i_j = 1.0
            f_M[i,j] = f_M_i_j/sum_all_state_i_j
            f_X[i,j] = f_X_i_j/sum_all_state_i_j
            f_Y[i,j] = f_Y_i_j/sum_all_state_i_j
    return f_M,f_X,f_Y

def backwardProb(q,nq,r,nr,b_M,b_X,b_Y,eProb_M,eProb_X,eProb_Y,tProb):
    b_M[nq-1,nr-1], b_X[nq-1,nr-1], b_Y[nq-1,nr-1] = 1.0, 1.0, 1.0 #or these could be tau (prob Any_state->End_state)
    for i in range(nq-2,-1,-1):#nq-2,...,1,0
        qi = getindicies(q[i])
        qi_1 = getindicies(q[i+1])
        emm_X = eProb_X[qi_1,qi]
        b_X[i,nr-1] = tProb[1,0]*emm_X*b_X[i+1,nr-1]
        b_M[i,nr-1] = tProb[0,1]*emm_X*b_X[i+1,nr-1] 
        b_Y[i,nr-1] = 0.0
    for j in range(nr-2,-1,-1):#nr-2,...,1,0
        rj_1 = getindicies(r[j+1])
        emm_Y = eProb_Y[rj_1]
        b_X[nq-1,j] = 0.0
        b_M[nq-1,j] = tProb[0,2]*emm_Y*b_Y[nq-1,j+1] 
        b_Y[nq-1,j] = tProb[2,2]*emm_Y*b_Y[nq-1,j+1]
    for j in range(nr-2,-1,-1):#nr-2,...,1,0
        for i in range(nq-2,-1,-1):#nq-2,...,1,0
            qi = getindicies(q[i])
            qi_1 = getindicies(q[i+1])
            rj_1 = getindicies(r[j+1])
            #emission probabilities for each state given the observation
            emm_X = eProb_X[qi_1,qi]
            emm_Y = eProb_Y[rj_1]
            emm_M = eProb_M[qi_1,rj_1]
            #recursion 
            b_X_i_j = tProb[1,0]*emm_M*b_M[i+1,j+1]+tProb[1,1]*emm_X*b_X[i+1,j]
            b_M_i_j = tProb[0,0]*emm_M*b_M[i+1,j+1]+tProb[0,1]*emm_X*b_X[i+1,j]+tProb[0,2]*emm_Y*b_Y[i,j+1] 
            b_Y_i_j = tProb[2,0]*emm_M*b_M[i+1,j+1]+tProb[2,2]*emm_Y*b_Y[i,j+1]
            sum_all_state_i_j = 1.0
            b_M[i,j] = b_M_i_j/sum_all_state_i_j
            b_X[i,j] = b_X_i_j/sum_all_state_i_j
            b_Y[i,j] = b_Y_i_j/sum_all_state_i_j
    return b_M,b_X,b_Y

def getindicies(q):
    if q == 65: #its an A
       qi = 0
    if q == 67: #its a C
       qi = 1
    if q == 84: #its a T
       qi = 2
    if q == 71: #its a G
       qi = 3
    return qi

def numTR(f_M,f_X,f_Y,ntr,nr,nq):
    endAt = np.zeros((nr+1),dtype=np.float)
    RandProb = np.zeros((nr+1),dtype=np.float)
    temp = 0.0
    index = 0
    for j in range(1,nr+1):
        if j%ntr == 0:
           m = f_M[nq,j]
           x = f_X[nq,j]
           y = f_Y[nq,j]
           endAt[j] = m+x+y
           if endAt[j] >= temp:
              temp = endAt[j]
              index = j
    MaxfwdProb = temp
    ProbRandom = randomModel(nq,index)
    print "Max forward probability: ",MaxfwdProb
    llr = np.log(MaxfwdProb/ProbRandom)
    #computing log-odds ratio from max. fwd prob. of hmm w.r.t the random model
    print "Log-Odds ratio Score: ", llr
    #This is the measure of the likelihood that two sequences, occuring as aligned pairs, are related by any alignment, as opposed to being unrelated!
    if (llr > 0.0):
       allW = np.sum(endAt)
       num = 0.0
       for j in range(1,nr+1):
         if j%ntr == 0:
            num = num + (j/ntr)*endAt[j]/allW
    else:
       num = 0.0
       print "Log-Odds ratio Score < 0.0"
    return num, llr

def randomModel(nq,nr):
    eta = 0.05
    g = 0.25
    pqr = 1.0
    for i in range(1,nq+nr+1):
        pqr = pqr*g
    t = (1.0-eta)**(nq+nr+1)*eta*eta
    probRand = t*pqr
    lprobRand = -1.0*np.log(probRand)
#    print "Random model probability: ", probRand
    return probRand

if  __name__== '__main__':
    ###
    m = 0.985
    um = 0.005
    #indices on both axis: 0=A,1=C,2=T,3=G
    eProb_M = np.array([[ m,  um,  um,  um],\
              [ um,  m,  um,  um],\
              [ um,  um,  m,  um],
              [ um,  um,  um,  m]])
    #indices on both axis: 0=A,1=C,2=T,3=G
    eProb_X = np.array([[ 0.05,  0.55,  0.35, 0.05],\
              [ 0.04,  0.58,  0.33,  0.05],\
              [ 0.05,  0.58,  0.35,  0.02],\
              [ 0.05,  0.55,  0.35,  0.05]]) 
    pen = 1.0
    eProb_Y = np.array([pen, pen, pen, pen]) 
    #state transition probability matrix
    ex,ey = 0.1,0.1 
    dx,dy = 0.15,0.05 
    tau = 0.0 
    #indices on both axis: 0=M,1=X,2=Y,3=Begin,4=End 
    tProb = np.array([[ 1.0-dx-dy-tau,  dx,  dy,  0.,  tau],\
              [ 1.0-ex-tau,  ex,  0.,  0.,  tau],\
              [ 1.0-ey-tau,  0.,  ey,  0.,  tau],\
              [ 1.0-dx-dy-tau,  dx,  dy,  0.,  tau],\
              [ 0.,  0.,  0.,  0.,  0.]])
    ###
    QuerySeq = 'TCTCTCTCTCTCTCTTCTTCCTCTCTCTCCTCTCGTCTCCTCTCTCTTCTCCTTCTCTCTCTCCTCTCTCCCTCTTCTCTCACTCTCTTCCTCTCTTCTCGTTCCCTCTCTCAC'
    #QuerySeq = 'TCTC'
    trSeq = 'TC'
    Qnum = 60 #estimated large number
    nq = len(QuerySeq)
    ntr = len(trSeq)
    q = np.arange(nq,dtype=np.int_)
    tempQ = map(ord,QuerySeq)
    q = np.asarray(tempQ)
    print "q: ",QuerySeq

    repeatSeq = trSeq*(Qnum)
    nr = len(repeatSeq)
    r = np.arange(nr,dtype=np.int_)
    tempR = map(ord,repeatSeq)
    r = np.asarray(tempR)
    #np.set_printoptions(precision=4)
    np.set_printoptions(suppress=True)
    f_M = np.zeros((nq+1,nr+1))
    f_X= np.zeros((nq+1,nr+1))
    f_Y = np.zeros((nq+1,nr+1))
    f_M,f_X,f_Y= forwardProb(q,nq,r,nr,f_M,f_X,f_Y,eProb_M,eProb_X,eProb_Y,tProb)
    print "f_M"
    print f_M
    print "f_X"
    print f_X
    print "f_Y"
    print f_Y
    P_O_init = f_M[nq,nr]+f_Y[nq,nr]+f_X[nq,nr]
    print "P(O|model): ", P_O_init
    b_M = np.zeros((nq,nr))
    b_X = np.zeros((nq,nr))
    b_Y = np.zeros((nq,nr))
    b_M,b_X,b_Y= backwardProb(q,nq,r,nr,b_M,b_X,b_Y,eProb_M,eProb_X,eProb_Y,tProb)
    print "b_M"
    print b_M
    print "b_X"
    print b_X
    print "b_Y"
    print b_Y
    num_TR,llr = numTR(f_M,f_X,f_Y,ntr,nr,nq)
    print "num_TR: ",num_TR,"llr: ",llr
    tProb_bw=zetaHmm(q,nq,r,nr,f_M,f_X,f_Y,b_M,b_X,b_Y,eProb_M,eProb_X,eProb_Y,tProb) 
    print tProb
