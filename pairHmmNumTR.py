#!/usr/bin/env python
import sys
import os
import numpy as np
import pairHmmFWD as pHf
import math
import time
#from scipy.misc import logsumexp as lse


#GLOBAL VARIABLE#################################
TableLen = 16000
scale = 1000
lnExpTable = np.zeros((TableLen),dtype=np.double)
#################################################

def forwardProb(q,nq,r,nr,f_M,f_X,f_Y,eProb_M,eProb_X,eProb_Y,tProb):
    #Initialization of the probability matrices
    f_M[0,0], f_X[0,0], f_Y[0,0] = tProb[3,0], tProb[3,1], tProb[3,2]
    #f_M[0,0], f_X[0,0], f_Y[0,0] = 1.0,0.0,0.0
    for i in range(1,nq+1):
        qi = getindicies(q[i-1]) #seq in q at i position concantenated with a gap
        if i == 1:
           emm = 0.25 #a special case as emmisions in X state are conditional : RETHINK about this!!!
           f_X[i,0] = emm*(f_X[i-1,0])
        else:
           qi_1 = getindicies(q[i-2])
           emm = eProb_X[qi,qi_1]
           f_X[i,0] = emm*(tProb[0,1]*f_M[i-1,0] + tProb[1,1]*f_X[i-1,0] + tProb[2,1]*f_Y[i-1,0])
    for j in range(1,nr+1):
        rj = getindicies(r[j-1]) #seq in r at j position concantenated with a gap
        if j == 1:
           f_Y[0,j] = eProb_Y[rj]*(f_Y[0,j-1])
        else:
           f_Y[0,j] = eProb_Y[rj]*(tProb[0,2]*f_M[0,j-1] + tProb[2,2]*f_Y[0,j-1] + tProb[1,2]*f_X[0,j-1])
    for j in range(1,nr+1):
        for i in range(1,nq+1):
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            if i == 1:
               emm = 0.25
            else:
               qi_1 = getindicies(q[i-2])
               emm = eProb_X[qi,qi_1]
            if i == 1 and j == 1:
               f_M[i,j] = eProb_M[qi,rj]*(f_M[i-1,j-1])
            else:
               f_M[i,j] = eProb_M[qi,rj]*(tProb[0,0]*f_M[i-1,j-1] + tProb[1,0]*f_X[i-1,j-1] + tProb[2,0]*f_Y[i-1,j-1])
            f_X[i,j] = emm*(tProb[0,1]*f_M[i-1,j] + tProb[1,1]*f_X[i-1,j] + tProb[2,1]*f_Y[i-1,j])
            f_Y[i,j] = eProb_Y[rj]*(tProb[0,2]*f_M[i,j-1] + tProb[2,2]*f_Y[i,j-1] + tProb[1,2]*f_X[i,j-1])
    return f_M,f_X,f_Y
#++++

def forwardProbLog(q,nq,r,nr,Lf_M,Lf_X,Lf_Y,eProb_M,eProb_X,eProb_Y,tProb):
    LtProbMM = eln(tProb[0,0])
    LtProbMX = eln(tProb[0,1])
    LtProbMY = eln(tProb[0,2])

    LtProbXM = eln(tProb[1,0])
    LtProbXX = eln(tProb[1,1])
    LtProbXY = eln(tProb[1,2])
    
    LtProbYM = eln(tProb[2,0])
    LtProbYX = eln(tProb[2,1])
    LtProbYY = eln(tProb[2,2])
   
    Lf_M[0,0], Lf_X[0,0], Lf_Y[0,0] = np.log(tProb[3,0]), np.log(tProb[3,1]), np.log(tProb[3,2])

    for i in range(1,nq+1):
        qi = getindicies(q[i-1]) #seq in q at i position concantenated with a gap
        if i == 1:
           emmX = 0.25
           Lf_X[i,0] = eln(emmX) + eln(tProb[3,1]) 
        else:
           qi_1 = getindicies(q[i-2])
           emmX = eProb_X[qi,qi_1]
           LemmX = eln(emmX)
           Lf_X[i,0] = LemmX + log_sum(log_sum(LtProbMX+Lf_M[i-1,0],LtProbXX+Lf_X[i-1,0]),LtProbYX+Lf_Y[i-1,0]) 

    for j in range(1,nr+1):
        rj = getindicies(r[j-1]) #seq in r at j position concantenated with a gap
        LemmY = eln(eProb_Y[rj])
        if j == 1:
           Lf_Y[0,j] = LemmY + eln(tProb[3,2])
        else:
           Lf_Y[0,j] = LemmY + log_sum(log_sum(LtProbMY+Lf_M[0,j-1],LtProbYY+Lf_Y[0,j-1]),LtProbXY+Lf_X[0,j-1]) 

    for j in range(1,nr+1):
        for i in range(1,nq+1):
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            LemmM = eln(eProb_M[qi,rj])
            LemmY = eln(eProb_Y[rj])
            if i == 1:
               LemmX = eln(0.25)
            else:
               qi_1 = getindicies(q[i-2])
               LemmX = eln(eProb_X[qi,qi_1])
            if i == 1 and j == 1:
               Lf_M[i,j] = LemmM + eln(tProb[3,0])
            else:
               Lf_M[i,j] = LemmM + log_sum(log_sum(LtProbMM+Lf_M[i-1,j-1],LtProbXM+Lf_X[i-1,j-1]),LtProbYM+Lf_Y[i-1,j-1])
            Lf_X[i,j] = LemmX + log_sum(log_sum(LtProbMX+Lf_M[i-1,j],LtProbXX+Lf_X[i-1,j]),LtProbYX+Lf_Y[i-1,j])
            Lf_Y[i,j] = LemmY + log_sum(log_sum(LtProbMY+Lf_M[i,j-1],LtProbXY+Lf_X[i,j-1]),LtProbYY+Lf_Y[i,j-1])
    return Lf_M,Lf_X,Lf_Y

###UTILITY FUNCTIONS FOR LOG/EXP SPACE###

def log_sum_prev(a,b):
    if a > b:
       return a + eln(1.0 + eexp(b-a))
    else:
       return b + eln(1.0 + eexp(a-b))

def InitLookupTable():
    for i in range(0,TableLen):
        lnExpTable[i] = eln(1.0+eexp((float(-1.0*i/scale))))
    print "Message: LookUp Table initiated"
#def LogExpLookup(x):
    

def log_sum(a,b):
    max = a if (a >= b) else b
    min = b if (b < a) else a
    #return max if (min == -1.0*float("inf") or (max-min) >= 15.7) else (max + eln(1.0 + eexp(min-max)))
    return max if (min == -1.0*float("inf") or (max-min) >= 15.7) else (max + lnExpTable[int((max-min)*scale)])

def eexp(x):
    if math.isnan(x) != True:
       return np.exp(x)
    else:
       print "x is nan"
       return 0

def eln(x):
    if x > 0.0:
       return np.log(x)
    else:
       print "x <= 0.0 encountered"

########################################
def getindicies(q):
    if q == 65: #its an A
       qi = 0
    if q == 67: #its a C
       qi = 1
    if q == 84: #its a T
       qi = 2
    if q == 71: #its a G
       qi = 3
    return qi

def numTR(f_M,f_X,f_Y,ntr,nr,nq):
    endAt = np.zeros((nr+1),dtype=np.float)
    temp = 0.0
    index = 0
    for j in range(1,nr+1):
        if j%ntr == 0:
           m = f_M[nq,j]
           x = f_X[nq,j]
           y = f_Y[nq,j]
           endAt[j] = m+x+y
           if endAt[j] >= temp:
              temp = endAt[j]
              index = j
    #print "endAt_in_numTR:\n",endAt
    MaxfwdProb = temp
    ProbRandom = randomModel2(nq,index)
    print "Max forward probability: ",MaxfwdProb
    print "Occurs at index: ",index
    llr = np.log(MaxfwdProb/ProbRandom)
    #computing log-odds ratio from max. fwd prob. of hmm w.r.t the random model
    print "Log-Odds ratio Score: ", llr
    #This is the measure of the likelihood that two sequences, occuring as aligned pairs, are related by any alignment, as opposed to being unrelated!
    if (llr > 0.0):
       allW = np.sum(endAt)
       print "All W sum: ",(allW)
       num = 0.0
       for j in range(1,nr+1):
         if j%ntr == 0:
            num = num + (j/ntr)*endAt[j]/allW
    else:
       num = 0.0
       print "Log-Odds ratio Score < 0.0"
    return num, llr, index


def numTRlog(Lf_M,Lf_X,Lf_Y,ntr,nr,nq):
    LendAt = np.zeros((nr+1),dtype=np.float)
    temp = -1.0*float("inf")
    index = 0
    for j in range(1,nr+1):
        if j%ntr == 0:
           #LendAt[j] = np.log(np.exp(Lf_M[nq,j])+np.exp(Lf_X[nq,j])+np.exp(Lf_Y[nq,j])) #this gives right answer...
           LendAt[j] = log_sum(log_sum(Lf_M[nq,j],Lf_X[nq,j]),Lf_Y[nq,j]) 
           if LendAt[j] >= temp:
              temp = LendAt[j]
              index = j
    LMaxfwdProb = temp

    LProbRandom = randomModel(nq,index)
    print "Max forward probability in Log space: ", LMaxfwdProb
    print "Occurs at index: ",index

    llr = LMaxfwdProb - LProbRandom
    print "Log-Odds ratio Score: ", llr
    
    if (llr > 0.0):
       SumAllW = calLogSumSeries(LendAt,nr,ntr,0)
       print "All W sum in Log: ",(SumAllW)#, "sumL: ",sumL
       LogNumTR = calLogSumSeries(LendAt,nr,ntr,1) - SumAllW
       num = eexp(LogNumTR)            
    else:
       num = 0.0
       print "Log-Odds ratio Score < 0.0"
    return num, llr, index

def calLogSumSeries(LendAt,nr,ntr,flag):
    LogNumInit = -1.0*float("inf")
    Lognum = 0.0
    if flag == 1:
       for j in range(1,nr+1):
           if j%ntr == 0:
              Lognum = log_sum(LogNumInit,LendAt[j]+eln(j/ntr))
              LogNumInit = Lognum
    else:
       for j in range(1,nr+1):
           if j%ntr == 0:
              Lognum = log_sum(LogNumInit,LendAt[j])
              LogNumInit = Lognum
    return Lognum

def randomModel(nq,nr):
    eta = 0.05
    g = 0.25 #prob of emitting a base = 0.25 for each base in query sequence.
    pqr = 1.0
    LprobRand = LprobRand = 2.0*np.log(eta) + (nq+nr+1)*np.log(1.0-eta) + (nq+nr)*np.log(g)
    print "LprobRand: ",LprobRand
    return LprobRand

def randomModel2(nq,nr):
    eta = 0.05
    g = 0.25
    pqr = 1.0
    for i in range(1,nq+nr+1):
        pqr = pqr*g
    t = (1.0-eta)**(nq+nr+1)*eta*eta
    probRand = t*pqr
    lprobRand = -1.0*np.log(probRand)
#    print "Random model probability: ", probRand
    return probRand

if  __name__== '__main__':
    ###
    m = 0.985
    um = 0.005
    #indices on both axis: 0=A,1=C,2=T,3=G
    eProb_M = np.array([[ m,  um,  um,  um],\
              [ um,  m,  um,  um],\
              [ um,  um,  m,  um],
              [ um,  um,  um,  m]])
    #indices on both axis: 0=A,1=C,2=T,3=G
    eProb_X = np.array([[ 0.55,  0.05,  0.05, 0.35],\
              [ 0.25,  0.25,  0.25,  0.25],\
              [ 0.25,  0.25,  0.25,  0.25],\
              [ 0.35,  0.05,  0.05,  0.55]]) 
    pen = 1.0
    eProb_Y = np.array([pen, pen, pen, pen]) 
    #state transition probability matrix
    ex,ey = 0.39,0.035
    x2y,y2x = 0.015,0.015 
    dx,dy = 0.23,0.05 
    tau = 0.015 
    #indices on both axis: 0=M,1=X,2=Y,3=Begin,4=End 
    tProb = np.array([[ 1.0-dx-dy-tau,  dx,  dy,  0.,  tau],\
              [ 1.0-ex-tau-x2y,  ex,  x2y,  0.,  tau],\
              [ 1.0-ey-tau-y2x,  y2x,  ey,  0.,  tau],\
              [ 1.0-dx-dy-tau,  dx,  dy,  0.,  tau],\
              [ 0.,  0.,  0.,  0.,  0.]])
    print "tProb:\n",tProb
    ###
    QuerySeq = 'AAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAGAAGAGGGAAGAGGAGAGAAGAGGAGGAAGAATGCGGAGAACAGAGAGAGAGAGAGAGAGAGAGGAGGAGAGAAGAGGAAAAGAGGGAAGAGAGAGGAGAGACAGGAGAGAGAG'
    #QuerySeq = 'TTATTTCTCTCTCCTTCTCTCTCTCTCTGTGCTGTCCTTTCTCTCTTCTCTCTCTCCTTCTCCCCTTTCTCTCAACTTCTCTCTCTACCTACCTCTCTCTTCCGTCTCTCT'
    #QuerySeq = 'AGAG'
    trSeq = 'AG'
    #trSeq = 'T'
    Qnum = len(QuerySeq)*int(1.3) #estimated large number
    #Qnum = 2 #estimated large number
    nq = len(QuerySeq)
    ntr = len(trSeq)
    q = np.arange(nq,dtype=np.int_)
    tempQ = map(ord,QuerySeq)
    q = np.asarray(tempQ)
    print "q: ",QuerySeq
    print "length of q: ",len(q)

    repeatSeq = trSeq*(Qnum)
    nr = len(repeatSeq)
    r = np.arange(nr,dtype=np.int_)
    tempR = map(ord,repeatSeq)
    r = np.asarray(tempR)
   
    InitLookupTable()
    #f_M = np.zeros((nq+1,nr+1))
    #f_X= np.zeros((nq+1,nr+1))
    #f_Y = np.zeros((nq+1,nr+1))
    #f_M,f_X,f_Y = forwardProb(q,nq,r,nr,f_M,f_X,f_Y,eProb_M,eProb_X,eProb_Y,tProb)
    #print "f_M:\n",f_M
    #print "f_X:\n",f_X
    #print "f_Y:\n",f_Y

    #P_O_init = f_M[nq,nr]+f_Y[nq,nr]+f_X[nq,nr]
    #print "P(O|model_original): ", P_O_init

    #num_TR,llr,index = numTR(f_M,f_X,f_Y,ntr,nr,nq)
    #print "num_TR: ",num_TR,"llr: ",llr,"index: ",index
    #end1 = time.time()
    #print "time in org fwd algo: ",end1-start1
    print "\n####\n"
    start2 = time.time()
    np.set_printoptions(suppress=True)
    Lf_M = np.zeros((nq+1,nr+1))+(-1*9999.0)
    Lf_X= np.zeros((nq+1,nr+1))+(-1*9999.0)
    Lf_Y = np.zeros((nq+1,nr+1))+(-1*9999.0)
    Lf_M,Lf_X,Lf_Y = forwardProbLog(q,nq,r,nr,Lf_M,Lf_X,Lf_Y,eProb_M,eProb_X,eProb_Y,tProb)
    #print "Lf_M:\n",Lf_M
    #print "Lf_X:\n",Lf_X
    #print "Lf_Y:\n",Lf_Y
    print "\n##done_FWD_Calc##\n"
    start3 = time.time()
    print "time in log fwd algo: ",start3-start2
    #P_O_init = np.exp(Lf_M[nq,nr])+np.exp(Lf_Y[nq,nr])+np.exp(Lf_X[nq,nr])
    #print "P(O|model_log): ", P_O_init

    num_TR,llr,index = numTRlog(Lf_M,Lf_X,Lf_Y,ntr,nr,nq)
    print "num_TR: ",num_TR,"llr: ",llr,"index: ",index
    end2 = time.time()
    print "time in num_TR: ",end2-start3

