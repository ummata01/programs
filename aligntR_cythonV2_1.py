#!/usr/bin/env python
import os
import sys
sys.path.append('/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/pHmm')
sys.path.append('/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/psCount')
import numpy as np
from io_hupac.ReadMatcherIO import parseRm5
from model.AlignmentHit import AlignmentHit
from io_hupac.binFileIO import parsebinf
from model.binLine import binLine
from model.Sequence import revcomp as rc
import time
import multiprocessing as mp
#import model.dpFuncs_sw as dpF
import dpFuncs_sw2 as dpF
from mleHmm import countsOnList as cL
from mleHmm import getGlobalProb as load
from tRpHmm import getNumTRfromPairHmm 

#GLOBAL VARIABLE
#fastaDict = {}
binData = {}

#class bSeq:
#    def __init__( self ):
#        self.query_id, self.target_id,self.target_st, self.target_end = None, None, 0, 0
#        self.tR_st, self.tR_end, self.tR_period, self.tR_copyNum = 0, 0, 0, 0.0
#        self.query_Seq, self.tR_Seq, self.prefix_Seq, self.suffix_Seq = None, None, None, None
#        self.flankLen = 0
##QueryId target_St target_End tR_St tR_End tR_Period tR_CopyNum tR_Seq PrefixSeq SuffixSeq
#
#def create_fastaDict(fastaDict,file):
#    fastaIn = open(file,'r')
#    fastaLine = fastaIn.readline()
#    while (fastaLine):
#          qidFin = fastaLine
#          qidF = qidFin.strip().split(">")[1]
#          seq = fastaIn.readline().strip()
#          fastaDict.setdefault(qidF,[]).append(seq)
#          fastaLine = fastaIn.readline()
#    print "FastaDictionary formed, with number of queries: ",len(fastaDict)
#
#def parsebSeq(file):
#    for line in open(file):
#        values = line.rstrip("\n").split()
#        hit = bSeq()
#        hit.query_id, hit.target_id, hit.target_st, hit.target_end = values[0],values[1],int(values[2]),int(values[3])
#        hit.tR_st, hit.tR_end, hit.tR_period, hit.tR_copyNum = int(values[4]), int(values[5]), int(values[6]), float(values[7])
#        hit.tR_Seq, hit.prefix_Seq, hit.suffix_Seq = values[8],values[9],values[10]
#        hit.flankLen = int(sys.argv[3]) 
#        if hit.query_id in fastaDict:
#           print "I am in fastaDict"
#           hit.query_Seq = fastaDict[hit.query_id][0]
#        else:
#           hit.query_Seq = 'None'
#        yield hit
#
#def dictforqueries_bSeq(queryAligns,file):
#    for hit in parsebSeq(file):
#        queryAligns[hit.query_id] = hit
   
def dictforbindata(binData,binfile):
    for line in parsebinf(binfile):
        binData.setdefault(line.query_id,[]).append(line)
    print "Length of binData: ", len(binData)

def dictforqueries(queryAligns,m5file):
    for hit in parseRm5(m5file):
        queryAligns[hit.query_id] = hit

def tb_prefix(query,prefix,P,P_p,iPre):
    pre = False
    j = np.size(prefix)
    i = iPre
    qSeq = map(chr,query)
    preSeq = map(chr,prefix)
    qList = []
    preList= []
    #print P[i][j]
    while(j > 0): 
      if (P[i][j] > 0):
         if (P_p[i][j] == 0):
            qList.append(qSeq[i-1])
            preList.append(preSeq[j-1])
            i += -1
            j += -1
         if (P_p[i][j] == 1):
            qList.append(qSeq[i-1])
            preList.append("-")
            i += -1
         if (P_p[i][j] == 2):
            qList.append("-")
            preList.append(preSeq[j-1])
            j += -1
         #print qList, preList
      else:
         break
    preList.reverse()
    qList.reverse()
    q_pre_Aligned = []
    q_pre_Aligned.append(''.join(preList))
    q_pre_Aligned.append(''.join(qList))
    return q_pre_Aligned

def tb_suffix(query,suffix,S,S_p):
    temp = 0
    i = 0
    j = np.size(suffix)
    nq = np.size(query)
    qSeq = map(chr,query)
    sufSeq = map(chr,suffix)
    qList = []
    sufList= []
    # Finds the maximal value for the suffix to begin tb
    for k in range(nq+1):
        if temp < S[k][j]:
           temp = S[k][j]
           i = k
    bestscoresuffix = temp
    pre = False
    index_i_tR = 0
    try:
      while (not(pre)):
        if (S[i][j] > 0):
           if (S_p[i][j] == 1):
              qList.append(qSeq[i-1])
              sufList.append("-")
              i += -1
           elif (S_p[i][j] == 2):
              qList.append("-")
              sufList.append(sufSeq[j-1])
              j += -1
           elif (S_p[i][j] == 0):
              qList.append(qSeq[i-1])
              sufList.append(sufSeq[j-1])
              i += -1
              j += -1
           elif (S_p[i][j] == 3):
              qList.append(qSeq[i-1])
              sufList.append(sufSeq[j-1])
              j += -1
              i += -1
              pre = True
           index_i_tR = i
        else :
           index_i_tR = 0
           pre = True
      sufList.reverse()
      qList.reverse()
      q_suf_Aligned = []
      q_suf_Aligned.append(''.join(sufList))
      q_suf_Aligned.append(''.join(qList))
      #print "q_suf_Aligned:\n",q_suf_Aligned[0],"\n",q_suf_Aligned[1] 
    except:
      index_i_tR = -1
      bestscoresuffix = -1000
      q_suf_Aligned = ['Error_in_tb_suffix','Error_in_tb_suffix']
      return index_i_tR, bestscoresuffix,q_suf_Aligned
    else: 
      return index_i_tR, bestscoresuffix,q_suf_Aligned

def finalTrace(qArray,tR,T,T_p,tR_rowMax,index_i_tR):
    try:
      i = index_i_tR
      #print "i index in TR: ", i
      if (i > 0):
        j = tR_rowMax[i][1]
        scoretRentry = T[i][j]
        t = len(tR)
        pre = False
        query = map(chr,qArray)
        qList = []
        tList = []
        while (not (pre)):
          if (T[i][j] > 0):
           if (T_p[i][j] == 0):
              qList.append(query[i-1])
              tList.append("-")
              i += - 1
           elif (T_p[i][j] == 1):
              qList.append("-")
              tList.append(tR[j-1])
              j += - 1
           elif (T_p[i][j] == 2):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += - 1
              j += - 1
           elif (T_p[i][j] == 3):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              #open up gaps on query
              for k in range(0,j-1):
                  qList.append("-")
                  tList.append(tR[k])
              i += - 1
              j = t
           elif (T_p[i][j] == 4):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += - 1
              pre = True
              trSinQ = i
              scoretRexit = T[i+1][j]
          else:
              print "Tandem repeat score <= 0"
              scoretRexit = T[i+1][j]
              i += 1
              trSinQ = i
              break
        tList.reverse()
        qList.reverse()
        repeatAligned = []
        repeatAligned.append(''.join(tList))
        repeatAligned.append(''.join(qList))

        tempTList = []
        for i in range(0, len(tList)):
           if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
              tempTList.append(tList[i])

        tRLength = len(tempTList)
        str_tR = "".join(tR)
        str_tempTList = "".join(tempTList)
        #print "str_tempTList: ",str_tempTList
        if (t < tRLength):
             if t >= 1:
                number_tR = (tRLength)/float(t)
                naiveTR = index_i_tR - trSinQ
             else:
                number_tR = -30000.0
                naiveTR = -30000.0
        else:
          number_tR = -20000.0
          naiveTR = -20000.0
      else:
        number_tR = -10000.0
        naiveTR = -10000.0
        repeatAligned = ['','']
        scoretRentry = 0
      return number_tR, repeatAligned, scoretRentry, scoretRexit, naiveTR
    except:
      number_tR = -00000.0
      naiveTR = -00000.0
      repeatAligned = ['','']
      scoretRentry = -90000
      scoretRexit = -999999
      return number_tR, repeatAligned, scoretRentry, scoretRexit, naiveTR
 
def alignRegions(query,pre_suf_tR):
    '''INPUT: 
    query - query sequence
    pre_suf_tR = [<prefix_seq>, <suf_seq>, <tr_seq>]
    RETURNS:  
    number_tR = number of tandem repeats in query
    repeatAligned = [<TR align sequence, ref>, <TR align sequence, query>]
    Prefix_BestScore = Smith-waterman score of Prefix Score matrix
    Suffix_BestScore = SW score in suffix matrix (overall: Prefix+TR+suffix)
    tR_maxScore = SW score in tR matrix (Prefix + TR)
    tR_exitScore = SW score in tR matrix where alignment jumps to prefix matrix
    '''
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
    pArray = np.arange(len_prefix,dtype=np.int_)
    tempP = map(ord,prefix)
    tempP2 = np.asarray(tempP)
    pArray = tempP2
#    print "pArray: ", pArray

    suffix = []
    suffix = list(pre_suf_tR[1])
    len_suffix = len(suffix)
    sArray = np.arange(len_suffix,dtype=np.int_)
    tempS = map(ord,suffix)
    tempS2 = np.asarray(tempS)
    sArray = tempS2
#    print "sArray: ", sArray

    tR = []
    tR = list(pre_suf_tR[2])
    len_tR = len(tR)
    tRArray = np.arange(len_tR,dtype=np.int_)
    temptR = map(ord,tR)
    temptR2 = np.asarray(temptR)
    tRArray = temptR2
#    print "tRArray: ", tRArray

    preScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    # Added in initialization for prefix
    preMaxScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    preMaxScore.fill(-1)
    for j in xrange (1, len_prefix+1):
        preScore[0][j] = -5*j

    Prefix_BestScore = 0
    Prefix_BestScoreIndex = 0
    #Prefix_BestScore = dpF.sw_prefix(query,pArray,preScore,preMaxScore)
    Prefix_BestScore, Prefix_BestScoreIndex = dpF.sw_prefix(query,pArray,preScore,preMaxScore)
    #print "Prefix_BestScoreIndex: ",Prefix_BestScoreIndex
    # Added in initialization for tR
    tRScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)
    for j in xrange(1,len_tR+1):
        tRScore[0][j] = -10000000

    tRMaxScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)-1
    tR_rowMax = np.zeros((len(query)+1,2),dtype=np.int_)
    dpF.sw_tR_simple(query,tRArray,tRScore,tRMaxScore,len_prefix,preScore,tR_rowMax)

    index_i_tR = 0
    Suffix_BestScore = 0
    # Added in initialization for suffix
    sufScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)
    for j in xrange(1,len_suffix+1):
        sufScore[0][j] = -10000000

    sufMaxScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)-1
    dpF.sw_suffix(query,sArray,sufScore,sufMaxScore,tR_rowMax)
    index_i_tR,Suffix_BestScore,alignSufxQ = tb_suffix(query,sArray,sufScore,sufMaxScore)

    number_tR = 0.0
    naiveTR = 0.0
    tR_maxScore = 0
    repeatAligned = []
    print "Entering finalTrace for query: "
    number_tR, repeatAligned, tR_maxScore, tR_exitScore, naiveTR = finalTrace(query,tR,tRScore,tRMaxScore,tR_rowMax,index_i_tR)
    print "Done final Trace"
    #Here goes the prefix_tracebacks
    alignPrfxQ = tb_prefix(query,pArray,preScore,preMaxScore,Prefix_BestScoreIndex)
    #here query is in int, but tR is in string representation
    print "exit finalTrace..."
    return number_tR, repeatAligned, Prefix_BestScore, Suffix_BestScore, tR_maxScore, tR_exitScore, alignSufxQ, alignPrfxQ, naiveTR

#####

def align_worker_bSeq((query, hit)):
    print "worker starts for query: ", query
    start1 = time.time()
    outList = []
    pre_suf_tR = []
    flankLen = hit.flankLen
    try:
       #p = hit.prefix_Seq
       #s = hit.suffix_Seq
       p = hit.prefix_Seq[-flankLen:]
       s = hit.suffix_Seq[0:flankLen]
       tr = hit.tR_Seq
       pre_suf_tR = [p,s,tr]
    except:
       print "Error in pre_suf_tR List. Please check the sequences."
    if hit.query_id != 'None':
       loq = len(hit.query_Seq)
       qArray = np.arange(loq,dtype=np.int_)
       tempQ = map(ord,hit.query_Seq)
       tempQ2 = np.asarray(tempQ)
       qArray = tempQ2
       number_tR, repeatAligned, PreS, SufS, tR_S, tR_Ex = alignRegions(qArray, pre_suf_tR)
       outList = [hit.query_id,hit.target_id,hit.target_st,hit.target_end,pre_suf_tR[2],number_tR,hit.tR_copyNum,PreS,SufS,tR_S,tR_Ex,repeatAligned[0],repeatAligned[1],hit.tR_st,hit.tR_end,hit.tR_period]
       end1 = time.time()
       print "Duration within worker: ",end1-start1
       print "worker ends for query: ", query
       return outList
    else:
       outList = ["No query in Fasta"]
       return outList

def calculateRegions_tR(flag,target, rev_target, trf_start, trf_end, target_start, target_end,refTrf_seq, flankLen):
    listPreSuf_tR = []
    tempTarget = []
    prefix_tR = ""
    suffix_tR = ""
    #print "trf_start", trf_start
#    print "target_start", target_start
#    print "trf_end", trf_end
#    print "target_end", target_end

    lengthOfPreX = trf_start - target_start
    lengthOfSufX = target_end - target_start
    startSufX = trf_end - target_start
    len_target = len(target)
    len_revtarget = len(rev_target)

    #print "startSufX ", startSufX
    #print "lengthofSufX ",lengthOfSufX
    #print "lengthofPreX ",lengthOfPreX
    if (len_target < lengthOfSufX):
       listPreSuf_tR.append('E')
    else:
       if flag == True:
          #print "I am in flag = TRUE"
          tempRevTarget = rev_target
          rev_prefix_tR = []
          for j in xrange(0,lengthOfPreX):
              rev_prefix_tR.append(tempRevTarget[j])
          suffix_tR = rc(''.join(rev_prefix_tR))
          #print "suffix_tR: ",suffix_tR
          rev_suffix_tR = []
          for k in xrange(startSufX, lengthOfSufX):
              rev_suffix_tR.append(tempRevTarget[k])
          prefix_tR = rc(''.join(rev_suffix_tR))
          #print "prefix_tR: ",prefix_tR

          refTrf_seq = rc(refTrf_seq)
          #print "refTrf_seq: ",refTrf_seq
       else:
          #print "I am in flag = false"
          tempTarget = target
          #print " len of tempTarget ", len(tempTarget) 
          prefix_tR = []
          for j in xrange(0,lengthOfPreX):
              prefix_tR.append(tempTarget[j])

          suffix_tR = []
          for k in xrange(startSufX, lengthOfSufX):
              suffix_tR.append(tempTarget[k])
          prefix_tR = ''.join(prefix_tR)
          suffix_tR = ''.join(suffix_tR)
       #print "prefix_tR:\n",prefix_tR
       #print "prefix_tR[-flankLen:]:\n",prefix_tR[-flankLen:]
       #print "suffix_tR:\n",suffix_tR
       #print "suffix_tR[0:flankLen]:\n",suffix_tR[0:flankLen]
       #print "refTrf_seq:\n",refTrf_seq
       listPreSuf_tR.append(prefix_tR[-flankLen:])
       #listPreSuf_tR.append(prefix_tR)
       listPreSuf_tR.append(suffix_tR[0:flankLen])
       #listPreSuf_tR.append(suffix_tR)
       listPreSuf_tR.append(refTrf_seq)
    #print listPreSuf_tR
    return listPreSuf_tR


def align_worker((query, hit)):
    print "worker starts for query: ", query
    start1 = time.time()
    FinalList = []
    if hit.query_id in binData:
       loq = len(hit.QuerySeq)
       qSeq = ''.join(hit.QuerySeq)
       qArray = np.arange(loq,dtype=np.int_)
       tempQ = map(ord,hit.QuerySeq)
       tempQ2 = np.asarray(tempQ)
       qArray = tempQ2
       pre_suf_tR = []
       flag = False # assume its in positive strand
       if hit.target_strand == 0:
          flag = True #1 means its the negative strand
       tempLine = binLine()
       numInList = len(binData[hit.query_id])
       print "number of entries in list: ",numInList
       for k in range(0,numInList):
           tempLine = binData[hit.query_id][k]
           pre_suf_tR = calculateRegions_tR(flag,hit.TargetSeq,hit.RevTargetSeq,tempLine.refTrf_st,tempLine.refTrf_en,hit.target_start,hit.target_end,tempLine.refTrf_seq,flankLen)
           #print pre_suf_tR, len(pre_suf_tR)
           if (len(pre_suf_tR)) == 1:
              tempStr = "query has weird m5 alignment (check m5 file alignment)\n"
              outList = [hit.query_id, tempStr]
              #print "outList:\n",outList
           else:
              number_tR, repeatAligned, PreS, SufS, tR_S, tR_Ex, qSufAligned, qPreAligned, naiveTR = alignRegions(qArray, pre_suf_tR)
              if repeatAligned[1] != '': 
                 ###PSeudoCounts here for generating tProb and eProb values
                 if len(qPreAligned[0]) >= 100 or len(qSufAligned[0]) >= 100:
                    listOfAligned = [qSufAligned, qPreAligned]
                    tProb, eProb_M, eProb_X = cL(listOfAligned)
                    #print "tProb:\n",tProb,"\neProb_M:\n",eProb_M,"\neProb_X:\n",eProb_X
                    #Might have to condition the eProb_X matrix here on the information about the Tandem Repeat sequence....
                 else:
                    #print "Use Global probability matrices"         
                    #load global probability matrices... 
                    tProb, eProb_M, eProb_X = load(hit.target_id)
                 ###
                 tProb1, eProb_M1, eProb_X1 = load(hit.target_id)
                 #print "tProb1:\n",tProb1,"\neProb_M1:\n",eProb_M1,"\neProb_X1:\n",eProb_X1
                 ###call pHmm to est numTRs
                 tList = repeatAligned[1]
                 tempTList = []
                 for i in range(0, len(tList)):
                     if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
                        tempTList.append(tList[i])
                 trInQseq = ''.join(tempTList)
                 #print "trInQseq: ",trInQseq    
                 if np.allclose(tProb,tProb1,0.01,0.001) == True:
                    print "Local psCount pHmm start..."
                    numTR, llr, ProbMax = getNumTRfromPairHmm(trInQseq,pre_suf_tR[2],tProb,eProb_M,eProb_X) 
                    print "Local psCount pHmm End"
                    numTR1, llr1, ProbMax1 = -2,0.0,0.0 
                 else:
                    print "Local psCount pHmm start..."
                    numTR, llr, ProbMax = getNumTRfromPairHmm(trInQseq,pre_suf_tR[2],tProb,eProb_M,eProb_X) 
                    print "Local psCount pHmm End"
                    print "Global psCount pHmm start..."
                    numTR1, llr1, ProbMax1 = getNumTRfromPairHmm(trInQseq,pre_suf_tR[2],tProb1,eProb_M1,eProb_X1)
                    print "Global psCount pHmm End"
                 print "number_tR: ",number_tR," numTR: ",numTR," numTR1: ",numTR1 
                 ###
              else:
                 numTR, llr, ProbMax = -1,0.0,-1.0*float('inf')
                 numTR1, llr1, ProbMax1 = -1,0.0,-1.0*float('inf')
                 repeatAligned = ["Error","Error"]
              outList = [hit.query_id,hit.target_id,hit.target_start,hit.target_end,pre_suf_tR[2],number_tR,tempLine.refTrf_copyNum,PreS,SufS,tR_S,tR_Ex,repeatAligned[0],repeatAligned[1],tempLine.refTrf_st,tempLine.refTrf_en,tempLine.refTrf_repeat,naiveTR/float(tempLine.refTrf_repeat),numTR,llr,ProbMax,numTR1,llr1,ProbMax1]
           FinalList.append(outList)
    else:
       tempStr = "query does not exist in binned file\n"
       outList = [hit.query_id, tempStr]
       FinalList.append(outList)
    end1 = time.time()
    print "Duration within worker: ",end1-start1
    print "worker ends for query: ", query
    #print "len(FinalList): ",(FinalList)
    return FinalList

if __name__ == '__main__': 
    #'''USAGE: python aligntRMPmap3_tier2.py /RESULT.binned /allChrFasta.fa 1000 31'''
    #input 1: RESULT.binned file
    start = time.time()
    inm5fn = sys.argv[1]
    tempstr = inm5fn.rstrip("\n").split("/")
  
    # 
    ver = 'V2_1.Anchors'
    outdir = tempstr[-2]
    outfn = tempstr[-1] + ver
    outStr = "/projects/HuPac/repartition_MAR29/all_m5/bin_out4" + "/" + outdir + "/" + outfn + ".out"
    outStr4 ="/projects/HuPac/repartition_MAR29/all_m5/bin_out4" + "/" + outdir + "/" + outfn + ".Qs.fasta"
    #
 
    #outStr = "/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/3dp/test2.out"
    #outStr4 = "/hpc/users/ummata01/gitrepos/workIn/debug/pacBioTR/3dp/test2.Qs.fasta"
    
    #input 2: fasta file: needed to associate query_id with query_Seq
    #inFafn = sys.argv[2]
    inbinfn = sys.argv[2]

    #input 3: length of region flanking TR to align to 
    flankLen = int(sys.argv[3])
    #CAUTION: Something wanky with using flankLen. Need to test couple of more cases here....
    #input 4:  number of cpus to use
    ncores = int(sys.argv[4])

    dictforbindata(binData,inbinfn)
    #binData is a global dictionary
    print "No of entries in binData dict: ",len(binData)

    #Create a dictionary with <query_id:Sequence> entries
    #this should be used if the user supplies the queries in fasta and another file with prefix suffix data for a specific query
    #create_fastaDict(fastaDict,inFafn)

    #Create a dictionary with <query_id:filtered queries for a TR event with its relevant information> entries
    queryAligns = {}
    dictforqueries(queryAligns,inm5fn)
    NumOfQ = len(queryAligns)
    print "No of entries in queryAligns dict: ", NumOfQ
    #form a dictionary with the *.txt file. This dictionary could be formed everytime when a new query is started.

    try:
       pool = mp.Pool(ncores)
       print "Pool of workers created: ",ncores
       #Parallel processes are spawned where each process gets a particular <key:value> or <query_id:corresponding information about seq, TR event, prefix, suffix in the form of a bSeq() object>
       results = pool.map(align_worker, queryAligns.iteritems()) #the result from align_worker are collected in results which is of the form  [[],[],...[]], a list of lists
       #results = map(align_worker, queryAligns.iteritems()) #the result from align_worker are collected in results which is of the form  [[],[],...[]], a list of lists
       #print "len(results[0]): ", (results[0])
    except:
       print "Error with apply_async, empty or partial results list: RETRY"
    else:
       outfile = open(outStr,'w')
       outfile.write("query_id targetStart targetEnd RefTandemRepeat TrInQuery RefcopyNum PreScore SufScore tR_ScoreIn tR_ScoreOut TrAligned QueryAligney\n")
       #outfile2.write("Query_id target targetStart targetEnd ReftRStart ReftREnd RefPeriod RefNum QNum Ratio tRSeq tR_ScoreDiff NaiveTR pHmmTR llr ProbMax pHmmTRG llrG ProbMaxG\n")
       for listOfResults in results:
           #print "len of listOfResults: ",len(listOfResults)
           for result in listOfResults:
               strResult = ' '.join(map(str,result))
               outfile.write("%s\n"%(strResult))
               outfile.flush()
       outfile.close()
       
       #Writing out reads in fasta format here: direct reading from the dictionary
       outfile4 = open(outStr4,'w')
       for key,val in queryAligns.iteritems():
           outfile4.write(">%s\n%s\n"%(key,''.join(val.QuerySeq)))
           outfile4.flush()
       outfile4.close()
       duration = time.time() - start
       print "Total time for this run (secs)= ",duration
