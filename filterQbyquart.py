#!/usr/bin/env python

import sys
import os
from itertools import *
###################################################################################################
#sys.argv[1] = input R file with upper quartile - lower quartile > 20 per chr                               
#sys.argv[2] = input query bin files .qbin per chr             
#sys.argv[3] = output directory location 
#python filterQbyquart.py /hpc/users/ummata01/targets_overhang100/qbin/quartiles/results_Rchr1.txt /hpc/users/ummata01/targets_overhang100/qbin/chr1.qbin /hpc/users/ummata01/targets_overhang100/qbin/quartiles
###################################################################################################

class R:
    def __init__(self):
        self.trfS, self.trfMean, self.trfStd, self.trfMedian, self.trfLq, self.trfUq = 0,0.0,0.0,0.0,0.0,0.0

class qbin:
    def __init__(self):
        self.Query_id, self.target, self.targetStart, self.targetEnd, self.ReftRStart, self.ReftREnd, self.RefPeriod, self.RefNum, self.QNum, self.tRSeq = None,None,0,0,0,0,0,0,0,None
        #Query_id target targetStart targetEnd ReftRStart ReftREnd RefPeriod RefNum QNum tRSeq

def parseR(file):
    for line in open(file):
        templist = line.rstrip("\n").split("\t")
        hit = R()
        hit.trfS,hit.trfMean,hit.trfStd,hit.trfMedian,hit.trfLq,hit.trfUq = int(templist[0]),float(templist[1]),float(templist[2]),float(templist[3]),float(templist[4]),float(templist[5])
        yield hit

def dictforR(RDb,inm5fn):
    for hit in parseR(inm5fn):
        RDb[hit.trfS] = hit
    print "Db for queries with tr counts formed, with number of items: ", len(RDb)

def parseQbin(file):
    for line in open(file):
        values = line.rstrip("\n").split(" ")
        hit = qbin()
        hit.Query_id, hit.target, hit.targetStart, hit.targetEnd = values[0], values[1], int(values[2]),int(values[3])
        hit.ReftRStart, hit.ReftREnd, hit.RefPeriod, hit.RefNum, hit.QNum, hit.tRSeq = int(values[4]), int(values[5]), int(values[6]), int(values[7]), int(values[8]), values[9]
        yield hit

def binQtrs():
    inm5fn = sys.argv[1]

    RDb = {}
    dictforR(RDb,inm5fn)

    tempm5 = inm5fn.split("/")[-1].split(".")[0]
    outBinfn = sys.argv[3]
    hgT_outFn = outBinfn + "/" + tempm5 + ".qR"
    outfile = open(hgT_outFn, 'w')
    #housekeeping for outfile

    inBinfn = sys.argv[2]

    hits = parseQbin(inBinfn)
    for k,g in groupby(hits, lambda x: x.ReftRStart):
        hits = list(g)
        if hits[0].ReftRStart in RDb:
           if len(hits) >= 5:
                outfile.write("Stats for %s at location trfStart %d\n" %(hits[0].target,hits[0].ReftRStart))
                local = R()
                local=RDb[hits[0].ReftRStart]
                outfile.write("ReftRStart   mean   Stdev    median    25%    75%\n")
                outfile.write("%d %f %f %f %f %f\n" %(local.trfS,local.trfMean,local.trfStd,local.trfMedian,local.trfLq,local.trfUq))
                for hit in hits:
                    Ratio = 0.0
                    Ratio = float(hit.QNum)/float(hit.RefNum)
                    outfile.write("%s %s %i %i %i %i %d %d %i %.2f %s\n" %(hit.Query_id, hit.target, hit.targetStart, hit.targetEnd,hit.ReftRStart, hit.ReftREnd, hit.RefPeriod, hit.RefNum, hit.QNum, Ratio,hit.tRSeq))
                    outfile.flush()
    outfile.close()

binQtrs()
