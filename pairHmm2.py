#!/usr/bin/env python

import sys
import os
import numpy as np

def forwardProb(q,nq,tr,ntr,r,nr,f_M,f_X,f_Y,eProb,tProb,Al):
    #Initialization of the probability matrices
    f_M[0,0], f_X[0,0], f_Y[0,0] = 1.0, 0.0, 0.0
   
    #Special cases i or j = 0 for f_M,f_X,f_Y matrices:
    #first j = 0 case (all the j-1 terms are 0.0 except for in X state):
    for i in range(1,nq+1):
 	qi = Al[q[i-1]] #seq in q at i position concantenated with a gap
        f_X[i,0] = eProb[qi,4]*(tProb[0,1]*f_M[i-1,0] + tProb[1,1]*f_X[i-1,0])
        f_M[i,0] = 0.0
        f_Y[i,0] = 0.0
    #then i = 0 case:
    for j in range(1,nr+1):
 	rj = Al[r[j-1]] #seq in r at j position concantenated with a gap
        f_Y[0,j] = eProb[4,rj]*(tProb[0,2]*f_M[0,j-1] + tProb[2,2]*f_Y[0,j-1])
        f_M[0,j] = 0.0 
        f_X[0,j] = 0.0 
    #Recursion for i = 1,...,nq and j = 1,...,nr:
    logscale = np.zeros((nq+1,nr+1),dtype=np.float)+1.0
    scale = 0.0
    for i in range(1,nq+1):
        for j in range(1,nr+1):
            qi = Al[q[i-1]] 
            rj = Al[r[j-1]]
            f_M_i_j = eProb[qi,rj]*(tProb[0,0]*f_M[i-1,j-1] + tProb[1,0]*f_X[i-1,j-1] + tProb[2,0]*f_Y[i-1,j-1])
   	    f_X_i_j = eProb[qi,4]*(tProb[0,1]*f_M[i-1,j] + tProb[1,1]*f_X[i-1,j]) 
   	    f_Y_i_j = eProb[4,rj]*(tProb[0,2]*f_M[i,j-1] + tProb[2,2]*f_Y[i,j-1])
            sum_all_state_i_j = f_M_i_j + f_X_i_j + f_Y_i_j #these steps are done to perform scaling to contain overflow problems 
            logscale[i,j] = np.log(sum_all_state_i_j)
            scale = scale + logscale[i,j]
            f_M[i,j] = f_M_i_j/sum_all_state_i_j
            f_X[i,j] = f_X_i_j/sum_all_state_i_j
            f_Y[i,j] = f_Y_i_j/sum_all_state_i_j
    print "f_M\n", f_M.astype(np.longdouble)
    print "f_X\n", f_X.astype(np.longdouble)
    print "f_Y\n", f_Y.astype(np.longdouble)
    #Termination: take log
    log_Prob_q_r = np.log(tProb[0,4]) - scale #formula based on Rabinier paper with added transition probability to move to the end state
    return log_Prob_q_r, f_M, f_X, f_Y

def sample_forwardMatrix(q,nq,tr,ntr,r,nr,f_M,f_X,f_Y,eProb,tProb,Al):
    #Move on the space of these matrices based on the probability of each cell/state
    state = None
    i,j = nq, nr
    print i, j
    M = f_M[i,j]
    X = f_X[i,j]
    Y = f_Y[i,j]
    sum_i_j = M + X + Y
    m = M/sum_i_j
    x = X/sum_i_j
    y = Y/sum_i_j
    temp = np.random.random_sample()
    if 0.0 < temp <= m:
       state = 'M'
       i += -1
       j += -1
    if m < temp <= m + x:
       state = 'X'
       i += -1
    if m + x < temp <= 1.0:
       state = 'Y'
       j += -1
    flag = True
    print state
    while (flag):
        print "i: ", i, "j ", j
        if i > 0 or j > 0: 
           qi = Al[q[i-1]] 
           rj = Al[r[j-1]]
           if state == 'M':
              f_M_M = eProb[qi,rj]*(tProb[0,0]*f_M[i-1,j-1])
              f_M_X = eProb[qi,rj]*(tProb[1,0]*f_X[i-1,j-1])
              f_M_Y = eProb[qi,rj]*(tProb[2,0]*f_Y[i-1,j-1])
              sum_i_j = f_M_M + f_M_X + f_M_Y
              pMM = f_M_M/sum_i_j
              pMX = f_M_X/sum_i_j
              pMY = f_M_Y/sum_i_j
              temp = np.random.random_sample()
              print "temp: ", temp
              if 0.0 < temp <= pMM:
                 state = 'M'
              if pMM < temp <= pMM + pMX:
                 state = 'X'
              if pMM + pMX < temp <= 1.0:
                 state = 'Y'
              print state
              print pMM, pMX, pMY
              i += -1
              j += -1
           if state == 'X':
              f_X_M = eProb[qi,4]*(tProb[0,1]*f_M[i-1,j])
              f_X_X = eProb[qi,4]*(tProb[1,1]*f_X[i-1,j])
              sum_i_j = f_X_M + f_X_X
              pXM = f_X_M/sum_i_j
              pXX = f_X_X/sum_i_j
              temp = np.random.random_sample()
              print "temp: ", temp
              if 0.0 < temp <= pXM:
                 state = 'M'
              if pXM < temp <= 1.0:
                 state = 'X'
              print state
              print pXM,pXX
              i += -1
           if state == 'Y':
              f_Y_M = eProb[4,rj]*(tProb[0,2]*f_M[i,j-1])
              f_Y_Y = eProb[4,rj]*(tProb[2,2]*f_Y[i,j-1])
              sum_i_j = f_Y_M + f_Y_Y
              pYM = f_Y_M/sum_i_j
              pYY = f_Y_Y/sum_i_j
              temp = np.random.random_sample()
              print "temp: ", temp
              if 0.0 < temp <= pYM:
                 state = 'M'
              if pYM < temp <= 1.0:
                 state = 'Y'
              print state
              print pYM,pYY
              j += -1
        else:
           flag = False
    return 0 

if __name__ == '__main__':
    q = 'ACTACT'
    tr = 'ACT'
    Qnum = 2

    nq = len(q)
    ntr = len(tr)
    extraNum = 0
    r = tr*(Qnum + extraNum)#this is done to flatten the recursive structure of query vs repeat alignments 
    print r
    # tr should be a string.Qnum is the number calculated by the previous dynamic programming step
    nr = len(r)

    f_M = np.zeros((nq+1,nr+1),dtype=np.float)
    f_X= np.zeros((nq+1,nr+1),dtype=np.float)
    f_Y = np.zeros((nq+1,nr+1),dtype=np.float)
    
    #Dictionary for binding Alphabet to numbers
    Al = {'A':0, 'C':1, 'G':2, 'T':3}
    #eProb=emission probability dictionary (<base_pair>,<prob value>) 
    #tProb=transition probability matrix (5 states: M, X, Y, Begin, End) 
    m = 0.85
    um = 0.15
    pen = 0.25
    eProb = np.array([[ m,  um,  um,  um,  pen],\
              [ um,  m,  um,  um,  pen],\
              [ um,  um,  m,  um,  pen],\
              [ um,  um,  um,  m,  pen],\
              [ pen,  pen,  pen,  pen,  0.0]])
    #state transition probability matrix
    e, d, tau = 0.2, 0.10, 0.01
    
    #tProb = np.zeros((5,5),dtype=np.float)
    tProb = np.array([[ 1.0-2.0*d-tau,  d,  d,  0.,  tau],\
              [ 1.0-e-tau,  e,  0.,  0.,  tau],\
              [ 1.0-e-tau,  0.,  e,  0.,  tau],\
              [ 1.0-2.0*d-tau,  d,  d,  0.,  tau],\
              [ 0.,  0.,  0.,  0.,  0.]])
    
    Probability,f_M,f_X,f_Y = forwardProb(q,nq,tr,ntr,r,nr,f_M,f_X,f_Y,eProb,tProb,Al)
    print "Prob (in log space) of observing query & repeat (suming over all alignments): ", Probability
    #sampling of the forward matrices, where each i,j step is selected based on the respective probabilities
    #The idea is to sample many-many times to create a list of alignments & calculate their posterior probabilities (still to implement)
    #Expected value of repeats is calculated from the above sampled alignments (still to implement)
    sample_forwardMatrix(q,nq,tr,ntr,r,nr,f_M,f_X,f_Y,eProb,tProb,Al)
 
