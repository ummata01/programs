#!/usr/bin/env python
import numpy as np
cimport numpy as np

np.import_array()

def forwardProb(np.ndarray[np.int_t,ndim=1] q, int nq, np.ndarray[np.int_t,ndim=1] r, int nr, np.ndarray[np.float_t,ndim=2] f_M, np.ndarray[np.float_t,ndim=2] f_X, np.ndarray[np.float_t,ndim=2] f_Y, np.ndarray[np.float_t,ndim=2] eProb, np.ndarray[np.float_t,ndim=2] tProb):
    #Initialization of the probability matrices
    f_M[0,0] = 1.0 
    f_X[0,0] = 0.0 
    f_Y[0,0] = 0.0
    cdef int qi
    cdef int rj
    cdef float sum_all_state_i_j
    cdef int i
    cdef int j
    #first j = 0 case (all the j-1 terms are 0.0 except for in X state):
    for i in range(1,nq+1):
        qi = getindicies(q[i-1]) #seq in q at i position concantenated with a gap
        f_X[i,0] = eProb[qi,4]*(tProb[0,1]*f_M[i-1,0] + tProb[1,1]*f_X[i-1,0])
        f_M[i,0] = 0.0
        f_Y[i,0] = 0.0
    #then i = 0 case:
    for j in range(1,nr+1):
        rj = getindicies(r[j-1]) #seq in r at j position concantenated with a gap
        f_Y[0,j] = eProb[4,rj]*(tProb[0,2]*f_M[0,j-1] + tProb[2,2]*f_Y[0,j-1])
        f_M[0,j] = 0.0
        f_X[0,j] = 0.0
    #Recursion for i = 1,...,nq and j = 1,...,nr:
    for j in range(1,nr+1):
        for i in range(1,nq+1):
            qi = getindicies(q[i-1])
            rj = getindicies(r[j-1])
            f_M_i_j = eProb[qi,rj]*(tProb[0,0]*f_M[i-1,j-1] + tProb[1,0]*f_X[i-1,j-1] + tProb[2,0]*f_Y[i-1,j-1])
            f_X_i_j = eProb[qi,4]*(tProb[0,1]*f_M[i-1,j] + tProb[1,1]*f_X[i-1,j])
            f_Y_i_j = eProb[4,rj]*(tProb[0,2]*f_M[i,j-1] + tProb[2,2]*f_Y[i,j-1])
            sum_all_state_i_j = 1
            f_M[i,j] = f_M_i_j/sum_all_state_i_j
            f_X[i,j] = f_X_i_j/sum_all_state_i_j
            f_Y[i,j] = f_Y_i_j/sum_all_state_i_j
    return f_M,f_X,f_Y

cdef inline int getindicies(int q):
    cdef int qi
    if q == 65: #its an A
       qi = 0
    if q == 67: #its a C
       qi = 1
    if q == 84: #its a T
       qi = 2
    if q == 71: #its a G
       qi = 3
    return qi

def numTR(np.ndarray[np.float_t,ndim=2] f_M, np.ndarray[np.float_t,ndim=2] f_X, np.ndarray[np.float_t,ndim=2] f_Y, int ntr, int nr, int nq):
    cdef np.ndarray[np.float_t,ndim=1] endAt = np.zeros((nr+1), dtype=np.float)
    cdef int j
    cdef float allW = 0.0
    cdef float num = 0.0
    for j in range(1,nr+1):
        if j%ntr == 0:
           endAt[j] = f_M[nq,j]+f_X[nq,j]+f_Y[nq,j]
    allW = np.sum(endAt)
    
    for j in range(1,nr+1):
        if j%ntr == 0:
           num = num + (j/ntr)*endAt[j]/allW
    return num
