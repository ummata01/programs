#!/usr/bin/env python

import sys
import os

###################################################################################################
#sys.argv[1] = input m5 file (.m5)                                     
#sys.argv[2] = input hgTablewithrepeats information (.txt)             
#sys.argv[3] = the directory path where the out file needs to be stored (format .binned)
#python querydict2.py chr10_ovrhg1k.m5 hgchr10.txt /hpc/users/ummata01/targets_overhang100/bin_out 
###################################################################################################

class AlignmentHit:
    def __init__( self ):
        self.query_length, self.query_id, self.query_start, \
            self.query_end, self.query_strand = 0, None, 0, 0, None
        self.target_length, self.target_id, self.target_start, \
            self.target_end,self.target_strand = 0, None, 0, 0, None
        self.full = None


def parseRm5(file):
    """Parses readmatcher -printFormat 5 output into AlignmentHit objects"""
    for line in open(file):
        #print line
        
        values = line.rstrip("\n").split(" ")
        hit = AlignmentHit()
        hit.full = line
        hit.query_id, hit.query_length, hit.query_start, hit.query_end, hit.query_strand = values[0], int(values[1]), int(values[2]), int(values[3]), values[4]

        hit.target_id, hit.target_length, hit.target_start, hit.target_end, hit.target_strand = values[6], int(values[7]), int(values[8]), int(values[9]), values[10]
        hit.score = -1*int(values[11])
        #target_id = values[6] because there is white space in the m5 file before target_id

        hit.query_id = "/".join(hit.query_id.split("/")[0:3])
        yield hit

def dictforqueries(fastqReads,m5file):
    for hit in parseRm5(m5file): 
  	fastqReads[hit.query_id] = hit

def binm5file():
    inm5fn = sys.argv[1] #input .m5 file
    fastqReads = {}
    dictforqueries(fastqReads,inm5fn)

    tempm5 = inm5fn.split("/")[-1].split(".")[0]    
    outBinfn = sys.argv[3] #directory to store the .binned_anchors files
    hgT_outFn = outBinfn + "/" + tempm5 + ".binned_anchors"
    outfile = open(hgT_outFn, 'w')
    #m5out = outBinfn + "/" + tempm5 + ".m5" #do not need to rewrite this as I already have this information
    #outm5file = open(m5out, 'w')

    inhgTfn = sys.argv[2] #input reference Tandem repeat file (from UCSC genome browser)
    for hline in open(inhgTfn, 'r'):
        hvalues = hline.rstrip("\n").split()
        hgT_target = hvalues[0]
        hgT_start = int(hvalues[1])
        hgT_end = int(hvalues[2])
        hgT_trfSeq = hvalues[15]
        #hgT_perMatch = int(hvalues[7])
        hgT_period = int(hvalues[4])
        hgT_copyNum = float(hvalues[5])
        for query,obj in fastqReads.items():
            if obj.target_start < hgT_start and obj.target_end > hgT_end:
               outfile.write("%s %i %i %i %i %d %.2f %s %i %i\n" %(query, obj.target_start, obj.target_end, hgT_start, hgT_end, hgT_period, hgT_copyNum, hgT_trfSeq,(hgT_start - obj.target_start),(obj.target_end - hgT_end)))
               #writing out the anchor lengths on the trf_start and trf_end sides
               #outm5file.write("%s" %(obj.full)) 
               outfile.flush()
               #outm5file.flush() 
    outfile.close()    
    outm5file.close()

binm5file()
