#!/usr/bin/env python
import os
import sys
from io_hupac.ReadMatcherIO import parseRm5
from model.AlignmentHit import AlignmentHit
from io_hupac.binFileIO import parsebinf
from model.binLine import binLine
from model.Sequence import revcomp as rc
import numpy as np
import numba
from numba import jit
from numba import *
import time

#import cProfile
#read in two sequences. target. query. and align them 
#
#target sequence could be decomposed into three parts:
#a) prefix_tR: region upstream of tR region (calculated by say trf)
#b) tR: region of the tandem repeat
#c) suffix_tR: region downstream of the tR region
#
#A general function to perform prefix_tR/suffix_tR alignment with query
#Another function to perform tR alignment with query


def dictforbindata(binData,binfile):
    for line in parsebinf(binfile):
        binData[line.query_id] = line

def dictforqueries(queryAligns,m5file):
    for hit in parseRm5(m5file):
        queryAligns[hit.query_id] = hit

#@jit('i8,i8,i8(i8,i8,i8[:],i8[:],i8[:,:],i8[:,:],i8)')
#@jit(argtypes=(int32,int32,int32[:],int32[:],int32[:,:],int32[:,:],int32), restype=(int32,int32,int32))
def recurr(query,prefix,preScore,preMaxScore):
    nq = np.size(query)
    npre = np.size(prefix)
    pen = -2
    vals = np.arange(3,dtype=np.int_)
    for i in range(1,nq+1):
        for j in range(1,npre+1):
            m = -1
            if query[i-1] == prefix[j-1]:
               m = 1
            vals[0] = preScore[i-1][j-1] + m
            vals[1] = preScore[i-1][j] + pen
            vals[2] = preScore[i][j-1] + pen
            preScore[i][j] = np.max(vals)
            preMaxScore[i][j] = np.argmax(vals)

    best = np.arange(3,dtype=np.int_)
    best[0] = 1
    best[1] = 1
    best[2] = 0
 
    for i in range(nq+1):
        j = npre
        if best[2] < preScore[i][j]:
           best[2] = preScore[i][j]
           best[0] = i
           best[1] = j
    return best

fastrecurr = jit(i8[:](i8[:],i8[:],i8[:,:],i8[:,:]))(recurr)

def sw_prefix(query,prefix,preScore,preMaxScore):
#    print len(query), len(prefix)
    #Recursion for prefix_vs_query: local alignment
    best = np.arange(3,dtype=np.int_)
    start = time.time()
    best = fastrecurr(query,prefix,preScore,preMaxScore)
    duration = time.time() - start
    print "time spent in recurr in secs: ", duration
    #bestscores and indices for the scoring matrix, preScore

    #trace back here ->
    align_pre = []
    align_query = []
#    align_state = []

    tempScore = best[2]
    temp_i = best[0]
    temp_j = best[1]
    while (temp_j > 0):
          if preMaxScore[temp_i][temp_j] == 0:
             align_pre.append(prefix[temp_j-1])
             align_query.append(query[temp_i-1])
#             align_state.append("M")
             temp_i = temp_i - 1
             temp_j = temp_j - 1
             tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 1:
               align_pre.append("-")
               align_query.append(query[temp_i-1])
#               align_state.append("D")
               temp_i = temp_i - 1
               temp_j = temp_j
               tempScore = preScore[temp_i][temp_j]
          elif preMaxScore[temp_i][temp_j] == 2:
               align_pre.append(prefix[temp_j-1])
               align_query.append("-")
#               align_state.append("I")
               temp_i = temp_i
               temp_j = temp_j - 1
               tempScore = preScore[temp_i][temp_j]
    print "Score in sw_prefix: ",best[2]
    return best[2]
    
#########
def recurrT(query,tR,T,T_p,len_prefix,P):
    p = len_prefix
    t = np.size(tR)
    nq = np.size(query) 
    pen = -2
    vals = np.arange(2,dtype=np.int_)
    vals1 = np.arange(4,dtype=np.int_)
    for i in range(1,nq+1):
        m = -1
        if query[i-1] == tR[0]:
            m = 1        
        vals[0] = P[i-1][p] + m
        vals[1] = T[i-1][t] + m
        T[i][1] = np.max(vals)
        T_p[i][1] = 3 + np.argmax(vals)
        for j in range(2,t+1):
            m = -1
            if query[i-1] == tR[j-1]:
                m = 1
            vals1[0] = T[i-1][j] + pen
            vals1[1] = T[i][j-1] + pen
            vals1[2] = T[i-1][j-1] + m
            vals1[3] = P[i-1][p] + m
            T[i][j] = np.max(vals1)
            T_p[i][j] = np.argmax(vals1)
    return nq

fastrecurrT = jit(i8(i8[:],i8[:],i8[:,:],i8[:,:],i8,i8[:,:]))(recurrT)

#@autojit
def sw_tR_simple(query,tR, T, T_p,len_prefix, P,T_rowMax):
    start = time.time()
    nq=fastrecurrT(query,tR,T,T_p,len_prefix,P)
    duration = time.time() - start
    print "duration in tR: ", duration
   # print T
   # print T_p
    t = np.size(tR)
    for k in range(nq+1):
        temp = T[k][0]
        temp_tR = 0
        for l in range(1,t+1):
            if temp < T[k][l]:
               temp = T[k][l]
               temp_tR = l
        T_rowMax[k][0] = temp
        T_rowMax[k][1] = temp_tR
    
####
def recurrS(query,suffix,sufScore,sufMaxScore,tR_rowMax):
    nq = np.size(query)
    nsuf = np.size(suffix)
    pen = -2
    vals = np.arange(3,dtype=np.int_)
    for i in range(1,nq+1):
        m = -1
        if query[i-1] == suffix[0]:
           m = 1
        sufScore[i-1][1] = tR_rowMax[i-1][0] + m
        sufMaxScore[i-1][1] = 3
        for j in range(2,nsuf+1):
            m = -1
            if query[i-1] == suffix[j-1]:
               m = 1
            vals[0] = sufScore[i-1][j-1] + m
            vals[1] = sufScore[i-1][j] + pen
            vals[2] = sufScore[i][j-1] + pen
            sufScore[i][j] = np.max(vals)
            sufMaxScore[i][j] = np.argmax(vals)
    return nq

fastrecurrS = jit(i8(i8[:],i8[:],i8[:,:],i8[:,:],i8[:,:]))(recurrS)
#@autojit
def sw_suffix(query,suffix,sufScore,sufMaxScore,tR_rowMax):
#    print "".join(suffix)
#    print "Length of suffix:", len(suffix)
    start = time.time()
    dummy=fastrecurrS(query,suffix,sufScore,sufMaxScore,tR_rowMax)
    duration = time.time() - start
    print "duration in suffix in secs: ",duration

#@autojit 
def tb_suffix(query,suffix,S,S_p):
    temp = 0
    i = 0
    j = np.size(suffix)
    nq = np.size(query)
    # Finds the maximal value for the suffix to begin tb
    for k in range(nq+1):
        if temp < S[k][j]:
           temp = S[k][j]
           i = k
    bestscoresuffix = temp
    pre = False
    index_i_tR = 0
    while (not(pre)):
        if (S[i][j] > 0):
           if (S_p[i][j] == 1):
              i += - 1
           elif (S_p[i][j] == 2):
              j += - 1
           elif (S_p[i][j] == 0):
              i += - 1
              j += - 1
           elif (S_p[i][j] == 3):
              j += - 1
              i += -1
              pre = True
           index_i_tR = i
        else :
           index_i_tR = 0
           pre = True
 
    print "bestscoreSuff: ",bestscoresuffix 
    return index_i_tR, bestscoresuffix

def finalTrace(qArray,tR,P,P_p,T,T_p,tR_rowMax,index_i_tR):
    i = index_i_tR
    if (i > 0):
       j = tR_rowMax[i][1]
       scoretRentry = T[i][j]
       t = len(tR)
       pre = False
       query = map(chr,qArray)
       qList = []
       tList = []
       while (not (pre)):
           if (T_p[i][j] == 0):
              qList.append(query[i-1])
              tList.append("-")
              i += - 1
           elif (T_p[i][j] == 1):
              qList.append("-")
              tList.append(tR[j-1])
              j += - 1
           elif (T_p[i][j] == 2):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += - 1
              j += - 1
           elif (T_p[i][j] == 3):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += - 1
              pre = True
              scoretRexit = T[i+1][j]
           else:
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += -1
              j = t
       tList.reverse()
       print "tList: ",tList
       qList.reverse()
       repeatAligned = []
       repeatAligned.append(''.join(tList))
       repeatAligned.append(''.join(qList))

       tempTList = []
       for i in range(0, len(tList)):
           if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
              tempTList.append(tList[i])

       tRLength = len(tempTList)
       str_tR = "".join(tR)
       print "str_tR: ", str_tR
       print "tempTList: ",tempTList

       if (t < tRLength):
          flag = False
          count_st = 0
          while (not(flag)):
              temp = []
              for k in range(len(tR)):
                  temp.append(tempTList[k+count_st])
              temp_str = "".join(temp)
              if str_tR == temp_str:
                 flag = True
              else:
                 count_st += 1
          flag = False
          count_end = tRLength
          while (not (flag)):
              temp = []
              for k in range(len(tR)):
                  temp.append(tempTList[count_end-1-k])
              temp.reverse()
              temp_str = "".join(temp)
              if str_tR == temp_str:
                 flag = True
              else:
                 count_end += -1
          extraBaseEnd = tRLength - count_end
          number_tR = (tRLength - count_st - extraBaseEnd)/len(tR)
       else:
          number_tR = 0.0
    else:
       number_tR = -1.0
       repeatAligned = ['Issue','In','Alignment']
       scoretRentry = 0

    return number_tR, repeatAligned, scoretRentry
 
#@autojit 
def alignRegions(query,pre_suf_tR):
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
    pArray = np.arange(len_prefix,dtype=np.int_)
    tempP = map(ord,prefix)
    tempP2 = np.asarray(tempP)
    pArray = tempP2
#    print "pArray: ", pArray

    suffix = []
    suffix = list(pre_suf_tR[1])
    len_suffix = len(suffix)
    sArray = np.arange(len_suffix,dtype=np.int_)
    tempS = map(ord,suffix)
    tempS2 = np.asarray(tempS)
    sArray = tempS2
#    print "sArray: ", sArray

    tR = []
    tR = list(pre_suf_tR[2])
    len_tR = len(tR)
    tRArray = np.arange(len_tR,dtype=np.int_)
    temptR = map(ord,tR)
    temptR2 = np.asarray(temptR)
    tRArray = temptR2
#    print "tRArray: ", tRArray

    preScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    # Added in initialization for prefix
    preMaxScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    preMaxScore.fill(-1)
    for j in range (1, len_prefix+1):
        preScore[0][j] = preScore[0][j-1] -1 
    Prefix_BestScore = 0 
    Prefix_BestScore = sw_prefix(query,pArray,preScore,preMaxScore)

    tRScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)-10000000
    tRMaxScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)-1
    tR_rowMax = np.zeros((len(query)+1,2),dtype=np.int_)
    sw_tR_simple(query,tRArray,tRScore,tRMaxScore,len_prefix,preScore,tR_rowMax)
    #tb_tR_simple(query,tR,tRScore,tRMaxScore)

    index_i_tR = 0
    Suffix_BestScore = 0
    sufScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)-10000000
    sufMaxScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)-1
    sw_suffix(query,sArray,sufScore,sufMaxScore,tR_rowMax)
    index_i_tR,Suffix_BestScore = tb_suffix(query,sArray,sufScore,sufMaxScore)
   
    number_tR = 0.0
    tR_maxScore = 0
    repeatAligned = [] 
    print "Entering finalTrace: "
    number_tR, repeatAligned, tR_maxScore = finalTrace(query,tR,preScore,preMaxScore,tRScore,tRMaxScore,tR_rowMax,index_i_tR)   
    #here query is in int, but tR is in string representation
 
    return number_tR, repeatAligned, Prefix_BestScore, Suffix_BestScore, tR_maxScore 
#####
   
def calculateRegions_tR(flag,target, rev_target, trf_start, trf_end, target_start, target_end,refTrf_seq, flankLen):
    listPreSuf_tR = []
    tempTarget = []
    prefix_tR = ""
    suffix_tR = ""
    lengthOfPreX = trf_start - target_start
    lengthOfSufX = target_end - target_start
    startSufX = trf_end - target_start
    if flag == True:
       tempRevTarget = rev_target
       rev_prefix_tR = []
       for j in range(0,lengthOfPreX):
           rev_prefix_tR.append(tempRevTarget[j])
       suffix_tR = rc(''.join(rev_prefix_tR))
       
       rev_suffix_tR = []
       for k in range(startSufX, lengthOfSufX):
           rev_suffix_tR.append(tempRevTarget[k])
       prefix_tR = rc(''.join(rev_suffix_tR))

       refTrf_seq = rc(refTrf_seq)
    else:
#       print "I am in flag = false"
       tempTarget = target 
       prefix_tR = []
       for j in range(0,lengthOfPreX):
           prefix_tR.append(tempTarget[j])

       suffix_tR = []
       for k in range(startSufX, lengthOfSufX):
           suffix_tR.append(tempTarget[k])
       prefix_tR = ''.join(prefix_tR)
       suffix_tR = ''.join(suffix_tR)
    
    listPreSuf_tR.append(prefix_tR[-flankLen:])
    listPreSuf_tR.append(suffix_tR[0:flankLen])
    listPreSuf_tR.append(refTrf_seq)       
    
    return listPreSuf_tR
 
def align_main():
    #input 1: m5 file
    start = time.time()
    inm5fn = sys.argv[1]
    tempstr = inm5fn.rstrip("\n").split("/")
   
#    outdir = tempstr[-2]
#    outfn = tempstr[-1]
#    outStr = "/projects/HuPac/repartition_MAR29/all_m5/bin_out/run2" + "/" + outdir + "/" + outfn + "_1"
    
    outStr = "test.txt"
    outfile = open(outStr,'w')
    outfile.write("query_id targetStart targetEnd RefTandemRepeat TrInQuery RefcopyNum PreScore SufScore tR_Score TrAligned QueryAligned\n")
    #input 2: txt file with binned queries
    inbinfn = sys.argv[2]

    #input 3: length of region flanking TR to align to 
    flankLen = int(sys.argv[3])

    binData = {}
    dictforbindata(binData,inbinfn)
    queryAligns = {}
    dictforqueries(queryAligns,inm5fn)
    #form a dictionary with the *.txt file. This dictionary could be formed everytime when a new query is started.

    for query,hit in queryAligns.items():
          if hit.query_id in binData:
             pre_suf_tR = []
             flag = False # assume its in positive strand
             if hit.target_strand == 0:
                flag = True #1 means its the negative strand
             tempLine = binLine()
             tempLine = binData[hit.query_id] 
             pre_suf_tR = calculateRegions_tR(flag,hit.TargetSeq,hit.RevTargetSeq,tempLine.refTrf_st,tempLine.refTrf_en,hit.target_start,hit.target_end,tempLine.refTrf_seq, flankLen)
             loq = len(hit.QuerySeq)
             qArray = np.arange(loq,dtype=np.int_)
             tempQ = map(ord,hit.QuerySeq)
             tempQ2 = np.asarray(tempQ)
             qArray = tempQ2
             number_tR, repeatAligned, PreS, SufS, tR_S = alignRegions(qArray, pre_suf_tR)
             outfile.write("%s %d %d %s %d %d %d %d %d %s %s\n" %(hit.query_id,hit.target_start,hit.target_end,pre_suf_tR[2],number_tR,tempLine.refTrf_copyNum,PreS,SufS,tR_S,repeatAligned[0],repeatAligned[1]))
             outfile.flush()
          else: 
             outfile.write("query %s does not exist in binned file\n" %(hit.query_id))
             continue
    duration = time.time() - start
    print "Total time for this run (secs)= ",duration

align_main() 
