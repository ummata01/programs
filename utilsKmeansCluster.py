import sys
import os
import numpy as np

np.random.seed(42)


def calSigma(d,c,Ri,K):
    if Ri > K:
     factor = 1.0/(Ri-K)
     rss = 0.0
     for j in range(len(d)):
         rss += (d[j]-c)*(d[j]-c)
     #print "factor ",factor
     #print "rss ",rss
     return factor*rss
    else:
     return -1

def log_LikGauss(data,centroids,K,R,M=1):
    '''Calculating the maximum log-likelihood for the data
       l(D) = -Ri*(log2*pi)/2.0 - Ri*M*(log(Sigma-hat^2))/2.0 - (Ri - K)/2.0 + Ri*log(Ri) - Ri*log(R) (Ref: X-Means paper)
       D = data; Ri = set of points which belongs to centroid i, or, |Di|; M = number of dimensions; Sigma-hat = estimated standard deviation maximizing the log-likelihood function;
       K = number of clusters; R = |D| 
    '''
    pi = 3.141
    K = 1
    Ri = len(data) 
    #print "Ri", Ri
    sigma_hat = calSigma(data,centroids,Ri,K)
    if sigma_hat != -1:
       lDi = -1*Ri*np.log(2*pi)/2.0 - Ri*M*(np.log(sigma_hat))/2.0 - (Ri - K)/2.0 + Ri*np.log(Ri) - Ri*np.log(R)
       return lDi
    else:
       return 0

def BIC_Gauss(db,R,centroids,K,M=1):
    '''BIC(Mj) = lj(D) - pj*log(R)/2.0 (Ref: X-Means paper; Derivation in the notes)
       Mj = alternative models describing the data, where different models correspond to solutions with different values of number of clusters, K
       lj(D) = Maximum log-likelihood estimate for model j for data D
       R = |D|, number of data points in each entry of db
       pj = number of free parameters in the model j = M*K + 1 + K - 1 = (M + 1)*K
       K = number of clusters; M = number of dimensions of the data space
    '''
    lLik = np.zeros((K),dtype=np.float_)
    pj = (M + 1)*K
    sum_lLik = 0.0
    for i in range(K):
        #print "db[i]", db[i]
        #print "centroids[i]", centroids[i]
        lLik[i] = log_LikGauss(db[i],centroids[i],K,R)
        sum_lLik += lLik[i]
    bic = sum_lLik - pj*np.log(R)/2.0 
    return bic

