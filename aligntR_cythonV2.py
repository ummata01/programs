#!/usr/bin/env python
import os
import sys
import numpy as np
import time
import multiprocessing as mp
import model.dpFuncs_sw as dpF

#GLOBAL VARIABLE
fastaDict = {}

class bSeq:
    def __init__( self ):
        self.query_id, self.target_id,self.target_st, self.target_end = None, None, 0, 0
        self.tR_st, self.tR_end, self.tR_period, self.tR_copyNum = 0, 0, 0, 0.0
        self.query_Seq, self.tR_Seq, self.prefix_Seq, self.suffix_Seq = None, None, None, None
        self.flankLen = 0
#QueryId target_St target_End tR_St tR_End tR_Period tR_CopyNum tR_Seq PrefixSeq SuffixSeq

def create_fastaDict(fastaDict,file):
    fastaIn = open(file,'r')
    fastaLine = fastaIn.readline()
    while (fastaLine):
          qidFin = fastaLine
          qidF = qidFin.strip().split(">")[1]
          seq = fastaIn.readline().strip()
          fastaDict.setdefault(qidF,[]).append(seq)
          fastaLine = fastaIn.readline()
    print "FastaDictionary formed, with number of queries: ",len(fastaDict)

def parsebSeq(file):
    for line in open(file):
        values = line.rstrip("\n").split()
        hit = bSeq()
        hit.query_id, hit.target_id, hit.target_st, hit.target_end = values[0],values[1],int(values[2]),int(values[3])
        hit.tR_st, hit.tR_end, hit.tR_period, hit.tR_copyNum = int(values[4]), int(values[5]), int(values[6]), float(values[7])
        hit.tR_Seq, hit.prefix_Seq, hit.suffix_Seq = values[8],values[9],values[10]
        hit.flankLen = int(sys.argv[3]) 
        if hit.query_id in fastaDict:
           print "I am in fastaDict"
           hit.query_Seq = fastaDict[hit.query_id][0]
        else:
           hit.query_Seq = 'None'
        yield hit

def dictforqueries(queryAligns,file):
    for hit in parsebSeq(file):
        queryAligns[hit.query_id] = hit

    
def tb_suffix(query,suffix,S,S_p):
    temp = 0
    i = 0
    j = np.size(suffix)
    nq = np.size(query)
    # Finds the maximal value for the suffix to begin tb
    for k in range(nq+1):
        if temp < S[k][j]:
           temp = S[k][j]
           i = k
    bestscoresuffix = temp
    pre = False
    index_i_tR = 0
    try:
      while (not(pre)):
        if (S[i][j] > 0):
           if (S_p[i][j] == 1):
              i += - 1
           elif (S_p[i][j] == 2):
              j += - 1
           elif (S_p[i][j] == 0):
              i += - 1
              j += - 1
           elif (S_p[i][j] == 3):
              j += - 1
              i += -1
              pre = True
           index_i_tR = i
        else :
           index_i_tR = 0
           pre = True
    except:
      index_i_tR = -1
      bestscoresuffix = -1000
      return index_i_tR, bestscoresuffix
    else: 
      return index_i_tR, bestscoresuffix

def finalTrace(qArray,tR,P,P_p,T,T_p,tR_rowMax,index_i_tR):
    try:
      i = index_i_tR
      print "i index in TR: ", i
      if (i > 0):
        j = tR_rowMax[i][1]
        scoretRentry = T[i][j]
        t = len(tR)
        pre = False
        query = map(chr,qArray)
        qList = []
        tList = []
        #print T
        #print T_p
        while (not (pre)):
          if (T[i][j] > 0):
           if (T_p[i][j] == 0):
              qList.append(query[i-1])
              tList.append("-")
              i += - 1
           elif (T_p[i][j] == 1):
              qList.append("-")
              tList.append(tR[j-1])
              j += - 1
           elif (T_p[i][j] == 2):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += - 1
              j += - 1
           elif (T_p[i][j] == 3):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              #open up gaps on query
              for k in range(0,j-1):
                  qList.append("-")
                  tList.append(tR[k])
              i += - 1
              j = t
           elif (T_p[i][j] == 4):
              qList.append(query[i-1])
              tList.append(tR[j-1])
              i += - 1
              pre = True
              scoretRexit = T[i+1][j]
          else:
              print "Tandem repeat score <= 0"
        tList.reverse()
#       print "tList: ",tList
        qList.reverse()
        repeatAligned = []
        repeatAligned.append(''.join(tList))
        repeatAligned.append(''.join(qList))

        tempTList = []
        for i in range(0, len(tList)):
           if tList[i] == 'A' or tList[i] == 'C' or tList[i] == 'G' or tList[i] == 'T':
              tempTList.append(tList[i])

        tRLength = len(tempTList)
        str_tR = "".join(tR)
        str_tempTList = "".join(tempTList)

        if (t < tRLength):
             base_st = str_tempTList.index(str_tR)
             base_end = tRLength - str_tempTList.rindex(str_tR) - len(tR)
             boundary = 0.0
             if 0.5*t <= base_st:
                boundary = 1.0
             if 0.5*t <= base_end:
                boundary = 1.0
             if 0.5*t <= base_st and 0.5*t <= base_end:
                boundary = 2.0
             else:
                boundary = 0.0
             print boundary, base_st, base_end
             number_tR = (tRLength - base_st - base_end)/len(tR) + boundary
        else:
          number_tR = -20000.0
      else:
        number_tR = -10000.0
        repeatAligned = ['Issue','In Alignment: suffix traceback fault']
        scoretRentry = 0
      return number_tR, repeatAligned, scoretRentry, scoretRexit
    except:
      number_tR = -00000.0
      repeatAligned = ['Error','Error']
      scoretRentry = -99999
      scoretRexit = -999999
      return number_tR, repeatAligned, scoretRentry, scoretRexit
 
#@autojit 
def alignRegions(query,pre_suf_tR):
    '''INPUT: 
    query - query sequence
    pre_suf_tR = [<prefix_seq>, <suf_seq>, <tr_seq>]
    RETURNS:  
    number_tR = number of tandem repeats in query
    repeatAligned = [<TR align sequence, ref>, <TR align sequence, query>]
    Prefix_BestScore = Smith-waterman score of Prefix Score matrix
    Suffix_BestScore = SW score in suffix matrix (overall: Prefix+TR+suffix)
    tR_maxScore = SW score in tR matrix (Prefix + TR)
    tR_exitScore = SW score in tR matrix where alignment jumps to prefix matrix
    '''
    temp_trfSeq = []
    prefix = []
    prefix = list(pre_suf_tR[0])
    len_prefix = len(prefix)
    pArray = np.arange(len_prefix,dtype=np.int_)
    tempP = map(ord,prefix)
    tempP2 = np.asarray(tempP)
    pArray = tempP2
#    print "pArray: ", pArray

    suffix = []
    suffix = list(pre_suf_tR[1])
    len_suffix = len(suffix)
    sArray = np.arange(len_suffix,dtype=np.int_)
    tempS = map(ord,suffix)
    tempS2 = np.asarray(tempS)
    sArray = tempS2
#    print "sArray: ", sArray

    tR = []
    tR = list(pre_suf_tR[2])
    len_tR = len(tR)
    tRArray = np.arange(len_tR,dtype=np.int_)
    temptR = map(ord,tR)
    temptR2 = np.asarray(temptR)
    tRArray = temptR2
#    print "tRArray: ", tRArray

    preScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    # Added in initialization for prefix
    preMaxScore = np.zeros((len(query)+1,len_prefix+1),dtype=np.int_)
    preMaxScore.fill(-1)
    for j in xrange (1, len_prefix+1):
        preScore[0][j] = -5*j

    Prefix_BestScore = 0
    Prefix_BestScore = dpF.sw_prefix(query,pArray,preScore,preMaxScore)

    # Added in initialization for tR
    tRScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)
    for j in xrange(1,len_tR+1):
        tRScore[0][j] = -10000000

    tRMaxScore = np.zeros((len(query)+1,len_tR+1),dtype=np.int_)-1
    tR_rowMax = np.zeros((len(query)+1,2),dtype=np.int_)
    dpF.sw_tR_simple(query,tRArray,tRScore,tRMaxScore,len_prefix,preScore,tR_rowMax)

    index_i_tR = 0
    Suffix_BestScore = 0
    # Added in initialization for suffix
    sufScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)
    for j in xrange(1,len_suffix+1):
        sufScore[0][j] = -10000000

    sufMaxScore = np.zeros((len(query)+1,len_suffix+1),dtype=np.int_)-1
    dpF.sw_suffix(query,sArray,sufScore,sufMaxScore,tR_rowMax)
    index_i_tR,Suffix_BestScore = tb_suffix(query,sArray,sufScore,sufMaxScore)

    number_tR = 0.0
    tR_maxScore = 0
    repeatAligned = []
    print "Entering finalTrace for query: "
    number_tR, repeatAligned, tR_maxScore, tR_exitScore = finalTrace(query,tR,preScore,preMaxScore,tRScore,tRMaxScore,tR_rowMax,index_i_tR)
    #here query is in int, but tR is in string representation
    print "exit finalTrace..."
    return number_tR, repeatAligned, Prefix_BestScore, Suffix_BestScore, tR_maxScore, tR_exitScore

#####

def align_worker((query, hit)):
    print "worker starts for query: ", query
    start1 = time.time()
    outList = []
    pre_suf_tR = []
    flankLen = hit.flankLen
    try:
       #p = hit.prefix_Seq
       #s = hit.suffix_Seq
       p = hit.prefix_Seq[-flankLen:]
       s = hit.suffix_Seq[0:flankLen]
       tr = hit.tR_Seq
       pre_suf_tR = [p,s,tr]
    except:
       print "Error in pre_suf_tR List. Please check the sequences."
    if hit.query_id != 'None':
       loq = len(hit.query_Seq)
       qArray = np.arange(loq,dtype=np.int_)
       tempQ = map(ord,hit.query_Seq)
       tempQ2 = np.asarray(tempQ)
       qArray = tempQ2
       number_tR, repeatAligned, PreS, SufS, tR_S, tR_Ex = alignRegions(qArray, pre_suf_tR)
       outList = [hit.query_id,hit.target_id,hit.target_st,hit.target_end,pre_suf_tR[2],number_tR,hit.tR_copyNum,PreS,SufS,tR_S,tR_Ex,repeatAligned[0],repeatAligned[1],hit.tR_st,hit.tR_end,hit.tR_period]
       end1 = time.time()
       print "Duration within worker: ",end1-start1
       print "worker ends for query: ", query
       return outList
    else:
       outList = ["No query in Fasta"]
       return outList

if __name__ == '__main__': 
    #'''USAGE: python aligntRMPmap3_tier2.py /RESULT.binned /allChrFasta.fa 1000 31'''
    #input 1: RESULT.binned file
    start = time.time()
    inm5fn = sys.argv[1]
    tempstr = inm5fn.rstrip("\n").split("/")
   
    #outdir = tempstr[-2]
    #outfn = tempstr[-1]
    #outStr = "/projects/HuPac/repartition_MAR29/tierII_ALignments/bin_out" + "/" + outdir + "/" + outfn + ".out"
    #outStr2 ="/projects/HuPac/repartition_MAR29/tierII_ALignments/bin_out" + "/" + outdir + "/" + outfn + ".qbinr"
    
    outStr = "/hpc/users/ummata01/gitrepos/workIn/debug/alignCython/II.out"
    outStr2 = "/hpc/users/ummata01/gitrepos/workIn/debug/alignCython/II.qbinr"
    
    #input 2: fasta file: needed to associate query_id with query_Seq
    inFafn = sys.argv[2]

    #input 3: length of region flanking TR to align to 
    flankLen = int(sys.argv[3])

    #input 4:  number of cpus to use
    ncores = int(sys.argv[4])

    #Create a dictionary with <query_id:Sequence> entries
    create_fastaDict(fastaDict,inFafn)

    #Create a dictionary with <query_id:filtered queries for a TR event with its relevant information> entries
    queryAligns = {}
    dictforqueries(queryAligns,inm5fn)
    NumOfQ = len(queryAligns)
    print "No of entries in queryAligns dict: ", NumOfQ
    #form a dictionary with the *.txt file. This dictionary could be formed everytime when a new query is started.

    try:
       pool = mp.Pool(ncores)
       print "Pool of workers created: ",ncores
    except OSError:
       print "OS raied an error with resource limitations: RETRY this file"
    else:
       try:
          #Parallel processes are spawned where each process gets a particular <key:value> or <query_id:corresponding information about seq, TR event, prefix, suffix in the form of a bSeq() object>
          results = pool.map(align_worker, queryAligns.iteritems()) #the result from align_worker are collected in results which is of the form  [[],[],...[]], a list of lists
       except:
          print "Error with apply_async, empty or partial results list: RETRY"
       else:
          outfile = open(outStr,'w')
          outfile2 = open(outStr2,'w')
          outfile.write("query_id targetStart targetEnd RefTandemRepeat TrInQuery RefcopyNum PreScore SufScore tR_ScoreIn tR_ScoreOut TrAligned QueryAligney\n")
          outfile2.write("Query_id target targetStart targetEnd ReftRStart ReftREnd RefPeriod RefNum QNum Ratio tRSeq tR_ScoreDiff\n")
          for result in results:
              outfile.write("%s %d %d %s %.2f %.2f %d %d %d %d %s %s\n"%(result[0],result[2],result[3],result[4],result[5],result[6],result[7],result[8],result[9],result[10],result[11],result[12]))
              outfile2.write("%s %s %d %d %d %d %d %.2f %.2f %.2f %s %d\n"%(result[0],result[1],result[2],result[3],result[13],result[14],result[15],result[6],result[5],(result[5]/float(result[6])),result[4],result[10]-result[9]))
          outfile.close()
          outfile2.close()
          duration = time.time() - start
          print "Total time for this run (secs)= ",duration
